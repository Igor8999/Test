package lanars.com.defigo.login.recovery;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.service.IAuthService;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RecoveryPasswordFragmentTest {

    @Mock
    private IAuthService authService;

    @Mock
    private RecoveryPasswordView$$State viewState;

    private RecoveryPasswordPresenter presenter;

    @Before
    public void setUp() {
        presenter = new RecoveryPasswordPresenter(authService);
        presenter.setViewState(viewState);
    }

    @Test
    public void sendEmailRecoverySuccess() throws Exception {
        String email = "email@email.com";

        Map<String, String> params = new HashMap<>();
        params.put("email", email);

        StatusModel statusModel = new StatusModel();

        when(authService.recoveryPassword(params)).thenReturn(Observable.just(statusModel));

        presenter.sendEmailRecovery(email);

        InOrder inOrder = Mockito.inOrder(authService, viewState);

        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(authService).recoveryPassword(params);
        inOrder.verify(viewState).onHideProgress();
    }

    @Test
    public void validateEmailSuccess() throws Exception {
        String email = "ff.dd@mail.com";

        boolean isValid = presenter.validateEmail(email);

        assertThat(isValid, is(true));
    }

    @Test
    public void validateEmailFail() throws Exception {
        String email = "ff.dd mail";

        boolean isValid = presenter.validateEmail(email);

        assertThat(isValid, is(false));
        verify(viewState).enterCorrectEmailAlert();
    }

    @Test
    public void onShowError() throws Exception {
        String email = "ff.dd@mail.com";
        when(authService.recoveryPassword(any())).thenReturn(Observable.error(new Throwable()));

        presenter.sendEmailRecovery(email);

        InOrder inOrder = Mockito.inOrder(authService, viewState);

        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(viewState).onShowError(any());
        inOrder.verify(viewState).onHideProgress();
    }

}