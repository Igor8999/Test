package lanars.com.defigo.login;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.UsersRoom;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginFragmentPresenterTest {

    @Mock
    private IAppPreferences appPreferences;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IAuthService authService;

    @Mock
    private SocketManager socketManager;

    @Mock
    private LoginFragmentView$$State loginFragmentViewState;

    private LoginFragmentPresenter presenter;
    private final String email = "qwerty test";
    private final String password = "qwerty test";

    @Before
    public void setUp() {
        presenter = new LoginFragmentPresenter(null, appPreferences, userRepository, authService, socketManager);
        presenter.setViewState(loginFragmentViewState);
    }

    @Test
    public void signIn_shouldSignSuccessfully() {
        SignModel signModel = new SignModel();
        UsersRoom usersRoom = new UsersRoom(0, "Test", "Test");
        List<UsersRoom> usersRooms = new ArrayList<>();
        usersRooms.add(usersRoom);
        User user = new User(usersRooms);
        user.setId(10L);

        LoginFragmentPresenter spyPresenter = Mockito.spy(presenter);

        when(authService.signIn(signInParams())).thenReturn(Observable.just(signModel));
        when(appPreferences.saveAccessToken(anyString())).thenReturn(true);
        when(userRepository.usersMy()).thenReturn(Observable.just(user));
        when(appPreferences.saveCurrentUserId(anyInt())).thenReturn(true);

        doNothing().when(spyPresenter).updateDeviceId(any());

        spyPresenter.signIn(email, password);

        InOrder inOrder = Mockito.inOrder(spyPresenter, loginFragmentViewState, authService, appPreferences, userRepository);

        inOrder.verify(authService).signIn(any());
        inOrder.verify(loginFragmentViewState).onShowProgress();
        inOrder.verify(appPreferences).saveAccessToken(anyString());
        inOrder.verify(userRepository).usersMy();
        inOrder.verify(appPreferences).saveCurrentUserId(anyInt());
        inOrder.verify(userRepository).insertRooms(any());
        inOrder.verify(userRepository).insertUsers(any());
        inOrder.verify(spyPresenter).updateDeviceId(any());
        inOrder.verify(loginFragmentViewState).onSuccessLogin();
        inOrder.verify(loginFragmentViewState).onHideProgress();
    }

    @Test
    public void signIn_shouldShowError() {
        when(authService.signIn(signInParams())).thenReturn(Observable.error(new Throwable()));

        presenter.signIn(email, password);

        InOrder inOrder = Mockito.inOrder(loginFragmentViewState);

        inOrder.verify(loginFragmentViewState).onShowProgress();
        inOrder.verify(loginFragmentViewState).onShowError(anyString());
        inOrder.verify(loginFragmentViewState).onHideProgress();
    }

    private Map<String, String> signInParams() {
        Map<String, String> signInParams = new HashMap<>();
        signInParams.put("payload", email);
        signInParams.put("password", password);
        return signInParams;
    }
}