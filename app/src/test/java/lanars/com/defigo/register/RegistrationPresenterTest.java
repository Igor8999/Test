package lanars.com.defigo.register;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import io.reactivex.Observable;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.network.service.IDeviceService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationPresenterTest {

    @Mock
    private IAppPreferences appPreferences;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IAuthService authService;

    @Mock
    private SocketManager socketManager;

    @Mock
    private IDeviceService deviceService;

    @Mock
    private RegistrationView$$State viewState;

    @Mock
    private IFileUtils fileUtils;

    @Mock
    private Router router;

    private RegistrationPresenter presenter;

    @Before
    public void setUp() {
        presenter = new RegistrationPresenter(router, authService, userRepository, appPreferences, deviceService, socketManager, fileUtils);
        presenter.setViewState(viewState);
    }

    @Test
    public void uploadStorageImageInView() throws Exception {
        when(fileUtils.checkExistFile(any(), any())).thenReturn(true);

        presenter.uploadStorageImageInView();

        verify(viewState).uploadImageView(any());
    }

    @Test
    public void signUp_success() throws Exception {
        String userName = "userName";
        String password = "password";
        String firstName = "firstName";
        String lastName = "lastName";
        String phone = "phone";
        String email = "email";

        User user = new User();
        user.setId(1L);

        RegistrationPresenter spyPresenter = Mockito.spy(presenter);

        when(authService.signUp(any(), any(), any(), any(), any(), any(), any())).thenReturn(Observable.just(new SignModel()));
        when(userRepository.usersMy()).thenReturn(Observable.just(user));

        doNothing().when(spyPresenter).updateDeviceId(any());

        spyPresenter.signUp(userName, password, firstName, lastName, phone, email);

        InOrder inOrder = Mockito.inOrder(spyPresenter, viewState, authService, appPreferences, userRepository);

        inOrder.verify(authService).signUp(any(), any(), any(), any(), any(), any(), any());
        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(appPreferences).saveAccessToken(any());
        inOrder.verify(userRepository).usersMy();
        inOrder.verify(appPreferences).saveCurrentUserId(anyLong());
        inOrder.verify(userRepository).insertUsers(any());
        inOrder.verify(spyPresenter).updateDeviceId(any());
        inOrder.verify(viewState).onHideProgress();
    }

    @Test
    public void signUp_fail() throws Exception {
        String userName = "userName";
        String password = "password";
        String firstName = "firstName";
        String lastName = "lastName";
        String phone = "phone";
        String email = "email";

        User user = new User();
        user.setId(1L);

        when(authService.signUp(any(), any(), any(), any(), any(), any(), any())).thenReturn(Observable.error(new Throwable()));

        presenter.signUp(userName, password, firstName, lastName, phone, email);

        InOrder inOrder = Mockito.inOrder(viewState, authService);

        inOrder.verify(authService).signUp(any(), any(), any(), any(), any(), any(), any());
        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(viewState).onShowError(any());
        inOrder.verify(viewState).onHideProgress();
    }

    @Test
    public void showSelectImageDialog() throws Exception {
        presenter.showSelectImageDialog();

        verify(viewState).showPickerPhotoDialog();
    }

    @Test
    public void deleteSelectedPhotoFromLocalStorage_success() throws Exception {
        String nameImageFile = "file path";
        when(fileUtils.getDiskCacheDir(any(), anyString())).thenReturn(new File(nameImageFile));

        presenter.deleteSelectedPhotoFromLocalStorage(nameImageFile);

        verify(fileUtils).getDiskCacheDir(any(), any());
        verify(fileUtils).deleteFile(any());
    }

    @Test
    public void deleteSelectedPhotoFromLocalStorage_fail() throws Exception {
        String nameImageFile = null;

        presenter.deleteSelectedPhotoFromLocalStorage(nameImageFile);

        verify(fileUtils, never()).getDiskCacheDir(any(), any());
    }

}