package lanars.com.defigo.settings.layout;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.roomsettings.IndividualSettings;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomSettings;
import lanars.com.defigo.common.network.service.IRoomService;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import lanars.com.defigo.database.model.DBRoom;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LayoutSettingsPresenterTest {
    private LayoutSettingsPresenter presenter;

    @Mock
    private IAppPreferences appPreferences;

    @Mock
    private Router router;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IRoomService roomService;

    @Mock
    private IUserService userService;

    @Mock
    private IFileUtils fileUtils;

    @Mock
    private Context context;

    @Mock
    private LayoutSettingsView$$State layoutSettingsViewState;

    @Before
    public void setUp() {
        presenter = new LayoutSettingsPresenter(router, appPreferences, roomService, userService, userRepository, fileUtils, context, getMockRoomModel());
        presenter.setViewState(layoutSettingsViewState);
    }

    private RoomLayoutModel getMockRoomModel() {
        RoomLayoutModel roomLayoutModel = new RoomLayoutModel();
        IndividualSettings individualSettings = new IndividualSettings("", "", "");
        RoomSettings roomSettings = new RoomSettings(2L, "", "", "", true);
        roomLayoutModel.setRoomSettings(roomSettings);
        roomLayoutModel.setIndividualSettings(individualSettings);
        return roomLayoutModel;
    }

    @Test
    public void getCurrentRoomLayoutSettings_Success() {
        List<DBRoom> rooms = getStubbedRooms();
        DBRoom dbRoom = new DBRoom(2, "", "");
        rooms.add(dbRoom);
        RoomLayoutModel roomLayoutModel = new RoomLayoutModel();
        RoomSettings roomSettings = new RoomSettings();
        roomSettings.setOwnerId(0L);
        roomLayoutModel.setRoomSettings(roomSettings);

        when(userRepository.getAllRooms()).thenReturn(Flowable.just(rooms));
        when(userRepository.getRoomLayoutSettingsUser(rooms.get(0).id)).thenReturn(Observable.just(roomLayoutModel));

        presenter.getCurrentRoomLayoutSettings();

        InOrder inOrder = Mockito.inOrder(layoutSettingsViewState);
        inOrder.verify(layoutSettingsViewState).onShowProgress();
        inOrder.verify(layoutSettingsViewState).onHideProgress();
        verify(layoutSettingsViewState).changeLayoutSettingsView(anyBoolean());
    }

    @Test
    public void getCurrentRoomLayoutSettings_Error() {
        List<DBRoom> rooms = getStubbedRooms();
        String errorMessage = "error";

        when(userRepository.getAllRooms()).thenReturn(Flowable.just(rooms));
        when(userRepository.getRoomLayoutSettingsUser(rooms.get(0).id)).thenReturn(Observable.error(new Throwable(errorMessage)));

        presenter.getCurrentRoomLayoutSettings();
        InOrder inOrder = Mockito.inOrder(layoutSettingsViewState);
        inOrder.verify(layoutSettingsViewState).onShowProgress();
        inOrder.verify(layoutSettingsViewState).onHideProgress();
        inOrder.verify(layoutSettingsViewState).onShowError(errorMessage);
    }

    @Test
    public void layout_uploadImageViewSuccess() {
        when(fileUtils.checkExistFile(any(), anyString())).thenReturn(true);

        presenter.uploadStorageImageInView();

        verify(layoutSettingsViewState).uploadImageView(anyString());
    }

    @Test
    public void layout_submitIndividualSuccess() {
        List<DBRoom> rooms = getStubbedRooms();
        UpdateRoomModel updateRoomModel = mock(UpdateRoomModel.class);

        when(userRepository.getAllRooms()).thenReturn(Flowable.just(rooms));
        when(userService.updateInfoRoom(anyLong(), any(), any(), any())).thenReturn(Observable.just(updateRoomModel));

        presenter.submit(LayoutSettingsFragment.LAYOUT_INDIVIDUAL, "", "");

        InOrder inOrder = Mockito.inOrder(layoutSettingsViewState, userService);

        inOrder.verify(layoutSettingsViewState).onShowProgress();
        inOrder.verify(userService).updateInfoRoom(anyLong(), any(), any(), any());
        inOrder.verify(layoutSettingsViewState).onHideProgress();
    }

    @Test
    public void layout_submitIndividualFail() {
        List<DBRoom> rooms = getStubbedRooms();
        String errorMessage = "error";

        when(userRepository.getAllRooms()).thenReturn(Flowable.just(rooms));
        when(userService.updateInfoRoom(anyLong(), any(), any(), any())).thenReturn(Observable.error(new Throwable(errorMessage)));

        presenter.submit(LayoutSettingsFragment.LAYOUT_INDIVIDUAL, "", "");

        InOrder inOrder = Mockito.inOrder(layoutSettingsViewState);

        inOrder.verify(layoutSettingsViewState).onShowProgress();
        inOrder.verify(layoutSettingsViewState).onHideProgress();
        inOrder.verify(layoutSettingsViewState).onShowError(errorMessage);
    }

    @Test
    public void layout_submitFamilySuccess() {
        List<DBRoom> rooms = getStubbedRooms();
        UpdateRoomModel updateRoomModel = new UpdateRoomModel("", "", "", true);

        when(userRepository.getAllRooms()).thenReturn(Flowable.just(rooms));
        when(roomService.updateInfoRoom(anyLong(), any(), any(), any(), any())).thenReturn(Observable.just(updateRoomModel));

        presenter.submit(LayoutSettingsFragment.LAYOUT_FAMILY, "", "");
        InOrder inOrder = Mockito.inOrder(layoutSettingsViewState, roomService);

        inOrder.verify(layoutSettingsViewState).onShowProgress();
        inOrder.verify(roomService).updateInfoRoom(anyLong(), any(), any(), any(), any());
        inOrder.verify(layoutSettingsViewState).updateRoomSuccess(updateRoomModel);
        inOrder.verify(layoutSettingsViewState).onHideProgress();
    }

    @Test
    public void layout_submitFamilyFail() {
        List<DBRoom> rooms = getStubbedRooms();
        String errorMessage = "error";

        when(userRepository.getAllRooms()).thenReturn(Flowable.just(rooms));
        when(roomService.updateInfoRoom(anyLong(), any(), any(), any(), any())).thenReturn(Observable.error(new Throwable(errorMessage)));

        presenter.submit(LayoutSettingsFragment.LAYOUT_FAMILY, "", "");

        InOrder inOrder = Mockito.inOrder(layoutSettingsViewState);

        inOrder.verify(layoutSettingsViewState).onShowProgress();
        inOrder.verify(layoutSettingsViewState).onHideProgress();
        inOrder.verify(layoutSettingsViewState).onShowError(errorMessage);
    }

    public List<DBRoom> getStubbedRooms() {
        List<DBRoom> rooms = new ArrayList<>();
        rooms.add(new DBRoom(1, "header", "address"));
        rooms.add(new DBRoom(2, "header", "address"));
        rooms.add(new DBRoom(3, "header", "address"));
        return rooms;
    }
}