package lanars.com.defigo.settings.address;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.model.DBUser;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddressPresenterTest {

    private AddressPresenter presenter;

    @Mock
    private IAppPreferences appPreferences;

    @Mock
    private Router router;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IUserService userService;

    @Mock
    private AddressView$$State addressViewState;

    @Before
    public void setUp() throws Exception {
        presenter = new AddressPresenter(router, userRepository, appPreferences, userService);
        presenter.setViewState(addressViewState);
    }

    @Test
    public void getAddressUser_Success() throws Exception {
        long userId = 2L;
        String street = "street";
        String zip = "zip";
        String city = "city";
        String country = "country";
        DBUser dbUser = new DBUser(userId, "", "");
        dbUser.street = street;
        dbUser.zip = zip;
        dbUser.city = city;
        dbUser.country = "city";

        when(appPreferences.getCurrentUserId()).thenReturn(userId);
        when(userRepository.getUserById(userId)).thenReturn(Flowable.just(dbUser));

        presenter.getAddressUser();

        InOrder inOrder = Mockito.inOrder(addressViewState);

        inOrder.verify(addressViewState).onShowProgress();
        inOrder.verify(addressViewState).onHideProgress();
    }
    @Test
    public void submitAddress_Success() throws Exception {
        User user = mock(User.class);
        when(userService.usersMyUpdate(any())).thenReturn(Observable.just(user));

        presenter.submitAddress("street", "zip", "country", "city");

        InOrder inOrder = Mockito.inOrder(addressViewState);

        inOrder.verify(addressViewState).onShowProgress();
        inOrder.verify(addressViewState).onShowSuccessUserUpdate();
        inOrder.verify(addressViewState).onHideProgress();
    }

    @Test
    public void submitAddress_Error() throws Exception {
        String errorMessage = "error";

        when(userService.usersMyUpdate(any())).thenReturn(Observable.error(new Throwable(errorMessage)));

        presenter.submitAddress("street", "zip", "country", "city");

        InOrder inOrder = Mockito.inOrder(addressViewState);

        inOrder.verify(addressViewState).onShowProgress();
        inOrder.verify(addressViewState).onShowError(errorMessage);
        inOrder.verify(addressViewState).onHideProgress();
    }

}