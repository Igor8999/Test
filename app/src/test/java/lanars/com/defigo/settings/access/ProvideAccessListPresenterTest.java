package lanars.com.defigo.settings.access;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.repository.user.IUserRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProvideAccessListPresenterTest {

    @Mock
    private IUserRepository userRepository;

    @Mock
    private ProvideAccessListView$$State provideAccessListViewState;

    private ProvideAccessListPresenter presenter;

    @Before
    public void setUp() {
        presenter = new ProvideAccessListPresenter(null, userRepository);
        presenter.setViewState(provideAccessListViewState);
    }

    @Test
    public void getProvideUsers__shouldProvideSuccessfully() throws Exception {
        User user = mock(User.class);
        List<User> users = new ArrayList<>();
        users.add(user);

        when(userRepository.getSharedAccessByUser()).thenReturn(Observable.just(users));

        presenter.getProvideUsers();

        InOrder inOrder = Mockito.inOrder(provideAccessListViewState);

        inOrder.verify(provideAccessListViewState).onShowProgress();
        inOrder.verify(provideAccessListViewState).setProvideUsers(users);
        inOrder.verify(provideAccessListViewState).onHideProgress();
    }

    @Test
    public void getProvideUsers__shouldProvideError() throws Exception {
        when(userRepository.getSharedAccessByUser()).thenReturn(Observable.error(new Throwable()));

        presenter.getProvideUsers();

        InOrder inOrder = Mockito.inOrder(provideAccessListViewState);

        inOrder.verify(provideAccessListViewState).onShowProgress();
        inOrder.verify(provideAccessListViewState).onShowError(anyString());
        inOrder.verify(provideAccessListViewState).onHideProgress();
    }

    @Test
    public void searchUsers__shouldSearchSuccessfully() throws Exception {
        User user = mock(User.class);
        List<User> users = new ArrayList<>();
        users.add(user);

        when(userRepository.users(anyString(), anyInt(), anyInt())).thenReturn(Observable.just(users));

        presenter.searchUsers(anyString(), anyInt(), anyInt());

        //TODO add progress verification
        verify(provideAccessListViewState).setAddFoundUsers(any());
    }

    @Test
    public void searchUsers__shouldSearchError() throws Exception {
        when(userRepository.users(anyString(), anyInt(), anyInt())).thenReturn(Observable.error(new Throwable()));

        presenter.searchUsers(anyString(), anyInt(), anyInt());

        //TODO add progress verification
        verify(provideAccessListViewState).onShowError(anyString());
    }

    @Test
    public void deleteShareAccessUser_ShowError() throws Exception {
        User user = mock(User.class);
        when(userRepository.removeSharedAccess(anyLong())).thenReturn(Observable.error(new Throwable()));

        presenter.deleteShareAccessUser(user);

        //TODO add progress verification
        verify(provideAccessListViewState).onShowError(anyString());
    }

    @Test
    public void deleteShareAccessUser_ShowSuccess() throws Exception {
        User user = mock(User.class);
        StatusModel statusModel = mock(StatusModel.class);
        when(userRepository.removeSharedAccess(anyLong())).thenReturn(Observable.just(statusModel));

        presenter.deleteShareAccessUser(user);

        //TODO add progress verification
        verify(provideAccessListViewState).onDeleteUser(user);
    }
}
