package lanars.com.defigo.settings.phonenumber;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.model.DBUser;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhoneNumberPresenterTest {

    private PhoneNumberPresenter presenter;

    @Mock
    private IAppPreferences appPreferences;

    @Mock
    private Router router;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IUserService userService;

    @Mock
    private PhoneNumberView$$State phoneNumberViewState;

    @Before
    public void setUp() {
        presenter = new PhoneNumberPresenter(router, userRepository, appPreferences, userService);
        presenter.setViewState(phoneNumberViewState);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    @Test
    public void getPhoneNumberUser_Success() throws Exception {
        DBUser dbUser = new DBUser(1, "", "");
        String phoneNumber = "number";
        dbUser.phone = phoneNumber;

        when(appPreferences.getCurrentUserId()).thenReturn(2L);
        when(userRepository.getUserById(2)).thenReturn(Flowable.just(dbUser));

        presenter.getPhoneNumberUser();

        InOrder inOrder = Mockito.inOrder(phoneNumberViewState);

        inOrder.verify(phoneNumberViewState).onShowProgress();
        inOrder.verify(phoneNumberViewState).onHideProgress();
    }

    @Test
    public void getPhoneNumberUser_Error() throws Exception {
        when(appPreferences.getCurrentUserId()).thenReturn(2L);
        when(userRepository.getUserById(2)).thenReturn(Flowable.error(new Throwable()));

        presenter.getPhoneNumberUser();
        InOrder inOrder = Mockito.inOrder(phoneNumberViewState);

        inOrder.verify(phoneNumberViewState).onShowProgress();
        inOrder.verify(phoneNumberViewState).checkPermissionAndGetPhoneNumber();
        inOrder.verify(phoneNumberViewState).onHideProgress();
    }

    @Test
    public void changePhoneNumber_Success() throws Exception {
        String phoneNumber = "12345";
        User user = mock(User.class);
        when(userService.usersMyUpdate(any())).thenReturn(Observable.just(user));

        presenter.changePhoneNumber(phoneNumber);

        InOrder inOrder = Mockito.inOrder(phoneNumberViewState);

        inOrder.verify(phoneNumberViewState).onShowProgress();
        inOrder.verify(phoneNumberViewState).onShowSuccessUserUpdate();
        inOrder.verify(phoneNumberViewState).onHideProgress();

    }

    @Test
    public void changePhoneNumber_Error() throws Exception {
        String phoneNumber = "12345";
        String errorMessage = "error";

        when(userService.usersMyUpdate(any())).thenReturn(Observable.error(new Throwable(errorMessage)));

        presenter.changePhoneNumber(phoneNumber);

        InOrder inOrder = Mockito.inOrder(phoneNumberViewState);

        inOrder.verify(phoneNumberViewState).onShowProgress();
        inOrder.verify(phoneNumberViewState).onShowError(errorMessage);
        inOrder.verify(phoneNumberViewState).onHideProgress();
    }

}
