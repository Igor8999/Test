package lanars.com.defigo.settings.email;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.model.DBUser;
import ru.terrakok.cicerone.Router;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmailSettingsPresenterTest {

    private EmailSettingsPresenter presenter;

    @Mock
    private IAppPreferences appPreferences;

    @Mock
    private Router router;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IUserService userService;

    @Mock
    private EmailSettingsView$$State viewState;

    @Before
    public void setUp() throws Exception {
        presenter = new EmailSettingsPresenter(router, userRepository, appPreferences, userService);
        presenter.setViewState(viewState);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    @Test
    public void getEmailUser_success() throws Exception {
        DBUser dbUser = new DBUser(2, "", "");
        String email = "email";
        dbUser.email = email;
        long userId = 3L;

        when(appPreferences.getCurrentUserId()).thenReturn(userId);
        when(userRepository.getUserById(userId)).thenReturn(Flowable.just(dbUser));

        presenter.getEmailUser();

        InOrder inOrder = Mockito.inOrder(viewState);

        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(viewState).onHideProgress();
        inOrder.verify(viewState).showUserEmail(anyString());
    }

    @Test
    public void getEmailUser_Error() throws Exception {
        long userId = 2L;
        String errorMessage = "error";

        when(appPreferences.getCurrentUserId()).thenReturn(userId);
        when(userRepository.getUserById(userId)).thenReturn(Flowable.error(new Throwable(errorMessage)));

        presenter.getEmailUser();

        InOrder inOrder = Mockito.inOrder(viewState);
        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(viewState).onHideProgress();
        inOrder.verify(viewState).onShowError(any());
    }

    @Test
    public void changeEmail_Success() throws Exception {
        User user = mock(User.class);
        String email = "email@email.ua";
        when(userService.usersMyUpdate(any())).thenReturn(Observable.just(user));
        presenter.changeEmail(email);

        InOrder inOrder = Mockito.inOrder(viewState);

        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(viewState).onShowSuccessUserUpdate();
        inOrder.verify(viewState).onHideProgress();
    }

    @Test
    public void changeEmail_ValidateError() throws Exception {
        String email = "email.ua";
        presenter.changeEmail(email);

        verify(viewState).enterCorrectEmailAlert();
    }

    @Test
    public void changeEmail_Error() throws Exception {
        String errorMessage = "error";
        when(userService.usersMyUpdate(any())).thenReturn(Observable.error(new Throwable(errorMessage)));
        presenter.changeEmail("email@ll.ua");

        InOrder inOrder = Mockito.inOrder(viewState);

        inOrder.verify(viewState).onShowProgress();
        inOrder.verify(viewState).onShowError(errorMessage);
        inOrder.verify(viewState).onHideProgress();
    }

}