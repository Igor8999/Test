package lanars.com.defigo.home;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.AvailableDoorModel;
import lanars.com.defigo.common.network.models.TypeAvalableDoorbells;
import lanars.com.defigo.common.network.service.IDoorbellService;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class HomeFragmentPresenter extends BasePresenter<HomeView> {

    @Inject
    Router router;

    @Inject
    IDoorbellService doorbellService;

    @Inject
    NetworkClientUtil networkClientUtil;

    public HomeFragmentPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void reciveAllDoors() {
        addToDisposable(doorbellService.getAvailableDoorbells()
                .flatMap(this::onHandleCodeResponse)
                .subscribe(this::onSuccessGetAvailableDoorbells, this::onErrorGetAvailableDoorbells));
    }

    private <T> ObservableSource<T> onHandleCodeResponse(Response<T> response) {
        return networkClientUtil.onHandleCodeResponse(response);
    }

    private void onSuccessGetAvailableDoorbells(List<AvailableDoorModel> availableDoorModels) {
        getViewState().setAvailableDoorbells(availableDoorModels);
    }

    private void onErrorGetAvailableDoorbells(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void openDoor(AvailableDoorModel availableDoorModel, int position) {
        addToDisposable(doorbellService.openDoorbell(availableDoorModel.getId())
                .delay(4000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().changeStatusAvailableDoor(position, TypeAvalableDoorbells.STATE_OPEN))
                .doAfterTerminate(() -> getViewState().changeStatusAvailableDoor(position, TypeAvalableDoorbells.STATE_CLOSE))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOpenDoors, this::onErrorOpenDoors));

    }

    private void onSuccessOpenDoors(ResponseBody responseBody) {

    }

    private void onErrorOpenDoors(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }
}
