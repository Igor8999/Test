package lanars.com.defigo.home;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.AvailableDoorModel;

interface HomeView extends BaseMvpView {

    void setAvailableDoorbells(List<AvailableDoorModel> availableDoorbells);

    void changeStatusAvailableDoor(int position, int stateClose);

}
