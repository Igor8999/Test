package lanars.com.defigo.home.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.AvailableDoorModel;
import lanars.com.defigo.common.network.models.TypeAvalableDoorbells;
import lanars.com.defigo.common.ui.listener.OnItemClickAvailebleDoors;

public class AvailableDoorsAdapter extends RecyclerView.Adapter<AvailableDoorsAdapter.ViewHolder> {

    private List<AvailableDoorModel> availableDoorModels;
    private OnItemClickAvailebleDoors onItemClickAvailebleDoors;

    public AvailableDoorsAdapter(List<AvailableDoorModel> availableDoorModels, OnItemClickAvailebleDoors onItemClickAvailebleDoors) {
        this.availableDoorModels = availableDoorModels;
        this.onItemClickAvailebleDoors = onItemClickAvailebleDoors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TypeAvalableDoorbells.STATE_CLOSE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_available_doors_close, parent, false);
                ViewHolder viewHolderClose = new ViewHolder(view);
                viewHolderClose.itemView.setOnClickListener(v -> setOnClick(viewHolderClose));
                return viewHolderClose;
            case TypeAvalableDoorbells.STATE_OPEN:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_available_doors_open, parent, false);
                ViewHolder viewHolderOpen = new ViewHolder(view);
                viewHolderOpen.itemView.setOnClickListener(v -> setOnClick(viewHolderOpen));
                return viewHolderOpen;
            default:
                return null;
        }
    }

    private void setOnClick(ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onItemClickAvailebleDoors.onItemClick(position);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AvailableDoorModel availableDoorModel = availableDoorModels.get(position);
        holder.tvAddressDoor.setText(availableDoorModel.getAddress());
        holder.tvNumber.setText(String.format("%s.", position + 1));
    }

    @Override
    public int getItemCount() {
        return availableDoorModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgStatusActivate)
        ImageView imgStatusActivate;

        @BindView(R.id.tvAddressDoor)
        TextView tvAddressDoor;

        @BindView(R.id.tvStatusOpened)
        TextView tvStatusOpened;

        @BindView(R.id.tvNumber)
        TextView tvNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return availableDoorModels.get(position).getStateProvideAccess();
    }

    public List<AvailableDoorModel> getAvailableDoorModels() {
        return availableDoorModels;
    }
}
