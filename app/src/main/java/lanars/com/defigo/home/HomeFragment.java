package lanars.com.defigo.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.AvailableDoorModel;
import lanars.com.defigo.common.network.models.TypeAvalableDoorbells;
import lanars.com.defigo.common.ui.listener.OnItemClickAvailebleDoors;
import lanars.com.defigo.home.adapter.AvailableDoorsAdapter;

public class HomeFragment extends MvpAppCompatFragment implements HomeView, OnItemClickAvailebleDoors {

    @BindView(R.id.rvDoors)
    RecyclerView rvDoors;

    @BindView(R.id.empty_view)
    View empty_view;

    @InjectPresenter
    HomeFragmentPresenter homeFragmentPresenter;

    private AvailableDoorsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        homeFragmentPresenter.reciveAllDoors();
    }

    @Override
    public void onShowError(String errorMessage) {

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

    @Override
    public void setAvailableDoorbells(List<AvailableDoorModel> availableDoorbells) {
        if (availableDoorbells.isEmpty()) {
            return;
        }
        empty_view.setVisibility(View.GONE);
        adapter = new AvailableDoorsAdapter(availableDoorbells, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvDoors.setLayoutManager(linearLayoutManager);
        rvDoors.setAdapter(adapter);
        rvDoors.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onItemClick(int position) {
        AvailableDoorModel availableDoorModel = adapter.getAvailableDoorModels().get(position);
        if (availableDoorModel.getStateProvideAccess() == TypeAvalableDoorbells.STATE_OPEN) {
            return;
        }
        homeFragmentPresenter.openDoor(availableDoorModel, position);
    }

    @Override
    public void changeStatusAvailableDoor(int position, int stateClose) {
        adapter.getAvailableDoorModels().get(position).setStateProvideAccess(stateClose);
        adapter.notifyItemChanged(position);
    }
}
