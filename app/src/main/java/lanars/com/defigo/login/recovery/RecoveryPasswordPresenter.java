package lanars.com.defigo.login.recovery;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.service.IAuthService;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class RecoveryPasswordPresenter extends BasePresenter<RecoveryPasswordView> {

    @Inject
    Router router;

    @Inject
    IAuthService authService;

    public RecoveryPasswordPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    RecoveryPasswordPresenter(IAuthService authService) {
        this.authService = authService;
    }

    public void sendEmailRecovery(String email) {
        if (validateEmail(email)) {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);

            getViewState().onShowProgress();

            addToDisposable(
                    authService.recoveryPassword(params)
                            .doAfterTerminate(() -> getViewState().onHideProgress())
                            .ignoreElements()
                            .subscribe(this::onSuccessSendEmail, this::onErrorSendEmail)
            );
        }
    }

    @VisibleForTesting
    boolean validateEmail(String email) {
        if (TextUtils.isEmpty(email) || !email.contains("@")) {
            getViewState().enterCorrectEmailAlert();
            return false;
        }
        return true;
    }

    private void onErrorSendEmail(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessSendEmail() {
        getViewState().sendEmailRecoverySuccess();

    }

    public void goToLogin() {
        router.exit();
    }

    public void onBack() {
        router.exit();
    }
}
