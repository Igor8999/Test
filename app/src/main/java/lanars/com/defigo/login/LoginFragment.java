package lanars.com.defigo.login;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseFragment;
import lanars.com.defigo.common.utils.EditTextClearUtil;
import lanars.com.defigo.common.utils.Utils;

public class LoginFragment extends BaseFragment implements LoginFragmentView {

    @BindView(R.id.etPassword)
    EditText etPass;

    @BindView(R.id.etUsername)
    EditText etUsername;

    @BindView(R.id.btnRegister)
    TextView btnRegister;

    @BindView(R.id.btnLogin)
    TextView btnLogin;

    @InjectPresenter
    LoginFragmentPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Drawable clearDrawable = getActivity().getDrawable(R.drawable.close_icon);
        getEtClearUtil(clearDrawable, etUsername);
        getEtClearUtil(clearDrawable, etPass);
        etUsername.requestFocus();
        //todo need merge in one chain
        chekInputTextField(etUsername);
        chekInputTextField(etPass);
    }

    private void chekInputTextField(EditText editText) {
        presenter.addToDisposable(
                RxTextView.textChanges(editText)
                        .skipInitialValue()
                        .subscribe(charSequence -> checkEmailInput(charSequence, editText)));
    }

    private void checkEmailInput(CharSequence charSequence, EditText editText) {
        switch (editText.getId()) {
            case R.id.etEmail:
                if (!charSequence.toString().equals("")) {
                    etUsername.setBackground(getContext().getDrawable(R.drawable.error_edit_text));
                    return;
                }
                etUsername.setBackground(getContext().getDrawable(R.drawable.ed_selector));
                break;
            case R.id.etPassword:
                if (charSequence.toString().length() < 6 && !charSequence.toString().equals("")) {
                    etPass.setBackground(getContext().getDrawable(R.drawable.error_edit_text));
                    return;
                }
                etPass.setBackground(getContext().getDrawable(R.drawable.ed_selector));
                break;
        }
    }

    private EditTextClearUtil getEtClearUtil(Drawable clearDrawable, EditText editText) {
        return new EditTextClearUtil.Builder(getContext())
                .setEditText(editText)
                .setClearDrawable(clearDrawable)
                .build();
    }

    @OnClick(R.id.btnRegister)
    void goToRegistration() {
        presenter.goToRegistration();
    }

    @OnClick(R.id.btnLogin)
    void login() {
        presenter.signIn(etUsername.getText().toString(), etPass.getText().toString());
    }

    @OnClick(R.id.tvForgotPassword)
    void goToRecoveryPassword() {
        presenter.goToRecoveryPassword();
    }

    @Override
    public void onSuccessLogin() {
        presenter.goToTutorialScreen();
    }

    @Override
    public void hideKeyboard() {
        Utils.hideKeyboard(getActivity());
    }

    @Override
    public void openAdminApp() {
        presenter.openAdminApp();
    }
}