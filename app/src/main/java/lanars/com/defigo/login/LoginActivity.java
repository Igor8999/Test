package lanars.com.defigo.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseActivity;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.cropimage.CropFragment;
import lanars.com.defigo.login.recovery.RecoveryPasswordFragment;
import lanars.com.defigo.main.MainActivity;
import lanars.com.defigo.register.RegistrationFragment;
import lanars.com.defigo.tutorial.TutorialActivity;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;

public class LoginActivity extends MvpAppCompatActivity implements LoginActivityView {
    @InjectPresenter
    LoginActivityPresenter presenter;

    @Inject
    NavigatorHolder navigatorHolder;

    public static void start(Context context) {
        Intent starter = constructIntent(context);
        context.startActivity(starter);
    }

    public static Intent constructIntent(Context context) {
        Intent starter = new Intent(context, LoginActivity.class);

        starter.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return starter;
    }

    private Navigator navigator = new SupportAppNavigator(this, R.id.loginContainer) {

        @Override
        protected Intent createActivityIntent(Context context, String screenKey, Object data) {
            switch (screenKey) {
                case RouteScreenName.MAIN:
                    return new Intent(context, MainActivity.class);
                case RouteScreenName.ADMIN:
                    return new Intent(context, AdminBaseActivity.class);
                case RouteScreenName.TUTORIAL_ACTIVITY:
                    return new Intent(context, TutorialActivity.class);
            }
            return null;
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case RouteScreenName.LOGIN:
                    return new LoginFragment();

                case RouteScreenName.REGISTRATION:
                    return new RegistrationFragment();

                case RouteScreenName.CROP_FRAGMENT:
                    return CropFragment.newInstance((Bundle) data);

                case RouteScreenName.RECOVERY_PASSWORD:
                    return new RecoveryPasswordFragment();
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void openLogin() {
        presenter.goToLogin();
    }

    @Override
    public void onShowError(String errorMessage) {

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

    @Override
    public void openMainScreen() {
        presenter.openMainScreen();
        finish();
    }

    @Override
    public void openAdminApp() {
        presenter.openAdminApp();
        finish();
    }
}
