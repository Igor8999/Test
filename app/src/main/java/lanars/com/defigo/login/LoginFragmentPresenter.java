package lanars.com.defigo.login;

import android.support.annotation.VisibleForTesting;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.iid.FirebaseInstanceId;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.UsersRoom;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.network.service.IDeviceService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.DeviceIdUtil;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.converter.RoomConverter;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.database.model.DBUser;
import lanars.com.defigo.settings.sound.util.SoundSaverUtil;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class LoginFragmentPresenter extends BasePresenter<LoginFragmentView> {

    @Inject
    Router router;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IUserRepository userRepository;

    @Inject
    IAuthService authService;

    @Inject
    IDeviceService deviceService;

    @Inject
    SocketManager socketManager;

    public LoginFragmentPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    LoginFragmentPresenter(Router router, IAppPreferences appPreferences, IUserRepository userRepository, IAuthService authService, SocketManager socketManager) {
        this.router = router;
        this.appPreferences = appPreferences;
        this.userRepository = userRepository;
        this.authService = authService;
        this.socketManager = socketManager;
    }

    public void signIn(final String payload, final String password) {
        Map<String, String> mapParams = new HashMap<>();
        mapParams.put("payload", payload);
        mapParams.put("password", password);

        addToDisposable(
                authService
                        .signIn(mapParams)
                        .doOnSubscribe(disposable -> getViewState().hideKeyboard())
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .doOnNext(saveAccessToken())
                        .flatMap(getCurrentUserProfile())
                        .doOnNext(saveCurrentUserId())
                        .doOnNext(this::convertAndSaveRoomsInDb)
                        .map(UserConverter::convertToDatabaseUser)
                        .doOnNext(saveUserInDatabase())
                        .doOnNext(this::updateDeviceId)
                        .doOnNext(this::checkSoundFile)
                        .subscribe(this::onSuccessLogin, this::onErrorLogin));
    }

    private void convertAndSaveRoomsInDb(User user) {
        List<DBRoom> dbRooms = new ArrayList<>();
        DBRoom[] dbRoomArray = new DBRoom[user.getUsersRooms().size()];
        for (UsersRoom userRoom : user.getUsersRooms()) {
            dbRooms.add(RoomConverter.convertToDatabaseRoom(userRoom));
        }
        dbRooms.toArray(dbRoomArray);
        saveUserRoom(dbRoomArray);
    }

    private void saveUserRoom(DBRoom... dbRoom) {
        userRepository.insertRooms(dbRoom);
    }

    @NonNull
    private Consumer<SignModel> saveAccessToken() {
        return signModel -> appPreferences.saveAccessToken(signModel.getAccessToken());
    }

    @NonNull
    private Function<Object, ObservableSource<User>> getCurrentUserProfile() {
        return signModelResponse -> userRepository.usersMy();
    }

    @NonNull
    private Consumer<DBUser> saveUserInDatabase() {
        return user -> userRepository.insertUsers(user);
    }

    @NonNull
    private Consumer<User> saveCurrentUserId() {
        return user -> appPreferences.saveCurrentUserId(user.getId());
    }

    private void onSuccessLogin(DBUser dbUser) {
        if (dbUser.role == User.ROLE_ADMIN) {
            getViewState().openAdminApp();
            return;
        }

        onSuccessLogin();
    }

    @VisibleForTesting
    void updateDeviceId(DBUser dbUser) {
        DeviceIdUtil.updateDeviceId(deviceService, provideFirebaseToken(), provideDeviceId());
    }

    private void checkSoundFile(DBUser dbUser) {
        SoundSaverUtil.checkIfSoundExist(dbUser.currentSound, appPreferences);
    }

    private void onSuccessLogin() {
        socketManager.connectSocket(appPreferences.getAccessToken(), appPreferences.getCurrentUserId(), null);
        getViewState().onSuccessLogin();
    }

    private String provideDeviceId() {
        return FirebaseInstanceId.getInstance().getId();
    }

    private String provideFirebaseToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    private void onErrorLogin(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void goToTutorialScreen() {
        Logger.d("login activity goToTutorialScreen");

        //TODO return opening tutorial
        router.newRootScreen(RouteScreenName.MAIN);
    }

    public void goToRecoveryPassword() {
        router.navigateTo(RouteScreenName.RECOVERY_PASSWORD, null);
    }

    public void goToRegistration() {
        router.navigateTo(RouteScreenName.REGISTRATION, null);
    }

    public void openAdminApp() {
        Logger.d(LoginFragmentPresenter.class.getName() + " openAdminApp");
        router.newRootScreen(RouteScreenName.ADMIN);
    }
}
