package lanars.com.defigo.login;

import lanars.com.defigo.common.base.BaseMvpView;

public interface LoginFragmentView extends BaseMvpView {
    void onSuccessLogin();

    void openAdminApp();

    void hideKeyboard();
}
