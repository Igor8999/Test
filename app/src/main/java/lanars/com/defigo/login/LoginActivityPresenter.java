package lanars.com.defigo.login;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.model.DBUser;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class LoginActivityPresenter extends BasePresenter<LoginActivityView> {

    @Inject
    Router router;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    SocketManager socketManager;

    @Inject
    IUserRepository userRepository;
    private Disposable getUserDisposable;

    public LoginActivityPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getUserDisposable = userRepository
                .getUserById(appPreferences.getCurrentUserId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessGetUser, this::onError);
    }

    private void disposeUserRepositorySubscription() {
        if (getUserDisposable != null && !getUserDisposable.isDisposed()) {
            getUserDisposable.dispose();
        }
    }

    private void onSuccessGetUser(DBUser user) {
        this.disposeUserRepositorySubscription();

        if (didUserLogin()) {
            if (checkUserIsAdmin(user)) {
                getViewState().openAdminApp();
                return;
            }

            getViewState().openMainScreen();
        } else {
            getViewState().openLogin();
        }
    }

    private void onError(Throwable throwable) {
        this.disposeUserRepositorySubscription();

        throwable.printStackTrace();
        getViewState().openLogin();
        Crashlytics.logException(throwable);
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void openMainScreen() {
        Logger.d("login activity openMainScreen");
        socketManager.connectSocket(appPreferences.getAccessToken(), appPreferences.getCurrentUserId(), null);
        router.navigateTo(RouteScreenName.MAIN);
    }

    public void goToLogin() {
        router.newRootScreen(RouteScreenName.LOGIN);
    }

    public boolean didUserLogin() {
        String token = appPreferences.getAccessToken();

        if (!TextUtils.isEmpty(token)) {
            return true;
        }

        return false;
    }

    public void openAdminApp() {
        router.navigateTo(RouteScreenName.ADMIN);
    }

    public boolean checkUserIsAdmin(DBUser user) {
        return user.role == User.ROLE_ADMIN;
    }
}
