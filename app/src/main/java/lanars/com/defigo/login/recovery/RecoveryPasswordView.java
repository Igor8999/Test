package lanars.com.defigo.login.recovery;

import lanars.com.defigo.common.base.BaseMvpView;

public interface RecoveryPasswordView extends BaseMvpView {

    void sendEmailRecoverySuccess();

    void enterCorrectEmailAlert();
}
