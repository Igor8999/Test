package lanars.com.defigo.login;

import lanars.com.defigo.common.base.BaseMvpView;

public interface LoginActivityView extends BaseMvpView {
    void openMainScreen();

    void openAdminApp();

    void openLogin();
}
