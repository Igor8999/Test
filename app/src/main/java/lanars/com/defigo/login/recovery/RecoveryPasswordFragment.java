package lanars.com.defigo.login.recovery;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.Utils;

public class RecoveryPasswordFragment extends MvpAppCompatFragment implements RecoveryPasswordView {

    @BindView(R.id.input_email_layout)
    View input_email_layout;

    @BindView(R.id.continue_recovery_layout)
    View continue_recovery_layout;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @InjectPresenter
    RecoveryPasswordPresenter recoveryPasswordPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recovery_password_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnSend)
    void sendEmailRecovery() {
        Utils.hideKeyboard(getActivity());
        recoveryPasswordPresenter.sendEmailRecovery(etEmail.getText().toString());
    }

    @OnClick(R.id.btnContinue)
    void continueRecovery() {
        recoveryPasswordPresenter.goToLogin();
    }

    @OnClick(R.id.imgBack)
    void onBack() {
        recoveryPasswordPresenter.onBack();
    }

    @Override
    public void sendEmailRecoverySuccess() {
        input_email_layout.setVisibility(View.GONE);
        continue_recovery_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void enterCorrectEmailAlert() {
        Alert.create(getActivity())
                .setText(getString(R.string.enter_email))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void onShowError(String errorMessage) {
        Alert.create(getActivity())
                .setText(errorMessage)
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }
}
