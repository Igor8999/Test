package lanars.com.defigo.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.orhanobut.logger.Logger;

import java.io.File;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import lanars.com.defigo.BuildConfig;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.call.CallPreviewActivity;
import lanars.com.defigo.common.utils.IAppPreferences;

public class TrackerMessagingService extends FirebaseMessagingService {
    private static final String CALL_CHANNEL_ID = "CALL_CHANNEL_ID";

    public final static String KEY_ADDRESS = "address";
    public final static String KEY_CALL_UUID = "callUuid";
    public final static String KEY_DOORBELL = "doorbell";
    public final static String KEY_DOORBELL_ID = "doorbellId";
    public final static String KEY_VIDEO_ENABLED = "hasVideo";
    public final static String KEY_FROM_PUSH = "fromPush";

    public static final int CALL_NOTIFICATION_ID = 10;

    @Inject
    IAppPreferences appPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            handleIntentInternally(remoteMessage.getData());
            Logger.d("onMessageReceived " + remoteMessage.getData().toString());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Crashlytics.logException(throwable);
        }
    }

    private void handleIntentInternally(Map<String, String> data) {
        if (data == null) {
            Crashlytics.logException(new Throwable("Empty bundle extras at TrackerMessagingService"));
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createCallNotificationChannel();
        }

        if (DefigoApplication.getInstance().isRunning()) {
            return;
        }

        Bundle bundle = new Bundle();

        bundle.putString(KEY_CALL_UUID, data.get(KEY_CALL_UUID));
        bundle.putString(KEY_DOORBELL, data.get(KEY_DOORBELL));
        bundle.putString(KEY_DOORBELL_ID, data.get(KEY_DOORBELL_ID));
        bundle.putString(KEY_ADDRESS, data.get(KEY_ADDRESS));
        bundle.putBoolean(KEY_VIDEO_ENABLED, parseIsVideoEnabledFlag(data));
        bundle.putBoolean(KEY_FROM_PUSH, true);

        handleReceivedMessage(bundle);
    }

    private boolean parseIsVideoEnabledFlag(Map<String, String> data) {
        String stringIsVideoEnabled = data.get(KEY_VIDEO_ENABLED);
        boolean isVideoEnabled = false;

        if (!TextUtils.isEmpty(stringIsVideoEnabled)) {
            isVideoEnabled = Boolean.parseBoolean(stringIsVideoEnabled);
        }

        return isVideoEnabled;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createCallNotificationChannel() {
        setupChannel();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannel() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.deleteNotificationChannel(CALL_CHANNEL_ID);
        }

        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_HIGH;

        Uri soundUri = this.getSoundFileUri(appPreferences.getCurrentSoundFilePath());

        NotificationChannel notificationChannel = new NotificationChannel(CALL_CHANNEL_ID, name, importance);
        notificationChannel.setDescription(description);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.BLUE);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        notificationChannel.setSound(soundUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private Uri getSoundFileUri(String currentSoundFilePath) {
        if (TextUtils.isEmpty(currentSoundFilePath)) {
            return Uri.parse("android.resource://" + getPackageName() + "/raw/dingdong");
        }

        Uri uriForFile = FileProvider.getUriForFile(
                this,
                BuildConfig.APPLICATION_ID + ".file.provider",
                new File(currentSoundFilePath)
        );

        grantUriPermission("com.android.systemui", uriForFile, Intent.FLAG_GRANT_READ_URI_PERMISSION);

        return uriForFile;
    }

    private void handleReceivedMessage(Bundle bundle) {
        Random randomPendingRequestCode = new Random(10000);
        Random randomPendingRequestCodeDecline = new Random(20000);

        int requestCodeAnswer = Math.abs(randomPendingRequestCode.nextInt());
        int requestCodeDecline = Math.abs(randomPendingRequestCodeDecline.nextInt());

        Notification.Builder notificationBuilder = createNotificationBuilder();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Uri soundUri = this.getSoundFileUri(appPreferences.getCurrentSoundFilePath());
            notificationBuilder.setSound(soundUri);
        }

        notificationBuilder.setSmallIcon(R.drawable.language_ic)
                .setPriority(Notification.PRIORITY_HIGH)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setContentTitle(String.format("%s %s", bundle.getString(KEY_DOORBELL), getString(R.string.call_notification_title)));

        Intent answerIntent = CallPreviewActivity.constructIntent(this, bundle);
        Intent declineIntent = DismissNotificationService.constructIntent(this, CALL_NOTIFICATION_ID, bundle.getString(KEY_CALL_UUID));

        PendingIntent answerPendingIntent = PendingIntent.getActivity(this, requestCodeAnswer, answerIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent declinePendingIntent = PendingIntent.getService(this, requestCodeDecline, declineIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        notificationBuilder
                .setContentText(bundle.getString(KEY_ADDRESS))
                .addAction(R.drawable.ic_call, getString(R.string.answer), answerPendingIntent)
                .addAction(R.drawable.ic_call_end, getString(R.string.decline), declinePendingIntent)
                .setFullScreenIntent(answerPendingIntent, true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(CALL_NOTIFICATION_ID, notificationBuilder.build());
    }

    @NonNull
    private Notification.Builder createNotificationBuilder() {
        Notification.Builder notificationBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder = new Notification.Builder(this, CALL_CHANNEL_ID);
        } else {
            notificationBuilder = new Notification.Builder(this);
        }
        return notificationBuilder;
    }
}
