package lanars.com.defigo.push;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.network.service.IDeviceService;
import lanars.com.defigo.common.utils.DeviceIdUtil;

public class TrackerIdService extends FirebaseInstanceIdService {

    @Inject
    IDeviceService deviceService;

    @Override
    public void onCreate() {
        super.onCreate();
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onTokenRefresh() {
        sendRegistrationToServer();
    }

    private void sendRegistrationToServer() {
        Logger.d("Refreshed token:");
        DeviceIdUtil.updateDeviceId(deviceService);
    }
}
