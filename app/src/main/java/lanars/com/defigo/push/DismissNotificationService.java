package lanars.com.defigo.push;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;

import lanars.com.defigo.call.SocketService;

public class DismissNotificationService extends IntentService {
    public static final String NOTIFICATION_ID = "id";
    public static final String CALL_UUID = "call_uuid";

    public static Intent constructIntent(Context context, int notificationId, String calUuid) {
        Intent serviceIntent = new Intent(context, DismissNotificationService.class);
        serviceIntent.putExtra(NOTIFICATION_ID, notificationId);
        serviceIntent.putExtra(CALL_UUID, calUuid);

        return serviceIntent;
    }

    public DismissNotificationService() {
        super(DismissNotificationService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        int notificationId = intent.getIntExtra(NOTIFICATION_ID, 0);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.cancel(notificationId);

        String callUuid = intent.getStringExtra(CALL_UUID);

        if (TextUtils.isEmpty(callUuid)) {
            return;
        }

        sendBrakeCallEventToServer(callUuid);
    }

    private void sendBrakeCallEventToServer(String callUuid) {
        Bundle bundle = new Bundle();

        bundle.putInt(SocketService.SOCKET_EVENT_KEY, SocketService.SEND_BRAKE_CALL_EVENT);
        bundle.putString(SocketService.CALL_UUID_KEY, callUuid);

        SocketService.start(this, bundle);
    }
}
