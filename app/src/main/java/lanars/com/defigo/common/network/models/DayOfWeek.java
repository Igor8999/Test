package lanars.com.defigo.common.network.models;

import android.os.Parcel;
import android.os.Parcelable;

public class DayOfWeek implements Parcelable {

    private String name;

    private int index;

    private boolean isSelected;

    public DayOfWeek(String name, int index, boolean isSelected) {
        this.name = name;
        this.index = index;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public DayOfWeek(String name, int index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.index);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
    }

    protected DayOfWeek(Parcel in) {
        this.name = in.readString();
        this.index = in.readInt();
        this.isSelected = in.readByte() != 0;
    }

    public static final Parcelable.Creator<DayOfWeek> CREATOR = new Parcelable.Creator<DayOfWeek>() {
        @Override
        public DayOfWeek createFromParcel(Parcel source) {
            return new DayOfWeek(source);
        }

        @Override
        public DayOfWeek[] newArray(int size) {
            return new DayOfWeek[size];
        }
    };
}
