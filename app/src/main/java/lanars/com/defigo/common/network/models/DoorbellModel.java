package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DoorbellModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("isPublic")
    @Expose
    private Boolean isPublic;
    @SerializedName("buildingId")
    @Expose
    private Integer buildingId;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Integer getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
        this.buildingId = buildingId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}


