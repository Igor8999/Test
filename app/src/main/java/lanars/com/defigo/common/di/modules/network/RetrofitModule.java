package lanars.com.defigo.common.di.modules.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lanars.com.defigo.BuildConfig;
import lanars.com.defigo.common.utils.IAppPreferences;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Retrofit.Builder builder, OkHttpClient okHttpClient) {
        return builder
                .client(okHttpClient)
                .baseUrl(BuildConfig.SERVER_URL).build();
    }

    @Provides
    @Singleton
    public Retrofit.Builder provideRetrofitBuilder(Converter.Factory converterFactory) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(converterFactory);
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient(HttpLoggingInterceptor loggerInterceptor, AuthTokenInterceptor authTokenInterceptor) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder
                .addInterceptor(authTokenInterceptor)
                .addInterceptor(loggerInterceptor)
                .connectTimeout(30, TimeUnit.SECONDS);
        return httpClientBuilder.build();
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideLoggerInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    public AuthTokenInterceptor provideAuthTokenInterceptor(IAppPreferences appPreferences) {
        return new AuthTokenInterceptor(appPreferences);
    }

    @Provides
    @Singleton
    public Converter.Factory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);

    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }
}
