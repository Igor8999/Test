package lanars.com.defigo.common.utils;

import android.util.Log;

public class AppConst {

    static final String ACCESS_TOKEN = "access_token";
    static final String CURRENT_USER_ID = "current_user_id";

    public static final String DOORBEL_VISIBILITY = "doorbell";
    public static final String SEQUENCE_TYPE = "sequence_type";
    public static final String KEY_MUTE_DAYS = "key_mute_days";
    public static final String OPEN_DOORS_TYPE = "open_doors_type";
    public static final String PROVIDE_USER = "provide_user";
    public static final String TYPE_PIN = "type_pin";

    //admin
    public static final String BUILDING_ID = "building_model_id";
    public static final String BUILDING_NAME = "building_model_name";
    public static final String DWELLER_USER = "dweller_user";
    public static final String CARD = "card";
    public static final String USER_ID = "user_id";


    public static void Logger(String msg) {
        Log.i("igor", msg);
    }
}
