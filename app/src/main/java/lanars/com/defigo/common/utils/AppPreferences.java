package lanars.com.defigo.common.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import lanars.com.defigo.common.network.models.DayOfWeek;

public class AppPreferences implements IAppPreferences {
    public static final String KEY_CURRENT_SOUND = "KEY_CURRENT_SOUND";
    public static final String KEY_CURRENT_SOUND_URL = "KEY_CURRENT_SOUND_URL";

    private SharedPreferences pref;
    private Gson gson;

    public AppPreferences(Context appContext, Gson gson) {
        this.pref = PreferenceManager.getDefaultSharedPreferences(appContext);
        this.gson = gson;
    }

    public boolean saveAccessToken(String accessToken) {
        return pref
                .edit().putString(AppConst.ACCESS_TOKEN, accessToken)
                .commit();
    }

    public String getAccessToken() {
        return pref.getString(AppConst.ACCESS_TOKEN, null);
    }

    public boolean saveCurrentUserId(long currentUserId) {
        return pref
                .edit()
                .putLong(AppConst.CURRENT_USER_ID, currentUserId)
                .commit();
    }

    public long getCurrentUserId() {
        try {
            return pref.getLong(AppConst.CURRENT_USER_ID, -1L);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            try {
                return Long.parseLong(String.valueOf(pref.getInt(AppConst.CURRENT_USER_ID, -1)));
            } catch (Throwable exception) {
                Crashlytics.logException(exception);
                return -1L;
            }
        }
    }

    public boolean saveMuteDays(List<DayOfWeek> muteDays, String keyMute) {
        String jsonMuteDays = gson.toJson(muteDays);
        return pref.edit().putString(keyMute, jsonMuteDays).commit();
    }

    public List<DayOfWeek> getMuteDays(String keyMute) {
        Type type = new TypeToken<List<DayOfWeek>>() {
        }.getType();
        String jsonMuteDays = pref.getString(keyMute, null);
        if (jsonMuteDays != null) {
            return gson.fromJson(jsonMuteDays, type);
        }
        return null;
    }

    @Override
    public boolean clearData() {
        return pref
                .edit()
                .clear()
                .commit();
    }

    @Override
    public boolean savePin(String pinCode, String typePinCode) {
        return pref
                .edit().putString(typePinCode, pinCode)
                .commit();
    }

    @Override
    public String getPin(String typePinCode) {
        return pref.getString(typePinCode, null);
    }

    @Override
    public boolean saveCurrentSound(String soundUrl, String canonicalPath) {
        boolean fileNameSaveResult = pref
                .edit().putString(KEY_CURRENT_SOUND, canonicalPath)
                .commit();

        boolean soundUrlSaveResult = pref
                .edit().putString(KEY_CURRENT_SOUND_URL, soundUrl)
                .commit();

        return fileNameSaveResult && soundUrlSaveResult;
    }

    @Override
    public String getCurrentSoundFilePath() {
        return pref.getString(KEY_CURRENT_SOUND, "");
    }

    @Override
    public String getCurrentSoundUrl() {
        return pref.getString(KEY_CURRENT_SOUND_URL, "");
    }
}
