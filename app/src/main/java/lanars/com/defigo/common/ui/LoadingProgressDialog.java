package lanars.com.defigo.common.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;

import lanars.com.defigo.R;

public class LoadingProgressDialog extends Dialog {

    public LoadingProgressDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.progress_dialog_layout);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
    }
}