package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.AuthApi;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AuthService implements IAuthService {

    private AuthApi authApi;

    private NetworkClientUtil networkClientUtil;

    public AuthService(AuthApi authApi, NetworkClientUtil networkClientUtil) {
        this.authApi = authApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<SignModel> signIn(Map<String, String> params) {
        return authApi.signIn(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<SignModel> signUp(MultipartBody.Part fileImage, RequestBody userName, RequestBody password, RequestBody fName, RequestBody lName, RequestBody phone, RequestBody email) {
        return authApi.signUp(fileImage, userName, password, fName, lName, phone, email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<StatusModel> recoveryPassword(Map<String, String> params) {
        return authApi.recoveryPassword(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<StatusModel> changePassword(Map<String, String> params) {
        return authApi.changePassword(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}
