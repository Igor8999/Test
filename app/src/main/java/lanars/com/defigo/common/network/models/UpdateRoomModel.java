package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateRoomModel {

    @SerializedName("header")
    @Expose
    private String header;

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("imgUrl")

    @Expose
    private String imgUrl;

    public UpdateRoomModel(String header, String text, String imgUrl, Boolean isSingleButton) {
        this.header = header;
        this.text = text;
        this.imgUrl = imgUrl;
        this.isSingleButton = isSingleButton;
    }

    @SerializedName("isSingleButton")

    @Expose
    private Boolean isSingleButton;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Boolean getIsSingleButton() {
        return isSingleButton;
    }

    public void setIsSingleButton(Boolean isSingleButton) {
        this.isSingleButton = isSingleButton;
    }

}


