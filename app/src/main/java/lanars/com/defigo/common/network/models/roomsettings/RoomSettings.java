package lanars.com.defigo.common.network.models.roomsettings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lanars.com.defigo.common.network.models.UpdateRoomModel;

public class RoomSettings {

    @SerializedName("ownerId")
    @Expose
    private Long ownerId;
    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;
    @SerializedName("isSingleButton")
    @Expose
    private Boolean isSingleButton;

    public RoomSettings(Long ownerId, String header, String text, String imgUrl, Boolean isSingleButton) {
        this.ownerId = ownerId;
        this.header = header;
        this.text = text;
        this.imgUrl = imgUrl;
        this.isSingleButton = isSingleButton;
    }

    public RoomSettings() {

    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Boolean getIsSingleButton() {
        return isSingleButton;
    }

    public void setIsSingleButton(Boolean isSingleButton) {
        this.isSingleButton = isSingleButton;
    }

    public void setUpdateRoom(UpdateRoomModel updateRoom) {
        this.header = updateRoom.getHeader();
        this.text = updateRoom.getText();
        this.imgUrl = updateRoom.getImgUrl();
        this.isSingleButton = updateRoom.getIsSingleButton();
    }

}


