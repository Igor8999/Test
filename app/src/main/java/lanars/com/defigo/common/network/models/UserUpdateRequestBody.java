package lanars.com.defigo.common.network.models;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UserUpdateRequestBody {
    private RequestBody userName;
    private RequestBody email;
    private RequestBody lName;
    private RequestBody fName;
    private RequestBody phone;
    private RequestBody street;
    private RequestBody zip;
    private RequestBody country;
    private RequestBody city;
    private RequestBody hasVideo;
    private RequestBody currentSound;

    private MultipartBody.Part fileImage;

    //TODO Use builder pattern from start till end
    private UserUpdateRequestBody(Builder builder) {
        setUserName(builder.userName);
        setEmail(builder.email);
        setlName(builder.lName);
        setfName(builder.fName);
        setPhone(builder.phone);
        setStreet(builder.street);
        setZip(builder.zip);
        setCountry(builder.country);
        setCity(builder.city);
        setHasVideo(builder.hasVideo);
        setCurrentSound(builder.currentSound);
        setFileImage(builder.fileImage);
    }

    public MultipartBody.Part getFileImage() {
        return fileImage;
    }

    public void setFileImage(MultipartBody.Part fileImage) {
        this.fileImage = fileImage;
    }

    public RequestBody getUserName() {
        return userName;
    }

    public void setUserName(RequestBody userName) {
        this.userName = userName;
    }

    public RequestBody getEmail() {
        return email;
    }

    public void setEmail(RequestBody email) {
        this.email = email;
    }

    public RequestBody getlName() {
        return lName;
    }

    public void setlName(RequestBody lName) {
        this.lName = lName;
    }

    public RequestBody getfName() {
        return fName;
    }

    public void setfName(RequestBody fName) {
        this.fName = fName;
    }

    public RequestBody getPhone() {
        return phone;
    }

    public void setPhone(RequestBody phone) {
        this.phone = phone;
    }

    public RequestBody getStreet() {
        return street;
    }

    public void setStreet(RequestBody street) {
        this.street = street;
    }

    public RequestBody getZip() {
        return zip;
    }

    public void setZip(RequestBody zip) {
        this.zip = zip;
    }

    public RequestBody getCountry() {
        return country;
    }

    public void setCountry(RequestBody country) {
        this.country = country;
    }

    public RequestBody getCity() {
        return city;
    }

    public void setCity(RequestBody city) {
        this.city = city;
    }

    public void setHasVideo(RequestBody hasVideo) {
        this.hasVideo = hasVideo;
    }

    public void setCurrentSound(RequestBody currentSound) {
        this.currentSound = currentSound;
    }

    public RequestBody getHasVideo() {
        return hasVideo;
    }

    public RequestBody getCurrentSound() {
        return currentSound;
    }

    public static final class Builder {
        private RequestBody userName;
        private RequestBody email;
        private RequestBody lName;
        private RequestBody fName;
        private RequestBody phone;
        private RequestBody street;
        private RequestBody zip;
        private RequestBody country;
        private RequestBody city;
        private RequestBody hasVideo;
        private RequestBody currentSound;
        private MultipartBody.Part fileImage;

        public Builder() {
        }

        public Builder userName(RequestBody val) {
            userName = val;
            return this;
        }

        public Builder email(RequestBody val) {
            email = val;
            return this;
        }

        public Builder lName(RequestBody val) {
            lName = val;
            return this;
        }

        public Builder fName(RequestBody val) {
            fName = val;
            return this;
        }

        public Builder phone(RequestBody val) {
            phone = val;
            return this;
        }

        public Builder street(RequestBody val) {
            street = val;
            return this;
        }

        public Builder zip(RequestBody val) {
            zip = val;
            return this;
        }

        public Builder country(RequestBody val) {
            country = val;
            return this;
        }

        public Builder city(RequestBody val) {
            city = val;
            return this;
        }

        public Builder hasVideo(RequestBody val) {
            hasVideo = val;
            return this;
        }

        public Builder currentSound(RequestBody val) {
            currentSound = val;
            return this;
        }

        public Builder fileImage(MultipartBody.Part val) {
            fileImage = val;
            return this;
        }

        public void setHasVideo(RequestBody hasVideo) {
            this.hasVideo = hasVideo;
        }

        public UserUpdateRequestBody build() {
            return new UserUpdateRequestBody(this);
        }
    }
}
