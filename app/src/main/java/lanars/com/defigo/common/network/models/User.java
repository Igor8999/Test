package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {
    public static final int ROLE_ADMIN = 5;
    public static final int ROLE_USER = 0;

    private long id;
    private String userName;
    private String email;
    @SerializedName("lName")
    private String lastName;
    @SerializedName("fName")
    private String firstName;
    private String phone;
    private String imgUrl;
    private String street;
    private String zip;
    private String country;
    private String city;
    private boolean hasVideo;
    private String verified;
    private String createdAt;
    private String updatedAt;
    private List<UsersRoom> usersRooms = null;
    private Schedule schedule;
    private int role;
    private String currentSound;

    public User(List<UsersRoom> usersRooms) {
        this.usersRooms = usersRooms;
    }

    public User(long id, String userName, String email, String lastName, String firstName, String phone, String imgUrl, String street, String zip, String country, String city, String verified, String createdAt, String updatedAt) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phone = phone;
        this.imgUrl = imgUrl;
        this.street = street;
        this.zip = zip;
        this.country = country;
        this.city = city;
        this.verified = verified;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public User() {

    }

    @TypeAccessUser
    public int getStateProvideAccess() {
        return stateProvideAccess;
    }

    public void setStateProvideAccess(@TypeAccessUser int stateProvideAccess) {
        this.stateProvideAccess = stateProvideAccess;
    }

    private int stateProvideAccess;

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<UsersRoom> getUsersRooms() {
        return usersRooms;
    }

    public void setUsersRooms(List<UsersRoom> usersRooms) {
        this.usersRooms = usersRooms;
    }

    public boolean hasVideo() {
        return hasVideo;
    }

    public void setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getCurrentSound() {
        return currentSound;
    }

    public void setCurrentSound(String currentSound) {
        this.currentSound = currentSound;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", phone='" + phone + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", street='" + street + '\'' +
                ", zip='" + zip + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", hasVideo=" + hasVideo +
                ", verified='" + verified + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", usersRooms=" + usersRooms +
                ", schedule=" + schedule +
                ", role=" + role +
                ", currentSound='" + currentSound + '\'' +
                ", stateProvideAccess=" + stateProvideAccess +
                '}';
    }
}