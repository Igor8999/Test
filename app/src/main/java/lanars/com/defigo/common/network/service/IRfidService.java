package lanars.com.defigo.common.network.service;

import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.RfidModel;

public interface IRfidService {
    Observable<List<RfidModel>> getRfidList();

    Observable<RfidModel> deleteRfidPin(int rfidId);

    Observable<RfidModel> setupRfidPin(int rfidId, String pin);

    Observable<RfidModel> changeRfidStatus(int rfidId, boolean isDisabled);

    Observable<RfidModel> updateRfid(int rfidId, String name, String pin, boolean isDisabled);
}
