package lanars.com.defigo.common.api;

import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.StatusModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AuthApi {

    @POST("/signin")
    Observable<Response<SignModel>> signIn(@Body Map<String, String> params);

    @PATCH("/password")
    Observable<Response<StatusModel>> recoveryPassword(@Body Map<String, String> params);

    @Multipart
    @POST("/signup")
    Observable<Response<SignModel>> signUp(@Part MultipartBody.Part fileImage,
                                           @Part("userName") RequestBody userName,
                                           @Part("password") RequestBody password,
                                           @Part("fName") RequestBody fName,
                                           @Part("lName") RequestBody lName,
                                           @Part("phone") RequestBody phone,
                                           @Part("email") RequestBody email);

    @PATCH("/api/users/password")
    Observable<Response<StatusModel>> changePassword(@Body Map<String, String> params);
}
