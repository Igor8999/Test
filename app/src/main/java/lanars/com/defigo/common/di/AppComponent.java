package lanars.com.defigo.common.di;

import javax.inject.Singleton;

import dagger.Component;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.admin.base.AdminBaseActivity;
import lanars.com.defigo.admin.base.AdminBasePresenter;
import lanars.com.defigo.admin.settings.building.AdminPanelPresenter;
import lanars.com.defigo.admin.settings.dweller.DwellerSettingsPresenter;
import lanars.com.defigo.admin.settings.dweller.detail.DwellerCardsPresenter;
import lanars.com.defigo.admin.settings.edit.EditCardFragmentPresenter;
import lanars.com.defigo.admin.settings.reader.ReadCardPresenter;
import lanars.com.defigo.call.CallActivityPresenter;
import lanars.com.defigo.call.CallPreviewActivityPresenter;
import lanars.com.defigo.call.SocketService;
import lanars.com.defigo.common.base.BaseActivity;
import lanars.com.defigo.common.di.modules.DataModule;
import lanars.com.defigo.common.di.modules.LocalNavigationModule;
import lanars.com.defigo.common.di.modules.NavigationModule;
import lanars.com.defigo.common.di.modules.database.DatabaseModule;
import lanars.com.defigo.common.di.modules.network.DefigoApiModule;
import lanars.com.defigo.cropimage.CropPresenter;
import lanars.com.defigo.home.HomeFragmentPresenter;
import lanars.com.defigo.login.LoginActivity;
import lanars.com.defigo.login.LoginActivityPresenter;
import lanars.com.defigo.login.LoginFragmentPresenter;
import lanars.com.defigo.login.recovery.RecoveryPasswordPresenter;
import lanars.com.defigo.main.MainActivity;
import lanars.com.defigo.main.MainPresenter;
import lanars.com.defigo.push.TrackerIdService;
import lanars.com.defigo.push.TrackerMessagingService;
import lanars.com.defigo.register.RegistrationFragment;
import lanars.com.defigo.register.RegistrationPresenter;
import lanars.com.defigo.settings.SettingsContainerFragment;
import lanars.com.defigo.settings.SettingsContainerPresenter;
import lanars.com.defigo.settings.SettingsMenuFragment;
import lanars.com.defigo.settings.access.ProvideAccessListPresenter;
import lanars.com.defigo.settings.access.ProvideAccessSchedulePresenter;
import lanars.com.defigo.settings.actions.ActionsSettingsPresenter;
import lanars.com.defigo.settings.actions.opendoors.OpenDoorsPresenter;
import lanars.com.defigo.settings.address.AddressPresenter;
import lanars.com.defigo.settings.call.CallSettingsPresenter;
import lanars.com.defigo.settings.callingsequence.CallingSequenceMenuPresenter;
import lanars.com.defigo.settings.callingsequence.membersettings.CallingSequenceSettingsPresenter;
import lanars.com.defigo.settings.doorbell.DoorbellSettingsPresenter;
import lanars.com.defigo.settings.email.EmailSettingsPresenter;
import lanars.com.defigo.settings.language.LanguageSettingsPresenter;
import lanars.com.defigo.settings.layout.LayoutSettingsPresenter;
import lanars.com.defigo.settings.mutemode.MuteModePresenter;
import lanars.com.defigo.settings.notifications.NotificationsSettingsPresenter;
import lanars.com.defigo.settings.password.ChangePasswordPresenter;
import lanars.com.defigo.settings.phonenumber.PhoneNumberPresenter;
import lanars.com.defigo.settings.pincode.ChangePinCodeFragment;
import lanars.com.defigo.settings.pincode.ChangePinPresenter;
import lanars.com.defigo.settings.profile.ProfileFragmentPresenter;
import lanars.com.defigo.settings.profile.personal.PersonalInfoPresenter;
import lanars.com.defigo.settings.rfid.RfidCardSettingsPresenter;
import lanars.com.defigo.settings.rfid.edit.EditUserRfidCardFragmentPresenter;
import lanars.com.defigo.settings.sound.SoundSettingsPresenter;
import lanars.com.defigo.settings.visibility.VisibilityOptionsPresenter;
import lanars.com.defigo.settings.visibility.scheduling.SchedulingVisiblePresenter;

@Component(modules = {DataModule.class,
        NavigationModule.class,
        LocalNavigationModule.class,
        DefigoApiModule.class,
        DatabaseModule.class}
)
@Singleton
public interface AppComponent {

    void inject(MainActivity activity);

    void inject(MainPresenter activity);

    void inject(SettingsContainerFragment settingsContainerFragment);

    void inject(SettingsContainerPresenter settingsContainerPresenter);

    void inject(LoginActivity loginActivity);

    void inject(LoginActivityPresenter loginActivityPresenter);

    void inject(LoginFragmentPresenter loginFragmentPresenter);

    void inject(RegistrationPresenter registrationPresenter);

    void inject(CropPresenter cropPresenter);

    void inject(RegistrationFragment registrationFragment);

    void inject(SettingsMenuFragment settingsMenuFragment);

    void inject(DoorbellSettingsPresenter doorbellSettingsPresenter);

    void inject(ActionsSettingsPresenter actionsSettingsPresenter);

    void inject(RfidCardSettingsPresenter rfidCardSettingsPresenter);

    void inject(ChangePinPresenter changePinPresenter);

    void inject(LayoutSettingsPresenter layoutSettingsPresenter);

    void inject(HomeFragmentPresenter homeFragmentPresenter);

    void inject(ProfileFragmentPresenter profileFragmentPresenter);

    void inject(PersonalInfoPresenter personalInfoPresenter);

    void inject(ProvideAccessListPresenter provideAccsessPresenter);

    void inject(AddressPresenter addressPresenter);

    void inject(ChangePasswordPresenter changePasswordPresenter);

    void inject(EmailSettingsPresenter emailSettingsPresenter);

    void inject(PhoneNumberPresenter phoneNumberPresenter);

    void inject(VisibilityOptionsPresenter visibilityOptionsPresenter);

    void inject(SchedulingVisiblePresenter schedulingVisiblePresenter);

    void inject(MuteModePresenter muteModePresenter);

    void inject(OpenDoorsPresenter openDoorsPresenter);

    void inject(NotificationsSettingsPresenter notificationsSettingsPresenter);

    void inject(SoundSettingsPresenter soundSettingsPresenter);

    void inject(CallingSequenceMenuPresenter callingSequenceMenuPresenter);

    void inject(CallingSequenceSettingsPresenter callingSequenceSettingsPresenter);

    void inject(ProvideAccessSchedulePresenter provideAccessSchedulePresenter);

    void inject(LanguageSettingsPresenter languageSettingsPresenter);

    void inject(ChangePinCodeFragment changePinCodeFragment);

    void inject(BaseActivity baseActivity);

    void inject(AdminBasePresenter adminBasePresenter);

    void inject(AdminBaseActivity adminBaseActivity);

    void inject(AdminPanelPresenter adminPanelPresenter);

    void inject(DwellerSettingsPresenter dwellerSettingsPresenter);

    void inject(DwellerCardsPresenter dwellerCardsPresenter);

    void inject(ReadCardPresenter readCardPresenter);

    void inject(EditCardFragmentPresenter editCardFragmentPresenter);

    void inject(SocketService socketService);

    void inject(CallActivityPresenter presenter);

    void inject(TrackerIdService service);

    void inject(CallPreviewActivityPresenter presenter);

    void inject(RecoveryPasswordPresenter recoveryPasswordPresenter);

    void inject(CallSettingsPresenter callSettingsPresenter);

    void inject(TrackerMessagingService service);

    void inject(EditUserRfidCardFragmentPresenter presenter);

    void inject(DefigoApplication application);
}
