package lanars.com.defigo.common.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.utils.Utils;

public abstract class BaseSettingsFragment extends BaseFragment {

    protected FrameLayout frameContainer;

    @BindView(R.id.tvTitleSettings)
    protected TextView tvTitleSettings;

    @BindView(R.id.settings_toolbar)
    protected View settings_toolbar;

    @BindView(R.id.imgClose)
    protected ImageView imgClose;

    @BindView(R.id.imgBack)
    protected ImageView imgBack;

    @BindView(R.id.btnLogout)
    protected ImageView btnLogout;

    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_settings_fragment, container, false);
        frameContainer = view.findViewById(R.id.frCont);
        View childView = createView(inflater, container);
        addViewToContainer(childView);
        ButterKnife.bind(this, view);
        if (getTitle() != null) {
            tvTitleSettings.setText(getTitle());
        }
        initViews(view);
        return view;
    }

    @OnClick(R.id.imgBack)
    public void onBackClick(View view) {
        handleBackClick();
    }

    @Override
    public void onStop() {
        super.onStop();
        Utils.hideKeyboard(getActivity());
    }

    protected abstract String getTitle();

    private void addViewToContainer(View childView) {
        frameContainer.addView(childView);
    }

    public abstract View createView(LayoutInflater inflater, ViewGroup container);

    public abstract void initViews(View view);

    public abstract void handleBackClick();
}
