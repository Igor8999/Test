package lanars.com.defigo.common.network.models.roomsettings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lanars.com.defigo.common.network.models.UpdateRoomModel;

public class IndividualSettings {

    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;

    public IndividualSettings(String header, String text, String imgUrl) {
        this.header = header;
        this.text = text;
        this.imgUrl = imgUrl;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setUpdateRoom(UpdateRoomModel updateRoom) {
        this.header = updateRoom.getHeader();
        this.text = updateRoom.getText();
        this.imgUrl = updateRoom.getImgUrl();
    }

}