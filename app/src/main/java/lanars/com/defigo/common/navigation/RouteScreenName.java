package lanars.com.defigo.common.navigation;

public class RouteScreenName {

    public static final String HOME_SCREEN = "Home_screen";
    public static final String SETTINGS_SCREEN = "Settings_screen";

    public static final String LOGIN = "login";
    public static final String REGISTRATION = "registration";

    public static final String CROP_FRAGMENT = "crop_fragment";

    public static final String MAIN = "main_screen";
    public static final String ADMIN = "admin_screen";
    public static final String TUTORIAL_ACTIVITY = "tutorial_activity";
    public static final String SETTINGS_MENU = "settings_menu";
    public static final String DOORBEL_SETTINGS = "doorbell_settings";
    public static final String ACTIONS_SETTINGS = "action_settings";
    public static final String RFID_SETTINGS = "rfid_settings";
    public static final String CHANGE_PIN = "change_pin";
    public static final String PROFILE_SETTINGS = "profile_settings";
    public static final String PERSONAL_INFO = "personal_info";
    public static final String LAYOUT_SETTINGS = "layout_settings";
    public static final String CALLING_SEQUENCE_MENU = "calling_sequence_menu";
    public static final String PROVIDE_ACCESS = "provide_access";
    public static final String ADDRESS = "address";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String EMAIL = "email_settings";
    public static final String VISIBILITY_OPTIONS = "visibility_options";
    public static final String CHANGE_PHONE_NUMBER = "change_phone_number";
    public static final String SHEDULING_VISIBILITY = "scheduling_visibility";
    public static final String MUTE_MODE = "mute_mode";
    public static final String OPEN_DOORS_ACTION = "open_doors_action";
    public static final String NOTIFICATIONS_SETTINGS = "notifications_settings";
    public static final String SCHEDULE_PROVIDE_ACCESS = "shedule_provide_access";
    public static final String CALLING_SEQUENCE_SETTINGS = "calling_sequence_settings";
    public static final String SOUND_SETTINGS = "sound_settings";
    public static final String LANGUAGE = "language";

    //admin
    public static final String ADMIN_PANEL = "admin_panel";
    public static final String DWELLERS = "dwellers";
    public static final String DWELLER_CARDS = "dweller_cards";
    public static final String READ_CARDS = "read_cards";
    public static final String EDIT_CARDS = "edit_cards";
    public static final String RECOVERY_PASSWORD = "recovery_password";
    public static final String CALL_SETTINGS = "call_settings";
}
