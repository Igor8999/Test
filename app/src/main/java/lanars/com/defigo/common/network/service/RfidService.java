package lanars.com.defigo.common.network.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.RfidApi;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;

public class RfidService implements IRfidService {

    private RfidApi rfidApi;
    private NetworkClientUtil networkClientUtil;

    public RfidService(RfidApi rfidApi, NetworkClientUtil networkClientUtil) {
        this.rfidApi = rfidApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<List<RfidModel>> getRfidList() {
        return rfidApi.getRfidList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<RfidModel> deleteRfidPin(int rfidId) {
        return setupRfidPin(rfidId, "");
    }

    @Override
    public Observable<RfidModel> setupRfidPin(int rfidId, String pin) {
        Map<String, Object> params = new HashMap<>();
        params.put("pin", pin);

        return getRfidModelObservable(rfidId, params);
    }

    @Override
    public Observable<RfidModel> changeRfidStatus(int rfidId, boolean isDisabled) {
        Map<String, Object> params = new HashMap<>();
        params.put("disabled", !isDisabled);

        return getRfidModelObservable(rfidId, params);
    }

    @Override
    public Observable<RfidModel> updateRfid(int rfidId, String name, String pin, boolean isDisabled) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("pin", pin);
        params.put("disabled", !isDisabled);

        return getRfidModelObservable(rfidId, params);
    }

    private Observable<RfidModel> getRfidModelObservable(int rfidId, Map<String, Object> params) {
        return rfidApi.updateRfid(rfidId, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}
