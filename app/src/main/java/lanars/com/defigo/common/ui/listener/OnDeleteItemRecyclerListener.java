package lanars.com.defigo.common.ui.listener;

public interface OnDeleteItemRecyclerListener {

    void onDeleteItem(int position);
}
