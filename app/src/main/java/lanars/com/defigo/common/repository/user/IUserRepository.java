package lanars.com.defigo.common.repository.user;

import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.SequenceUser;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.database.model.DBUser;

public interface IUserRepository {

    Observable<List<User>> getAllUsers();

    Observable<List<User>> users(String search, int offset, int limit);

    Observable<ActionsOptionsModel> getActionsOptions(long roomId, String typeActions);

    Observable<StatusModel> updateRulesActions(long roomId, Map<String, Object> params);

    Observable<User> usersMy();

    Observable<StatusModel> shareAccessToUser(Map<String, Object> params);

    Observable<StatusModel> removeSharedAccess(long guestId);

    Observable<List<VisibilityDoorModel>> getUsersDoorbellsVisibility();

    Observable<List<SequenceUser>> getSequenceUser(int typeCallingInstance);

    Observable<List<User>> getSharedAccessByUser();

    Observable<RoomLayoutModel> getRoomLayoutSettingsUser(long roomId);

    //from db
    Flowable<DBUser> getUserById(long id);

    void insertUsers(DBUser... users);

    void updateUsers(DBUser... users);

    Observable<Integer> clearUsers();

    void insertRooms(DBRoom... rooms);

    Observable<Integer> clearRooms();

    Flowable<List<DBRoom>> getAllRooms();

}
