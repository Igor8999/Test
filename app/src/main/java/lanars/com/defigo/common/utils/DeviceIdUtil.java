package lanars.com.defigo.common.utils;

import android.os.Build;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import lanars.com.defigo.common.network.service.IDeviceService;

public final class DeviceIdUtil {

    public static void updateDeviceId(IDeviceService deviceService) {
        String token = FirebaseInstanceId.getInstance().getToken();
        String deviceId = FirebaseInstanceId.getInstance().getId();

        updateDeviceId(deviceService, token, deviceId);
    }

    public static void updateDeviceId(IDeviceService deviceService, String token, String deviceId) {
        Map<String, Object> params = new HashMap<>();

        params.put("deviceId", deviceId);
        params.put("token", token);
        params.put("platform", "Android");
        params.put("type", "phone");
        params.put("model", Build.MODEL);

        deviceService
                .addNewDevice(params)
                .subscribe(responseBodyResponse -> {

                }, Crashlytics::logException);
    }

    public static void deleteDeviceId(IDeviceService deviceService, String deviceId) {
        deviceService
                .deleteDevice(deviceId)
                .subscribe(responseBodyResponse -> {

                }, Crashlytics::logException);
    }
}
