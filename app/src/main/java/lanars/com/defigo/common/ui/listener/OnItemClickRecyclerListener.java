package lanars.com.defigo.common.ui.listener;

public interface OnItemClickRecyclerListener {

    void onItemClick(int position);
}
