package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VisibilityDoorModel implements Serializable {

    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("isSingleButton")
    @Expose
    private Boolean isSingleButton;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("publicName")
    @Expose
    private String publicName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("isOwnerCurrentUser")
    @Expose
    private Boolean isOwner;
    @SerializedName("visibilityOptions")
    @Expose
    private VisibilityOptions visibilityOptions;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Boolean getIsSingleButton() {
        return isSingleButton;
    }

    public void setIsSingleButton(Boolean isSingleButton) {
        this.isSingleButton = isSingleButton;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublicName() {
        return publicName;
    }

    public void setPublicName(String publicName) {
        this.publicName = publicName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(Boolean isOwner) {
        this.isOwner = isOwner;
    }

    public Boolean getSingleButton() {
        return isSingleButton;
    }

    public void setSingleButton(Boolean singleButton) {
        isSingleButton = singleButton;
    }

    public Boolean getOwner() {
        return isOwner;
    }

    public void setOwner(Boolean owner) {
        isOwner = owner;
    }

    public VisibilityOptions getVisibilityOptions() {
        return visibilityOptions;
    }

    public void setVisibilityOptions(VisibilityOptions visibilityOptions) {
        this.visibilityOptions = visibilityOptions;
    }

}