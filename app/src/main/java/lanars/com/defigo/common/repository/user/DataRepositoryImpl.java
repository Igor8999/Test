package lanars.com.defigo.common.repository.user;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.models.CardMock;
import lanars.com.defigo.admin.models.EntranceModel;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.network.models.LanguageModel;
import lanars.com.defigo.common.network.models.NotificationModel;
import lanars.com.defigo.common.network.models.SoundModel;
import lanars.com.defigo.common.network.service.ISoundService;
import lanars.com.defigo.common.utils.IAppPreferences;

public class DataRepositoryImpl implements IDataRepository {

    private IAppPreferences appPreferences;
    private ISoundService soundService;

    private Context appContext;

    public DataRepositoryImpl(IAppPreferences appPreferences, ISoundService soundService) {
        this.appPreferences = appPreferences;
        this.appContext = DefigoApplication.getInstance();
        this.soundService = soundService;
    }

    @Override
    public Observable<List<DayOfWeek>> getAllDaysOfWeek(String keyMute) {
        if (appPreferences.getMuteDays(keyMute) != null) {
            return Observable.fromArray(appPreferences.getMuteDays(keyMute));
        }
        return Observable.fromArray(getListDaysOfWeek());
    }

    @Override
    public void saveDayOfWeeks(List<DayOfWeek> muteDays, String keyMute) {
        appPreferences.saveMuteDays(muteDays, keyMute);
    }

    @Override
    public Observable<List<NotificationModel>> getNotificationsSettings() {
        return Observable.fromArray(getNotificationsModels());
    }

    @Override
    public Observable<List<SoundModel>> getSounds() {
        return soundService.getSoundsList();
    }

    @Override
    public Observable<List<LanguageModel>> getLanguages() {
        return Observable.fromArray(getAllLanguages());
    }

    //todo this is stub!
    @Override
    public Observable<List<BuildingModel>> getBuildings() {
        return Observable.fromArray(getAllBuildings());
    }

    //todo this is stub!
    @Override
    public Observable<List<EntranceModel>> getEntrances() {
        return Observable.fromArray(getAllEntrances());
    }

    //todo this is stub!
    @Override
    public Observable<List<ApartmentModel>> getApartments() {
        return Observable.fromArray(getAllApartments());
    }

    //todo this is stub!
    @Override
    public Observable<List<CardMock>> getCards() {
        return Observable.fromArray(getAllCards());
    }

    private List<CardMock> getAllCards() {
        List<CardMock> cards = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            cards.add(new CardMock("Super card", "Rfid card " + (i + 1), i));
        }
        return cards;
    }

    //todo this is stub!
    private List<ApartmentModel> getAllApartments() {
        List<ApartmentModel> apartmentModels = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
//            apartmentModels.add(new ApartmentModel("Apartment " + (i + 1)));
        }
        return apartmentModels;
    }

    //todo this is stub!
    private List<EntranceModel> getAllEntrances() {
        List<EntranceModel> entranceModels = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entranceModels.add(null);
        }
        return entranceModels;
    }

    //todo this is stub!
    private List<BuildingModel> getAllBuildings() {
        List<BuildingModel> buildingModels = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
//            buildingModels.add(new BuildingModel("Building " + (i + 1)));
        }
        return buildingModels;
    }

    private List<LanguageModel> getAllLanguages() {
        List<LanguageModel> languages = new ArrayList<>();
        languages.add(new LanguageModel("English", "en", false));
        languages.add(new LanguageModel("Norsk", "no", false));
        languages.add(new LanguageModel("Svensk", "sv", false));
        return languages;
    }

    private List<DayOfWeek> getListDaysOfWeek() {
        List<DayOfWeek> dayOfWeeks = new ArrayList<>();
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.sunday), 0));
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.monday), 1));
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.tuesday), 2));
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.wednesday), 3));
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.thursday), 4));
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.friday), 5));
        dayOfWeeks.add(new DayOfWeek(appContext.getString(R.string.saturday), 6));
        return dayOfWeeks;
    }

    private List<NotificationModel> getNotificationsModels() {
        List<NotificationModel> notificationModels = new ArrayList<>();
        notificationModels.add(new NotificationModel("Email", false));
        notificationModels.add(new NotificationModel("SMS", false));
        notificationModels.add(new NotificationModel("Push", false));
        return notificationModels;
    }
}
