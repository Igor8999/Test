package lanars.com.defigo.common.di.modules.network;

import java.io.IOException;

import lanars.com.defigo.common.utils.IAppPreferences;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthTokenInterceptor implements Interceptor {

    private IAppPreferences appPreferences;

    public AuthTokenInterceptor(IAppPreferences appPreferences) {
        this.appPreferences = appPreferences;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        if (appPreferences.getAccessToken() != null) {
            Request request = original.newBuilder()
                    .header("Authorization", "Bearer " + appPreferences.getAccessToken())
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        }
        return chain.proceed(original);
    }
}