package lanars.com.defigo.common.base;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface BaseMvpView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void onShowError(String errorMessage);

    void onShowProgress();

    void onHideProgress();

}
