package lanars.com.defigo.common.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.os.Environment.isExternalStorageRemovable;

public class FileUtils implements IFileUtils {

    public static final String IMAGE_PROFILE = "image_profile.png";
    public static final String IMAGE_LAYOUT = "image_layout.png";

    // TODO Brake SRP
    public File getDiskCacheDir(Context context, String uniqueName) {
        if (TextUtils.isEmpty(uniqueName)) {
            return null;
        }

        final String cachePath = isExternalStorageAvailable()
                ? context.getExternalCacheDir().getPath()
                : context.getCacheDir().getPath();

        return new File(cachePath + File.separator + uniqueName);
    }

    private static boolean isExternalStorageAvailable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !isExternalStorageRemovable();
    }

    public void saveBitmapIntoFile(File filename, Bitmap bmp) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            AppConst.Logger("exception");
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean checkExistFile(Context context, String nameFile) {
        File directory = getDiskCacheDir(context, nameFile);
        return directory != null && directory.exists();
    }

    public Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public String getUniqueFileName(String fileName) {
        return String.format("%s%s", fileName, System.currentTimeMillis());
    }

    @Override
    public void deleteFile(File file) {
        file.delete();
    }
}
