package lanars.com.defigo.common.repository.user;

import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.models.CardMock;
import lanars.com.defigo.admin.models.EntranceModel;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.network.models.LanguageModel;
import lanars.com.defigo.common.network.models.NotificationModel;
import lanars.com.defigo.common.network.models.SoundModel;

public interface IDataRepository {

    Observable<List<DayOfWeek>> getAllDaysOfWeek(String keyMute);

    void saveDayOfWeeks(List<DayOfWeek> dayOfWeeks, String keyMute);

    Observable<List<NotificationModel>> getNotificationsSettings();

    Observable<List<SoundModel>> getSounds();

    Observable<List<LanguageModel>> getLanguages();

    Observable<List<BuildingModel>> getBuildings();

    Observable<List<EntranceModel>> getEntrances();

    Observable<List<ApartmentModel>> getApartments();

    Observable<List<CardMock>> getCards();
}
