package lanars.com.defigo.common.ui.listener;

public interface OnItemClickCallingSequenceListener {

    void onItemClickSequence(int position);
}
