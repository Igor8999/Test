package lanars.com.defigo.common.network.service;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public interface IUserService {

    Observable<Response<List<User>>> users();

    Observable<Response<List<User>>> users(String search, int offset, int limit);

    Observable<Response<User>> usersMy();

    Observable<User> usersMyUpdate(UserUpdateRequestBody userUpdateRequestBody);

    Observable<Response<ResponseBody>> getAllUsersDoorbells();

    Observable<Response<List<VisibilityDoorModel>>> getUsersDoorbellsVisibility();

    Observable<ResponseBody> changeVisibilityOptionsIndividual(Map<String, Object> params, long doorbellId);

    Observable<Response<RoomLayoutModel>> getRoomLayoutSettingsUser(long roomId);

    Observable<UpdateRoomModel> updateInfoRoom(long roomId,
                                               MultipartBody.Part file,
                                               RequestBody header,
                                               RequestBody text);

    Observable<Response<ActionsOptionsModel>> getActionOptions(long roomId, String type);

    Observable<Response<StatusModel>> updateRulesActions(long roomId, Map<String, Object> params);

    //provide access
    Observable<Response<List<User>>> getSharedAccessByUser();

    Observable<Response<StatusModel>> shareAccessToUser(Map<String, Object> params);

    Observable<Response<StatusModel>> removeSharedAccess(long guestId);

}
