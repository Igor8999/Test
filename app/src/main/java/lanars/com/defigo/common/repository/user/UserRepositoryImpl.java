package lanars.com.defigo.common.repository.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.SequenceUser;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import lanars.com.defigo.database.AppDatabase;
import lanars.com.defigo.database.dao.RoomsDao;
import lanars.com.defigo.database.dao.UsersDao;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.database.model.DBUser;
import lanars.com.defigo.settings.callingsequence.membersettings.CallingSequenceSettingsFragment;

public class UserRepositoryImpl implements IUserRepository {

    private NetworkClientUtil networkClientUtil;
    private IAuthService authService;
    private IUserService userService;
    private UsersDao usersDao;
    private RoomsDao roomsDao;

    public UserRepositoryImpl(AppDatabase appDatabase, IAuthService authService, IUserService userService, NetworkClientUtil networkClientUtil) {
        this.usersDao = appDatabase.usersDao();
        this.roomsDao = appDatabase.roomsDao();
        this.authService = authService;
        this.userService = userService;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return userService.users()
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<List<User>> users(String search, int offset, int limit) {
        return userService.users(search, offset, limit)
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Flowable<DBUser> getUserById(long id) {
        return this.usersDao.getUserById(id)
                .map(dbUsers -> {
                    if (dbUsers.size() > 0) {
                        return dbUsers.get(0);
                    }

                    return DBUser.provideStubUser();
                });
    }

    @Override
    public void insertUsers(DBUser... users) {
        Completable
                .fromAction(() -> usersDao.insertUsers(users))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void updateUsers(DBUser... users) {
        Completable
                .fromAction(() -> usersDao.updateUsers(users))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public void insertRooms(DBRoom... rooms) {
        Completable
                .fromAction(() -> roomsDao.insertRooms(rooms))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

    @Override
    public Observable<Integer> clearUsers() {
        return Observable.just(1)
                .doOnNext(integer -> usersDao.clearUserDb())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Integer> clearRooms() {
        return Observable.just(1)
                .doOnNext(integer -> roomsDao.clearRoomsDb())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Flowable<List<DBRoom>> getAllRooms() {
        return roomsDao.getAllRooms();
    }

    //todo stub!
    @Override
    public Observable<List<SequenceUser>> getSequenceUser(int typeCallingInstance) {
        if (typeCallingInstance == CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SIMULTANEOUS) {
            return Observable.just(getSimultaniusUsers());
        }
        return Observable.just(getSequencesUsers());
    }

    //todo stub!
    public List<SequenceUser> getSequencesUsers() {
        List<SequenceUser> users = new ArrayList<>();
        int i = 0;
        while (i < 10) {
            SequenceUser user = new SequenceUser("Monika Belucci", 0, false, true);
            users.add(user);
            i++;
        }
        return users;
    }

    //todo stub!
    public List<SequenceUser> getSimultaniusUsers() {
        List<SequenceUser> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SequenceUser user = new SequenceUser("Monika Belucci", 0, true, false);
            users.add(user);
        }
        return users;
    }

    @Override
    public Observable<List<VisibilityDoorModel>> getUsersDoorbellsVisibility() {
        return userService.getUsersDoorbellsVisibility()
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<ActionsOptionsModel> getActionsOptions(long roomId, String typeActions) {
        return userService.getActionOptions(roomId, typeActions)
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<List<User>> getSharedAccessByUser() {
        return userService.getSharedAccessByUser()
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<RoomLayoutModel> getRoomLayoutSettingsUser(long roomId) {
        return userService.getRoomLayoutSettingsUser(roomId)
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<StatusModel> updateRulesActions(long roomId, Map<String, Object> params) {
        return userService.updateRulesActions(roomId, params)
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<User> usersMy() {
        return userService.usersMy()
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<StatusModel> shareAccessToUser(Map<String, Object> params) {
        return userService.shareAccessToUser(params)
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<StatusModel> removeSharedAccess(long guestId) {
        return userService.removeSharedAccess(guestId)
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

}
