package lanars.com.defigo.common.network.service;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.UsersApi;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class UserService implements IUserService {

    private UsersApi usersApi;

    private NetworkClientUtil networkClientUtil;

    public UserService(UsersApi usersApi, NetworkClientUtil networkClientUtil) {
        this.usersApi = usersApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<Response<List<User>>> users() {
        return usersApi.users()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<List<User>>> users(String search, int offset, int limit) {
        return usersApi.users(search, offset, limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<User>> usersMy() {
        return usersApi.usersMy()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<User> usersMyUpdate(UserUpdateRequestBody userUpdateRequestBody) {
        return usersApi.usersMyUpdate(
                userUpdateRequestBody.getFileImage(),
                userUpdateRequestBody.getUserName(),
                userUpdateRequestBody.getEmail(),
                userUpdateRequestBody.getlName(),
                userUpdateRequestBody.getfName(),
                userUpdateRequestBody.getPhone(),
                userUpdateRequestBody.getStreet(),
                userUpdateRequestBody.getZip(),
                userUpdateRequestBody.getCountry(),
                userUpdateRequestBody.getCity(),
                userUpdateRequestBody.getHasVideo(),
                userUpdateRequestBody.getCurrentSound())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<Response<ResponseBody>> getAllUsersDoorbells() {
        return usersApi.getAllUsersDoorbels()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<List<VisibilityDoorModel>>> getUsersDoorbellsVisibility() {
        return usersApi.getUsersDoorbelsVisibility()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> changeVisibilityOptionsIndividual(Map<String, Object> params, long doorbellId) {
        return usersApi.changeVisibilityOptionsIndividual(params, doorbellId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<Response<RoomLayoutModel>> getRoomLayoutSettingsUser(long roomId) {
        return usersApi.getRoomLayoutSettingsUser(roomId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UpdateRoomModel> updateInfoRoom(long roomId, MultipartBody.Part file, RequestBody header, RequestBody text) {
        return usersApi.updateInfoRoom(roomId, file, header, text)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<Response<ActionsOptionsModel>> getActionOptions(long roomId, String type) {
        return usersApi.getActionOptions(roomId, type)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<StatusModel>> updateRulesActions(long roomId, Map<String, Object> params) {
        return usersApi.updateRulesActions(roomId, params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<List<User>>> getSharedAccessByUser() {
        return usersApi.getSharedAccessByUser()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<StatusModel>> shareAccessToUser(Map<String, Object> params) {
        return usersApi.shareAccessToUser(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<StatusModel>> removeSharedAccess(long guestId) {
        return usersApi.removeSharedAccess(guestId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
