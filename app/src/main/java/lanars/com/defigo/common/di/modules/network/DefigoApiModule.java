package lanars.com.defigo.common.di.modules.network;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lanars.com.defigo.common.api.AdminApi;
import lanars.com.defigo.common.api.AuthApi;
import lanars.com.defigo.common.api.BuildingsApi;
import lanars.com.defigo.common.api.DevicesApi;
import lanars.com.defigo.common.api.DoorbellsApi;
import lanars.com.defigo.common.api.RfidApi;
import lanars.com.defigo.common.api.RoomsApi;
import lanars.com.defigo.common.api.SoundApi;
import lanars.com.defigo.common.api.UsersApi;
import lanars.com.defigo.common.network.service.AdminService;
import lanars.com.defigo.common.network.service.AuthService;
import lanars.com.defigo.common.network.service.BuildingService;
import lanars.com.defigo.common.network.service.DeviceService;
import lanars.com.defigo.common.network.service.DoorbellService;
import lanars.com.defigo.common.network.service.IAdminService;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.network.service.IBuldingService;
import lanars.com.defigo.common.network.service.IDeviceService;
import lanars.com.defigo.common.network.service.IDoorbellService;
import lanars.com.defigo.common.network.service.IRfidService;
import lanars.com.defigo.common.network.service.IRoomService;
import lanars.com.defigo.common.network.service.ISoundService;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.network.service.RfidService;
import lanars.com.defigo.common.network.service.RoomService;
import lanars.com.defigo.common.network.service.SoundService;
import lanars.com.defigo.common.network.service.UserService;
import lanars.com.defigo.common.utils.NetworkClientUtil;

@Module(includes = {ApiModule.class})
public class DefigoApiModule {

    @Provides
    @Singleton
    public IUserService provideUserService(UsersApi usersApi, NetworkClientUtil networkClientUtil) {
        return new UserService(usersApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public IBuldingService provideBuildingsApi(BuildingsApi buildingsApi, NetworkClientUtil networkClientUtil) {
        return new BuildingService(buildingsApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public IDoorbellService provideDoorbellsApi(DoorbellsApi doorbellsApi, NetworkClientUtil networkClientUtil) {
        return new DoorbellService(doorbellsApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public IDeviceService provideDeviceService(DevicesApi devicesApi) {
        return new DeviceService(devicesApi);
    }

    @Provides
    @Singleton
    public IRoomService provideRoomsApi(RoomsApi roomsApi, NetworkClientUtil networkClientUtil) {
        return new RoomService(roomsApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public IAuthService provideAuthService(AuthApi authApi, NetworkClientUtil networkClientUtil) {
        return new AuthService(authApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public IAdminService provideAdminService(AdminApi adminApi, NetworkClientUtil networkClientUtil) {
        return new AdminService(adminApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public ISoundService provideSoundService(SoundApi soundApi, NetworkClientUtil networkClientUtil) {
        return new SoundService(soundApi, networkClientUtil);
    }

    @Provides
    @Singleton
    public IRfidService provideRfidService(RfidApi rfidApi, NetworkClientUtil networkClientUtil) {
        return new RfidService(rfidApi, networkClientUtil);
    }
}