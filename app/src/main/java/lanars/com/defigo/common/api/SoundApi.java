package lanars.com.defigo.common.api;

import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.SoundModel;
import retrofit2.Response;
import retrofit2.http.GET;

public interface SoundApi {

    @GET("/api/music")
    Observable<Response<List<SoundModel>>> getSoundsList();
}
