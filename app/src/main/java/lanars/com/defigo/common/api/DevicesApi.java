package lanars.com.defigo.common.api;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DevicesApi {

    @POST("/api/devices")
    Observable<Response<ResponseBody>> addNewDevice(@Body Map<String, Object> params);

    @DELETE("/api/devices/{deviceId}")
    Observable<Response<ResponseBody>> deleteDevice(@Path("deviceId") String deviceId);

    @PATCH("/api/devices/{deviceId}")
    Observable<Response<ResponseBody>> updateDeviceToken(@Body Map<String, Object> params, @Path("deviceId") long deviceId);
}
