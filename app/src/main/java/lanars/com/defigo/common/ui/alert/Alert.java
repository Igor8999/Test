package lanars.com.defigo.common.ui.alert;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

public final class Alert {

    private static WeakReference<Activity> activityWeakReference;

    private AlertLayout alertLayout;

    /**
     * Constructor
     */

    private Alert() {
        //Utility classes should not be instantiated
    }

    /**
     * Creates the Alert, and maintains a reference to the calling Activity
     *
     * @param activity The calling Activity
     * @return This Alert
     */
    public static Alert create(@NonNull final Activity activity) {
        final Alert alert = new Alert();

        Alert.clearCurrent(activity);

        alert.setActivity(activity);
        alert.setAlertLayout(new AlertLayout(activity));

        return alert;
    }

    /**
     * Cleans up the currently showing alert view, if one is present
     *
     * @param activity The current Activity
     */
    public static void clearCurrent(final Activity activity) {
        if (activity == null) {
            return;
        }

        try {
            final ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();

            //Find all Alert Views in Parent layout
            for (int i = 0; i < decorView.getChildCount(); i++) {
                final AlertLayout childView = decorView.getChildAt(i) instanceof AlertLayout ? (AlertLayout) decorView.getChildAt(i) : null;
                if (childView != null && childView.getWindowToken() != null) {
                    ViewCompat.animate(childView).alpha(0).withEndAction(getRemoveViewRunnable(childView));
                }
            }

        } catch (Exception ex) {
            Log.e(Alert.class.getClass().getSimpleName(), Log.getStackTraceString(ex));
        }
    }

    @NonNull
    private static Runnable getRemoveViewRunnable(final AlertLayout childView) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    ((ViewGroup) childView.getParent()).removeView(childView);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), Log.getStackTraceString(e));
                }
            }
        };
    }

    /**
     * Shows the Alert, after it's built
     *
     * @return An Alert object check can be altered or hidden
     */
    public AlertLayout show() {
        if (getActivityWeakReference() != null) {
            getActivityWeakReference().get().runOnUiThread(() -> {
                final ViewGroup decorView = getActivityDecorView();
                if (decorView != null && getAlertLayout().getParent() == null) {
                    decorView.addView(getAlertLayout());
                }
            });
        }

        return getAlertLayout();
    }

    /**
     * Sets the Alert Text
     *
     * @param textId Text String Resource
     * @return This Alert
     */
    public Alert setText(@StringRes final int textId) {
        if (getAlertLayout() != null) {
            getAlertLayout().setText(textId);
        }

        return this;
    }

    /**
     * Sets the Alert Text
     *
     * @param text String of Alert Text
     * @return This Alert
     */
    public Alert setText(final String text) {
        if (getAlertLayout() != null) {
            getAlertLayout().setText(text);
        }

        return this;
    }

    /**
     * Set the onClickListener for the Alert
     *
     * @param onClickListener The onClickListener for the Alert
     * @return This Alert
     */
    public Alert setOnClickListener(@NonNull final View.OnClickListener onClickListener) {
        if (getAlertLayout() != null) {
            getAlertLayout().setOnClickListener(onClickListener);
        }

        return this;
    }

    /**
     * Set the on screen duration of the alert
     *
     * @param milliseconds The duration in milliseconds
     * @return This Alert
     */
    public Alert setDuration(final long milliseconds) {
        if (getAlertLayout() != null) {
            getAlertLayout().setDuration(milliseconds);
        }
        return this;
    }

    public Alert setBackgroundColor(int color) {
        if (getAlertLayout() != null) {
            getAlertLayout().setBackgroundColor(color);
        }
        return this;
    }

    public Alert setTextColor(int color) {
        if (getAlertLayout() != null) {
            getAlertLayout().getText().setTextColor(color);
        }
        return this;
    }

    /**
     * Enables swipe to dismiss
     *
     * @return This Alert
     */
    public Alert enableSwipeToDismiss() {
        if (getAlertLayout() != null) {
            getAlertLayout().enableSwipeToDismiss();
        }
        return this;
    }

    /**
     * Gets the Alert associated with the Alert
     *
     * @return The current Alert
     */
    private AlertLayout getAlertLayout() {
        return alertLayout;
    }

    /**
     * Sets the Alert
     *
     * @param alertLayout The Alert to be references and maintained
     */
    private void setAlertLayout(final AlertLayout alertLayout) {
        this.alertLayout = alertLayout;
    }

    @Nullable
    private WeakReference<Activity> getActivityWeakReference() {
        return activityWeakReference;
    }

    /**
     * Get the enclosing Decor View
     *
     * @return The Decor View of the Activity the Alert was called from
     */
    @Nullable
    private ViewGroup getActivityDecorView() {
        ViewGroup decorView = null;

        if (getActivityWeakReference() != null && getActivityWeakReference().get() != null) {
            decorView = (ViewGroup) getActivityWeakReference().get().getWindow().getDecorView();
        }

        return decorView;
    }

    /**
     * Creates a weak reference to the calling Activity
     *
     * @param activity The calling Activity
     */
    private void setActivity(@NonNull final Activity activity) {
        activityWeakReference = new WeakReference<>(activity);
    }
}