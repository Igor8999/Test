package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.StatusModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IAuthService {

    Observable<SignModel> signIn(Map<String, String> params);

    Observable<StatusModel> recoveryPassword(Map<String, String> params);

    Observable<SignModel> signUp(MultipartBody.Part fileImage,
                                 RequestBody userName,
                                 RequestBody password,
                                 RequestBody fName,
                                 RequestBody lName,
                                 RequestBody phone,
                                 RequestBody email);

    Observable<StatusModel> changePassword(Map<String, String> params);
}
