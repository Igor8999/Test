package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailableDoorModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("publicName")
    @Expose
    private String publicName;
    @SerializedName("address")
    @Expose
    private String address;

    public int stateAvalable;

    @TypeAvalableDoorbells
    public int getStateProvideAccess() {
        return stateAvalable;
    }

    public void setStateProvideAccess(@TypeAvalableDoorbells int stateAvailable) {
        this.stateAvalable = stateAvailable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublicName() {
        return publicName;
    }

    public void setPublicName(String publicName) {
        this.publicName = publicName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}

