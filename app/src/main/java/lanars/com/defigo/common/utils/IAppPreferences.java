package lanars.com.defigo.common.utils;

import java.util.List;

import lanars.com.defigo.common.network.models.DayOfWeek;

public interface IAppPreferences {
    boolean saveAccessToken(String accessToken);

    String getAccessToken();

    boolean saveCurrentUserId(long currentUserId);

    long getCurrentUserId();

    boolean saveMuteDays(List<DayOfWeek> muteDays, String keyMute);

    List<DayOfWeek> getMuteDays(String keyMute);

    boolean clearData();

    boolean savePin(String pinCode, String typePinCode);

    String getPin(String typePinCode);

    boolean saveCurrentSound(String soundUrl, String canonicalPath);

    String getCurrentSoundFilePath();

    String getCurrentSoundUrl();
}
