package lanars.com.defigo.common.api;

import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.RoomModel;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RoomsApi {

    @POST("/api/rooms")
    Observable<Response<RoomModel>> addNewRoom(@Body Map<String, Object> params);

    @POST("/api/rooms/users")
    Observable<Response<ResponseBody>> addNewRoomsUsers(@Body Map<String, Object> params);

    @Multipart
    @PATCH("/api/rooms/{roomId}/info")
    Observable<Response<UpdateRoomModel>> updateInfoRoom(@Path("roomId") long roomId,
                                                         @Part MultipartBody.Part file,
                                                         @Part("header") RequestBody header,
                                                         @Part("text") RequestBody text,
                                                         @Part("isSingleButton") RequestBody isSingleButton);

    @PATCH("/api/rooms/doorbells/{doorbellId}/visibility")
    Observable<Response<ResponseBody>> changeVisibilityOptionsFamily(@Body Map<String, Object> params, @Path("doorbellId") long doorbellId);
}
