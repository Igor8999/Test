package lanars.com.defigo.common.di.modules.database;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lanars.com.defigo.database.AppDatabase;

@Module
public class DatabaseModule {

    public DatabaseModule() {
    }

    @Provides
    @NonNull
    @Singleton
    public AppDatabase provideAppDatabase(Context appContext) {
        return Room
                .databaseBuilder(appContext, AppDatabase.class, "database-name")
                .fallbackToDestructiveMigration()
                .build();
    }

}
