package lanars.com.defigo.common.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatFragment;

import lanars.com.defigo.common.ui.LoadingProgressDialog;
import lanars.com.defigo.common.ui.alert.Alert;

public class BaseFragment extends MvpAppCompatFragment implements BaseMvpView {

    LoadingProgressDialog loadingProgressDialog;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingProgressDialog = new LoadingProgressDialog(getActivity());
    }

    @Override
    public void onShowError(String errorMessage) {
        if (getActivity() != null) {
            Alert.create(getActivity())
                    .setText(errorMessage)
                    .setDuration(500)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    @Override
    public void onShowProgress() {
        loadingProgressDialog.show();

    }

    @Override
    public void onHideProgress() {
        loadingProgressDialog.dismiss();
    }
}
