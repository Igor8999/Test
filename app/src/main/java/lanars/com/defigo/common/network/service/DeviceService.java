package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.DevicesApi;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class DeviceService implements IDeviceService {

    private DevicesApi devicesApi;

    public DeviceService(DevicesApi devicesApi) {
        this.devicesApi = devicesApi;
    }

    @Override
    public Observable<Response<ResponseBody>> addNewDevice(Map<String, Object> params) {
        return devicesApi.addNewDevice(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<ResponseBody>> deleteDevice(String deviceId) {
        return devicesApi.deleteDevice(deviceId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<ResponseBody>> updateDeviceToken(Map<String, Object> params, long deviceId) {
        return devicesApi.updateDeviceToken(params, deviceId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
