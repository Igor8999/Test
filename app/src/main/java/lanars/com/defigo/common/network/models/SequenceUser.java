package lanars.com.defigo.common.network.models;

//todo this is stub model
public class SequenceUser {

    private String name;

    private int indexCall;

    private boolean isCallEnabled;

    public SequenceUser(String name, int indexCall, boolean isCallEnabled, boolean isSequenceType) {
        this.name = name;
        this.indexCall = indexCall;
        this.isCallEnabled = isCallEnabled;
        this.isSequenceType = isSequenceType;
    }

    private boolean isSequenceType;

    public boolean isSequenceType() {
        return isSequenceType;
    }

    public void setSequenceType(boolean sequenceType) {
        isSequenceType = sequenceType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndexCall() {
        return indexCall;
    }

    public void setIndexCall(int indexCall) {
        this.indexCall = indexCall;
    }

    public boolean isCallEnabled() {
        return isCallEnabled;
    }

    public void setCallEnabled(boolean callEnabled) {
        isCallEnabled = callEnabled;
    }
}
