package lanars.com.defigo.common.network.models.roomsettings;

import com.google.gson.annotations.SerializedName;

public class BuildingRules {
    @SerializedName("enableImg")
    private boolean isImageEnabled;

    public boolean isImageEnabled() {
        return isImageEnabled;
    }

    public void setImageEnabled(boolean imageEnabled) {
        isImageEnabled = imageEnabled;
    }
}
