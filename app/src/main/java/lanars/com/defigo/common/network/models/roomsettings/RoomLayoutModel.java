package lanars.com.defigo.common.network.models.roomsettings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoomLayoutModel {

    @SerializedName("currentUser")
    @Expose
    private CurrentUser currentUser;
    @SerializedName("roomSettings")
    @Expose
    private RoomSettings roomSettings;
    @SerializedName("individualSettings")
    @Expose
    private IndividualSettings individualSettings;

    private BuildingRules buildingRules;

    public CurrentUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }

    public RoomSettings getRoomSettings() {
        return roomSettings;
    }

    public void setRoomSettings(RoomSettings roomSettings) {
        this.roomSettings = roomSettings;
    }

    public IndividualSettings getIndividualSettings() {
        return individualSettings;
    }

    public void setIndividualSettings(IndividualSettings individualSettings) {
        this.individualSettings = individualSettings;
    }

    public BuildingRules getBuildingRules() {
        return buildingRules;
    }

    public void setBuildingRules(BuildingRules buildingRules) {
        this.buildingRules = buildingRules;
    }
}