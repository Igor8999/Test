package lanars.com.defigo.common.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import lanars.com.defigo.common.network.models.ErrorBody;
import retrofit2.Response;

public class NetworkClientUtil {

    private Gson gson;

    public NetworkClientUtil(Gson gson) {
        this.gson = gson;
    }

    public ErrorBody parseNetworkError(Response response) {
        JsonParser parser = new JsonParser();
        JsonElement mJson;
        try {
            mJson = parser.parse(response.errorBody().string());
            return gson.fromJson(mJson, ErrorBody.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public <T> ObservableSource<T> onHandleCodeResponse(Response<T> response) {
        if (response.code() == 200 || response.code() == 201) {
            return Observable.just(response.body());
        }
        return Observable.error(new Throwable(parseNetworkError(response).getMessage()));
    }
}
