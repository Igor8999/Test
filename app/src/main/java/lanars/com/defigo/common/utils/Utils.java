package lanars.com.defigo.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import lanars.com.defigo.R;

public class Utils {

    public static void hideKeyboard(Activity contextActivity) {
        InputMethodManager inputManager = (InputMethodManager) contextActivity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = contextActivity.getCurrentFocus();
        if (view == null) {
            return;
        }
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isConnected(@NonNull final Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            return wifi != null && wifi.isConnected() || mobile != null && mobile.isConnected();
        } else {
            boolean connected = false;
            for (Network network : connMgr.getAllNetworks()) {
                NetworkInfo info = connMgr.getNetworkInfo(network);
                connected |= info != null && info.isConnected();
            }
            return connected;
        }
    }

    public static void showPermissionRationaleAlert(Context context, int rationaleTextResId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(rationaleTextResId)
                .setPositiveButton(R.string.go_to_settings, (dialog, id) -> {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", context.getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> dialog.cancel());

        builder.show();
    }
}
