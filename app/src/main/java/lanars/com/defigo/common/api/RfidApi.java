package lanars.com.defigo.common.api;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.RfidModel;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;

public interface RfidApi {

    @GET("/api/users/rfid")
    Observable<Response<List<RfidModel>>> getRfidList();

    @PATCH("/api/users/rfid/{id}")
    Observable<Response<RfidModel>> updateRfid(@Path("id") int roomId,
                                               @Body Map<String, Object> params);
}
