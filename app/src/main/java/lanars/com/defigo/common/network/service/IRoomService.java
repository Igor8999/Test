package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.RoomModel;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public interface IRoomService {

    Observable<Response<RoomModel>> addNewRoom(Map<String, Object> params);

    Observable<Response<ResponseBody>> addNewRoomsUsers(Map<String, Object> params);

    Observable<UpdateRoomModel> updateInfoRoom(long roomId,
                                                         MultipartBody.Part file,
                                                         RequestBody header,
                                                         RequestBody text,
                                                         RequestBody isSingleButton);

    Observable<UpdateRoomModel> updateInfoRoom(long roomId,
                                               RequestBody header,
                                               RequestBody text,
                                               RequestBody isSingleButton);

    Observable<ResponseBody> changeVisibilityOptionsFamily(Map<String, Object> params, long doorbellId);
}
