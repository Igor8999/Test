package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

public interface IBuldingService {

    Observable<ResponseBody> addNewBuilding(Map<String, Object> params);
}
