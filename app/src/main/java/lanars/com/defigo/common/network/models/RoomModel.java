package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoomModel {

    @SerializedName("typeOfCall")
    @Expose
    private String typeOfCall;
    @SerializedName("isSingleButton")
    @Expose
    private Boolean isSingleButton;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("buildingId")
    @Expose
    private Integer buildingId;
    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("ownerId")
    @Expose
    private Object ownerId;
    @SerializedName("imgUrl")
    @Expose
    private Object imgUrl;
    @SerializedName("imgKey")
    @Expose
    private Object imgKey;

    public String getTypeOfCall() {
        return typeOfCall;
    }

    public void setTypeOfCall(String typeOfCall) {
        this.typeOfCall = typeOfCall;
    }

    public Boolean getIsSingleButton() {
        return isSingleButton;
    }

    public void setIsSingleButton(Boolean isSingleButton) {
        this.isSingleButton = isSingleButton;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
        this.buildingId = buildingId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Object ownerId) {
        this.ownerId = ownerId;
    }

    public Object getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(Object imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Object getImgKey() {
        return imgKey;
    }

    public void setImgKey(Object imgKey) {
        this.imgKey = imgKey;
    }

}