package lanars.com.defigo.common.api;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UsersApi {

    @GET("/api/users")
    Observable<Response<List<User>>> users(@Query("search") String search, @Query("offset") int offset, @Query("limit") int limit);

    @GET("/api/users")
    Observable<Response<List<User>>> users();

    @GET("/api/users/my")
    Observable<Response<User>> usersMy();

    @Multipart
    @PATCH("/api/users/my")
    Observable<Response<User>> usersMyUpdate(
            @Part MultipartBody.Part fileImage,
            @Part("userName") RequestBody userName,
            @Part("email") RequestBody email,
            @Part("lName") RequestBody lName,
            @Part("fName") RequestBody fName,
            @Part("phone") RequestBody phone,
            @Part("street") RequestBody street,
            @Part("zip") RequestBody zip,
            @Part("country") RequestBody country,
            @Part("city") RequestBody city,
            @Part("hasVideo") RequestBody hasVideo,
            @Part("currentSound") RequestBody currentSound);

    @GET("/api/users/doorbells")
    Observable<Response<ResponseBody>> getAllUsersDoorbels();

    @GET("/api/users/doorbells/visibility")
    Observable<Response<List<VisibilityDoorModel>>> getUsersDoorbelsVisibility();

    @PATCH("/api/users/doorbells/{doorbellId}/visibility")
    Observable<Response<ResponseBody>> changeVisibilityOptionsIndividual(@Body Map<String, Object> params, @Path("doorbellId") long doorbellId);

    @GET("/api/users/rooms/{roomId}/info")
    Observable<Response<RoomLayoutModel>> getRoomLayoutSettingsUser(@Path("roomId") long roomId);

    @Multipart
    @PATCH("/api/users/rooms/{roomId}/info")
    Observable<Response<UpdateRoomModel>> updateInfoRoom(@Path("roomId") long roomId,
                                                         @Part MultipartBody.Part file,
                                                         @Part("header") RequestBody header,
                                                         @Part("text") RequestBody text);

    @GET("/api/users/rooms/{roomId}/actions")
    Observable<Response<ActionsOptionsModel>> getActionOptions(@Path("roomId") long roomId, @Query("type") String type);

    @PATCH("/api/users/rooms/{roomId}/actions")
    Observable<Response<StatusModel>> updateRulesActions(@Path("roomId") long roomId, @Body Map<String, Object> params);

    //provide access
    @GET("/api/users/access")
    Observable<Response<List<User>>> getSharedAccessByUser();

    @PATCH("/api/users/access")
    Observable<Response<StatusModel>> shareAccessToUser(@Body Map<String, Object> params);

    @DELETE("/api/users/access")
    Observable<Response<StatusModel>> removeSharedAccess(@Query("guestId") long guestId);

}
