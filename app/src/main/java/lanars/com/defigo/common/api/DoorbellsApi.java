package lanars.com.defigo.common.api;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.AvailableDoorModel;
import lanars.com.defigo.common.network.models.DoorbellModel;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DoorbellsApi {

    @POST("/api/doorbells")
    Observable<Response<DoorbellModel>> addNewDoorbells(@Body Map<String, Object> params);

    @DELETE("/api/doorbells/{doorbellId}")
    Observable<Response<ResponseBody>> deleteDoorbell(@Path("doorbellId") int doorbellId);

    @GET("/api/users/doorbells")
    Observable<Response<List<AvailableDoorModel>>> getAvailableDoorbells();

    @GET("/api/doorbells/{doorbellId}/open")
    Observable<Response<ResponseBody>> openDoorbell(@Path("doorbellId") int doorbellId);
}
