package lanars.com.defigo.common.api;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface BuildingsApi {

    @POST("/api/buildings")
    Observable<Response<ResponseBody>> addNewBuilding(@Body Map<String, Object> params);
}
