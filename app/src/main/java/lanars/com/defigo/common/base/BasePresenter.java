package lanars.com.defigo.common.base;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.MvpViewState;

import java.util.Set;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<View extends MvpView> extends MvpPresenter<View> {
    private CompositeDisposable compositeSubscription = new CompositeDisposable();

    public void addToDisposable(@NonNull Disposable disposable) {
        compositeSubscription.add(disposable);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);
    }

    @Override
    public void detachView(View view) {
        super.detachView(view);
    }

    @Override
    public void destroyView(View view) {
        super.destroyView(view);
    }

    @Override
    public Set<View> getAttachedViews() {
        return super.getAttachedViews();
    }

    @Override
    public boolean isInRestoreState(View view) {
        return super.isInRestoreState(view);
    }

    @Override
    public void setViewState(MvpViewState<View> viewState) {
        super.setViewState(viewState);
    }

    @Override
    public void onDestroy() {
        compositeSubscription.clear();
        super.onDestroy();
    }

}
