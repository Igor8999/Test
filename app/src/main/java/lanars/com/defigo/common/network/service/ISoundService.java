package lanars.com.defigo.common.network.service;

import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.SoundModel;

public interface ISoundService {
    Observable<List<SoundModel>> getSoundsList();
}
