package lanars.com.defigo.common.network.service;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.models.EntranceModel;
import lanars.com.defigo.common.api.AdminApi;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;

public class AdminService implements IAdminService {

    private AdminApi adminApi;

    private NetworkClientUtil networkClientUtil;

    public AdminService(AdminApi adminApi, NetworkClientUtil networkClientUtil) {
        this.adminApi = adminApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<List<BuildingModel>> getBuildings() {
        return adminApi.getBuildings()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<List<EntranceModel>> getEntrances(int buildingId) {
        return adminApi.getEntrances(buildingId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<List<ApartmentModel>> getApartments(int buildingId) {
        return adminApi.getApartment(buildingId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<List<RfidModel>> getCards(long userId) {
        return adminApi.getCards(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<Object> deleteCard(int cardId) {
        return adminApi.deleteCard(cardId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<RfidModel> addCard(Map<String, Object> params) {
        return adminApi.addCard(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}
