package lanars.com.defigo.common.repository.user;

import java.util.List;

import io.reactivex.Observable;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.models.CardMock;
import lanars.com.defigo.admin.models.EntranceModel;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.network.models.LanguageModel;
import lanars.com.defigo.common.network.models.NotificationModel;
import lanars.com.defigo.common.network.models.SoundModel;

public class DataMockRepository implements IDataRepository {
    @Override
    public Observable<List<DayOfWeek>> getAllDaysOfWeek(String keyMute) {
        return null;
    }

    @Override
    public void saveDayOfWeeks(List<DayOfWeek> dayOfWeeks, String keyMute) {

    }

    @Override
    public Observable<List<NotificationModel>> getNotificationsSettings() {
        return null;
    }

    @Override
    public Observable<List<SoundModel>> getSounds() {
        return null;
    }

    @Override
    public Observable<List<LanguageModel>> getLanguages() {
        return null;
    }

    @Override
    public Observable<List<BuildingModel>> getBuildings() {
        return null;
    }

    @Override
    public Observable<List<EntranceModel>> getEntrances() {
        return null;
    }

    @Override
    public Observable<List<ApartmentModel>> getApartments() {
        return null;
    }

    @Override
    public Observable<List<CardMock>> getCards() {
        return null;
    }
}
