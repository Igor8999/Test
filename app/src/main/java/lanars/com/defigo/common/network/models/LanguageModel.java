package lanars.com.defigo.common.network.models;

public class LanguageModel {

    private String name;

    private String localeName;

    public LanguageModel(String name, String localeName, boolean isSelected) {
        this.name = name;
        this.localeName = localeName;
        this.isSelected = isSelected;
    }

    private boolean isSelected;

    public String getName() {
        return name;
    }

    public String getLocaleName() {
        return localeName;
    }

    public void setLocaleName(String localeName) {
        this.localeName = localeName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LanguageModel(String name) {

        this.name = name;
    }

    public LanguageModel(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
