package lanars.com.defigo.common.network.service;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.models.EntranceModel;
import lanars.com.defigo.common.network.models.RfidModel;

public interface IAdminService {

    Observable<List<BuildingModel>> getBuildings();

    Observable<List<EntranceModel>> getEntrances(int buildingId);

    Observable<List<ApartmentModel>> getApartments(int buildingId);

    Observable<List<RfidModel>> getCards(long userId);

    Observable<Object> deleteCard(int cardId);

    Observable<RfidModel> addCard(Map<String, Object> params);
}
