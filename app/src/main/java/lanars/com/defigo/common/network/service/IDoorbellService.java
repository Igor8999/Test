package lanars.com.defigo.common.network.service;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.common.network.models.AvailableDoorModel;
import lanars.com.defigo.common.network.models.DoorbellModel;
import okhttp3.ResponseBody;
import retrofit2.Response;

public interface IDoorbellService {

    Observable<Response<DoorbellModel>> addNewDoorbells(Map<String, Object> params);

    Observable<Response<ResponseBody>> deleteDoorbell(int doorbellId);

    Observable<Response<List<AvailableDoorModel>>> getAvailableDoorbells();

    Observable<ResponseBody> openDoorbell(int doorbellId);
}
