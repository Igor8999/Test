package lanars.com.defigo.common.network.models;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static lanars.com.defigo.common.network.models.TypeVisibilityOptions.TYPE_CALL_VISIBILITY;
import static lanars.com.defigo.common.network.models.TypeVisibilityOptions.TYPE_DISABLE_VISIBILITY;
import static lanars.com.defigo.common.network.models.TypeVisibilityOptions.TYPE_SCHEDULE_VISABILITY;

@Retention(RetentionPolicy.SOURCE)
@IntDef({TYPE_CALL_VISIBILITY, TYPE_DISABLE_VISIBILITY, TYPE_SCHEDULE_VISABILITY})
public @interface TypeVisibilityOptions {

    int TYPE_CALL_VISIBILITY = 0;

    int TYPE_DISABLE_VISIBILITY = 1;

    int TYPE_SCHEDULE_VISABILITY = 2;
}
