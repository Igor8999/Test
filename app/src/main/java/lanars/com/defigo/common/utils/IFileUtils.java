package lanars.com.defigo.common.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import java.io.File;

public interface IFileUtils {
    @Nullable
    File getDiskCacheDir(Context context, String uniqueName);

    void saveBitmapIntoFile(File filename, Bitmap bmp);

    boolean checkExistFile(Context context, String nameFile);

    Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels);

    String getUniqueFileName(String fileName);

    void deleteFile(File file);
}
