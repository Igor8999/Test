package lanars.com.defigo.common.network.models;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static lanars.com.defigo.common.network.models.TypeAccessUser.STATE_CLOSE_ACCESS;
import static lanars.com.defigo.common.network.models.TypeAccessUser.STATE_OPEN_ACCESS_ALWAYS;
import static lanars.com.defigo.common.network.models.TypeAccessUser.STATE_OPEN_ACCESS_ON_TIME;

@Retention(RetentionPolicy.SOURCE)
@IntDef({STATE_CLOSE_ACCESS, STATE_OPEN_ACCESS_ALWAYS, STATE_OPEN_ACCESS_ON_TIME})
public @interface TypeAccessUser {

    int STATE_CLOSE_ACCESS = 0;

    int STATE_OPEN_ACCESS_ON_TIME = 1;

    int STATE_OPEN_ACCESS_ALWAYS = 2;
}
