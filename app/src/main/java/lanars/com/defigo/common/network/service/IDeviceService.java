package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;

public interface IDeviceService {

    Observable<Response<ResponseBody>> addNewDevice(Map<String, Object> params);

    Observable<Response<ResponseBody>> deleteDevice(String deviceId);

    Observable<Response<ResponseBody>> updateDeviceToken(Map<String, Object> params, long deviceId);

}
