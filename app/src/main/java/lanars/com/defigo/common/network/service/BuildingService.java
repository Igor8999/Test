package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.BuildingsApi;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import okhttp3.ResponseBody;

public class BuildingService implements IBuldingService {

    private BuildingsApi buildingsApi;

    private NetworkClientUtil networkClientUtil;

    public BuildingService(BuildingsApi buildingsApi, NetworkClientUtil networkClientUtil) {
        this.buildingsApi = buildingsApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<ResponseBody> addNewBuilding(Map<String, Object> params) {
        return buildingsApi.addNewBuilding(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}
