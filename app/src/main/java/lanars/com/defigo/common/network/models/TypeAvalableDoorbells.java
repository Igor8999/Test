package lanars.com.defigo.common.network.models;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static lanars.com.defigo.common.network.models.TypeAvalableDoorbells.STATE_CLOSE;
import static lanars.com.defigo.common.network.models.TypeAvalableDoorbells.STATE_OPEN;

@Retention(RetentionPolicy.SOURCE)
@IntDef({STATE_OPEN, STATE_CLOSE})
public @interface TypeAvalableDoorbells {

    int STATE_CLOSE = 0;

    int STATE_OPEN = 1;

}
