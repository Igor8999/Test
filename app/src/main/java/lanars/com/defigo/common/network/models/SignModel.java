package lanars.com.defigo.common.network.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignModel {

    @SerializedName("accessToken")
    @Expose
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}