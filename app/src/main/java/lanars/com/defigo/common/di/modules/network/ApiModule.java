package lanars.com.defigo.common.di.modules.network;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lanars.com.defigo.common.api.AdminApi;
import lanars.com.defigo.common.api.AuthApi;
import lanars.com.defigo.common.api.BuildingsApi;
import lanars.com.defigo.common.api.DevicesApi;
import lanars.com.defigo.common.api.DoorbellsApi;
import lanars.com.defigo.common.api.RfidApi;
import lanars.com.defigo.common.api.RoomsApi;
import lanars.com.defigo.common.api.SoundApi;
import lanars.com.defigo.common.api.UsersApi;
import retrofit2.Retrofit;

@Module(includes = {RetrofitModule.class})
public class ApiModule {

    @Provides
    @Singleton
    public BuildingsApi provideBuildingsApi(Retrofit retrofit) {
        return retrofit.create(BuildingsApi.class);
    }

    @Provides
    @Singleton
    public DevicesApi provideDevicesApi(Retrofit retrofit) {
        return retrofit.create(DevicesApi.class);
    }

    @Provides
    @Singleton
    public DoorbellsApi provideDoorbelsApi(Retrofit retrofit) {
        return retrofit.create(DoorbellsApi.class);
    }

    @Provides
    @Singleton
    public AuthApi provideAuthApi(Retrofit retrofit) {
        return retrofit.create(AuthApi.class);
    }

    @Provides
    @Singleton
    public AdminApi provideAdminApi(Retrofit retrofit) {
        return retrofit.create(AdminApi.class);
    }

    @Provides
    @Singleton
    public RoomsApi provideRoomsApi(Retrofit retrofit) {
        return retrofit.create(RoomsApi.class);
    }

    @Provides
    @Singleton
    public UsersApi provideUsersApi(Retrofit retrofit) {
        return retrofit.create(UsersApi.class);
    }

    @Provides
    @Singleton
    public SoundApi provideSoundApi(Retrofit retrofit) {
        return retrofit.create(SoundApi.class);
    }

    @Provides
    @Singleton
    public RfidApi provideRfidApi(Retrofit retrofit) {
        return retrofit.create(RfidApi.class);
    }
}
