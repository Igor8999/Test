package lanars.com.defigo.common.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lanars.com.defigo.NetworkStateObservable;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.network.service.ISoundService;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.DataRepositoryImpl;
import lanars.com.defigo.common.repository.user.IDataRepository;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.repository.user.UserRepositoryImpl;
import lanars.com.defigo.common.utils.AppPreferences;
import lanars.com.defigo.common.utils.FileUtils;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import lanars.com.defigo.database.AppDatabase;

@Module
public class DataModule {

    private Context appContext;

    public DataModule(Context context) {
        this.appContext = context;
    }

    @Provides
    @Singleton
    public Context getContext() {
        return appContext;
    }

    @Provides
    @NonNull
    @Singleton
    public IAppPreferences getPreferences(Context appContext, Gson gson) {
        return new AppPreferences(appContext, gson);
    }

    @Provides
    @NonNull
    @Singleton
    public SocketManager getSocketManager() {
        return new SocketManager();
    }

    @Provides
    @NonNull
    @Singleton
    public NetworkClientUtil getNetworkUtil(Gson gson) {
        return new NetworkClientUtil(gson);
    }

    @Provides
    @NonNull
    @Singleton
    public IFileUtils provideFileUtils() {
        return new FileUtils();
    }

    @Provides
    @Singleton
    public IDataRepository provideDataRepository(IAppPreferences appPreferences, ISoundService soundService) {
        return new DataRepositoryImpl(appPreferences, soundService);
    }

    @Provides
    @Singleton
    public IUserRepository provideUserRepository(AppDatabase appDatabase, IAuthService authService, IUserService userService, NetworkClientUtil networkClientUtil) {
        return new UserRepositoryImpl(appDatabase, authService, userService, networkClientUtil);
    }

    @Provides
    @Singleton
    public NetworkStateObservable networkStateObservable() {
        return new NetworkStateObservable();
    }

}
