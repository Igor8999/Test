package lanars.com.defigo.common.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActionsOptionsModel {

    @SerializedName("isOwnerCurrentUser")
    @Expose
    private Boolean isOwner;
    @SerializedName("isSingleButton")
    @Expose
    private Boolean isSingleButton;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("daysOfWeek")
    @Expose
    private List<Integer> daysOfWeek;

    public Boolean getIsOwner() {
        return isOwner;
    }

    public void setIsOwner(Boolean isOwner) {
        this.isOwner = isOwner;
    }

    public Boolean getIsSingleButton() {
        return isSingleButton;
    }

    public void setIsSingleButton(Boolean isSingleButton) {
        this.isSingleButton = isSingleButton;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public List<Integer> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<Integer> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

}
