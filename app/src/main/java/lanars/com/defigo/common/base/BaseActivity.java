package lanars.com.defigo.common.base;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.NetworkStateObservable;

public class BaseActivity extends MvpAppCompatActivity {

    @Inject
    NetworkStateObservable networkStateObservable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DefigoApplication.getInstance().getAppComponent().inject(this);
        networkStateObservable.register(this).toObservable()
                .subscribe(this::changeNetworkConnection);
    }

    public void changeNetworkConnection(boolean isConnection) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // TODO fix me
        try {
            networkStateObservable.unRegister(this);
        } catch (Throwable throwable) {
            Crashlytics.logException(throwable);
        }
    }
}
