package lanars.com.defigo.common.network.service;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.DoorbellsApi;
import lanars.com.defigo.common.network.models.AvailableDoorModel;
import lanars.com.defigo.common.network.models.DoorbellModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class DoorbellService implements IDoorbellService {

    private DoorbellsApi doorbellsApi;

    private NetworkClientUtil networkClientUtil;

    public DoorbellService(DoorbellsApi doorbellsApi, NetworkClientUtil networkClientUtil) {
        this.doorbellsApi = doorbellsApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<Response<DoorbellModel>> addNewDoorbells(Map<String, Object> params) {
        return doorbellsApi.addNewDoorbells(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<ResponseBody>> deleteDoorbell(int doorbellId) {
        return doorbellsApi.deleteDoorbell(doorbellId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<List<AvailableDoorModel>>> getAvailableDoorbells() {
        return doorbellsApi.getAvailableDoorbells()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> openDoorbell(int doorbellId) {
        return doorbellsApi.openDoorbell(doorbellId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}
