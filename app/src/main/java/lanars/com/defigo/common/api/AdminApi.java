package lanars.com.defigo.common.api;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.models.EntranceModel;
import lanars.com.defigo.common.network.models.RfidModel;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AdminApi {

    @GET("/api/admin/buildings")
    Observable<Response<List<BuildingModel>>> getBuildings();

    //TODO dont use
    @GET("/api/admin/doorbells/{buildingId}")
    Observable<Response<List<EntranceModel>>> getEntrances(@Path("buildingId") int buildingId);

    @GET("/api/admin/building/{buildingId}")
    Observable<Response<List<ApartmentModel>>> getApartment(@Path("buildingId") int buildingId);

    @GET("/api/admin/users/rfid/{userId}")
    Observable<Response<List<RfidModel>>> getCards(@Path("userId") long  userId);

    @DELETE("/api/admin/rfid/{id}")
    Observable<Response<Object>> deleteCard(@Path("id") int cardId);

    @POST("/api/admin/rfid")
    Observable<Response<RfidModel>> addCard(@Body Map<String, Object> params);
}
