package lanars.com.defigo.common.network.service;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.SoundApi;
import lanars.com.defigo.common.network.models.SoundModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;

public class SoundService implements ISoundService {

    private SoundApi soundApi;
    private NetworkClientUtil networkClientUtil;

    public SoundService(SoundApi soundApi, NetworkClientUtil networkClientUtil) {
        this.soundApi = soundApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<List<SoundModel>> getSoundsList() {
        return soundApi.getSoundsList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}

