package lanars.com.defigo.common.ui.listener;

public interface OnItemCallingSimultaneousClickListener {

    void onItemClickSimultaneous(int position);

}
