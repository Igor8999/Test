package lanars.com.defigo.common.network.service;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.api.RoomsApi;
import lanars.com.defigo.common.network.models.RoomModel;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.utils.NetworkClientUtil;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class RoomService implements IRoomService {

    private RoomsApi roomsApi;
    private NetworkClientUtil networkClientUtil;

    public RoomService(RoomsApi roomsApi, NetworkClientUtil networkClientUtil) {
        this.roomsApi = roomsApi;
        this.networkClientUtil = networkClientUtil;
    }

    @Override
    public Observable<Response<RoomModel>> addNewRoom(Map<String, Object> params) {
        return roomsApi.addNewRoom(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<ResponseBody>> addNewRoomsUsers(Map<String, Object> params) {
        return roomsApi.addNewRoomsUsers(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UpdateRoomModel> updateInfoRoom(long roomId, MultipartBody.Part file, RequestBody header, RequestBody text, RequestBody isSingleButton) {
        return roomsApi.updateInfoRoom(roomId, file, header, text, isSingleButton)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }

    @Override
    public Observable<UpdateRoomModel> updateInfoRoom(long roomId, RequestBody header, RequestBody text, RequestBody isSingleButton) {
        return null;
    }

    @Override
    public Observable<ResponseBody> changeVisibilityOptionsFamily(Map<String, Object> params, long doorbellId) {
        return roomsApi.changeVisibilityOptionsFamily(params, doorbellId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(networkClientUtil::onHandleCodeResponse);
    }
}
