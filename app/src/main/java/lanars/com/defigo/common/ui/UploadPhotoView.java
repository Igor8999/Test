package lanars.com.defigo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.R;
import lanars.com.defigo.common.utils.FileUtils;
import lanars.com.defigo.common.utils.IFileUtils;

public class UploadPhotoView extends FrameLayout {

    private ImageView imgSrc;
    private TextView tvEmpty;
    private FrameLayout frRoot;
    private ConstraintLayout constraintLayout;
    private IFileUtils fileUtils;

    public UploadPhotoView(@NonNull Context context) {
        super(context);
    }

    public UploadPhotoView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public UploadPhotoView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public UploadPhotoView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        fileUtils = new FileUtils();

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.UploadPhotoView);
        boolean isVisibilityEmptyText = typedArray.getBoolean(R.styleable.UploadPhotoView_textEmptyVisibility, false);
        String emptyText = typedArray.getString(R.styleable.UploadPhotoView_textEmpty);

        typedArray.recycle();

        inflate(getContext(), R.layout.upload_photo_view, this);

        imgSrc = findViewById(R.id.imgSrc);
        frRoot = findViewById(R.id.frRoot);
        tvEmpty = findViewById(R.id.tvEmptyText);
        constraintLayout = findViewById(R.id.constraintImage);
        tvEmpty.setVisibility(isVisibilityEmptyText ? VISIBLE : GONE);
        if (!TextUtils.isEmpty(emptyText)) {
            tvEmpty.setText(emptyText);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
    }

    public void setEmptyText(String emptyText) {
        tvEmpty.setText(emptyText);
    }

    // TODO Remove business logic from view
    public void uploadImage(String uniqueName) {
        if (!TextUtils.isEmpty(uniqueName)) {
            constraintLayout.setVisibility(VISIBLE);
            frRoot.setBackground(null);
            Observable.just(BitmapFactory.decodeFile(fileUtils.getDiskCacheDir(getContext(), uniqueName).getAbsolutePath()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::bitmapUpload);
            return;
        }
        constraintLayout.setVisibility(GONE);
        frRoot.setBackground(getContext().getDrawable(R.drawable.upload_photo));
    }

    public void uploadImageUrl(String imageUrl) {
        constraintLayout.setVisibility(VISIBLE);
        frRoot.setBackground(null);
        Glide.with(getContext())
                .load(imageUrl)
                .into(imgSrc);

    }

    private void bitmapUpload(Bitmap bitmap) {
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(
                getResources(),
                bitmap
        );
        roundedBitmapDrawable.setCornerRadius(25);
        imgSrc.setImageDrawable(roundedBitmapDrawable);
    }

}
