package lanars.com.defigo.admin.settings.dweller.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;

public class DwellerAdapter extends RecyclerView.Adapter<DwellerAdapter.ViewHolder> {

    private List<User> users;

    private OnItemClickRecyclerListener onItemClickRecyclerListener;

    public List<User> getUsers() {
        return users;
    }

    public DwellerAdapter(List<User> users, OnItemClickRecyclerListener onItemClickRecyclerListener) {
        this.users = users;
        this.onItemClickRecyclerListener = onItemClickRecyclerListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dwellers, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(v -> onItemClick(viewHolder));
        return viewHolder;
    }

    private void onItemClick(ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onItemClickRecyclerListener.onItemClick(position);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);
        holder.tvName.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}