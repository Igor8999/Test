package lanars.com.defigo.admin.settings.listener;

public interface OnItemCardDeleteListener {

    void onDelete(int position);
}
