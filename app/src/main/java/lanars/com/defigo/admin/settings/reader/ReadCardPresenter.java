package lanars.com.defigo.admin.settings.reader;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.AppConst;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ReadCardPresenter extends BasePresenter<ReadCardView> {

    @Inject
    Router router;

    @Inject
    IUserRepository userRepository;

    public ReadCardPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    @Override
    public void attachView(ReadCardView view) {
        super.attachView(view);
        getViewState().setCloseIcon(true);
    }

    @Override
    public void detachView(ReadCardView view) {
        super.detachView(view);
        getViewState().setCloseIcon(false);
    }

    public void goToEditCards(long userId, RfidModel selectedCard) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.CARD, selectedCard);
        bundle.putLong(AppConst.USER_ID, userId);

        router.navigateTo(RouteScreenName.EDIT_CARDS, bundle);
    }
}
