package lanars.com.defigo.admin.settings.reader;

import android.Manifest;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.math.BigInteger;

import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseFragment;
import lanars.com.defigo.admin.common.NfcReaderObservable;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.utils.AppConst;

public class ReadCardFragment extends AdminBaseFragment implements ReadCardView {

    @InjectPresenter
    ReadCardPresenter readCardPresenter;

    private RfidModel selectedCard;
    private long userId;

    private ReadNfcLoaderDialog readDialog;

    public static ReadCardFragment newInstance(Bundle bundle) {
        ReadCardFragment fragment = new ReadCardFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        selectedCard = (RfidModel) getArguments().getSerializable(AppConst.CARD);
        userId = getArguments().getLong(AppConst.USER_ID);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.read_card_fragment, container, false);
    }

    @Override
    protected void initViews(View view) {

    }

    @OnClick(R.id.btnReadCard)
    void readCardClick() {
        getNfcPermissions();
    }

    private void getNfcPermissions() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.NFC)
                .subscribe(this::onSuccessPermission, this::onErrorPermission);
    }

    private void onErrorPermission(Throwable throwable) {
        onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessPermission(Boolean isGrant) {
        if (isGrant) {
            startReadCard();
        }
    }

    private void startReadCard() {
        NfcReaderObservable.getInstance().
                requestReadNfc(getActivity())
                .toObservable()
                .doOnSubscribe(disposable -> onShowProgress())
                .doAfterTerminate(this::onHideProgress)
                .subscribe(this::setRfidIntent, throwable -> onShowError(throwable.getLocalizedMessage()));
    }

    @Override
    protected String getTitle() {
        return getString(R.string.add_card);
    }

    @Override
    public void handleBackClick() {
        readCardPresenter.onBack();
    }

    @Override
    public void setCloseIcon(boolean isClose) {
        imgBack.setImageDrawable(isClose ? getResources().getDrawable(R.drawable.ic_close_white_48px) : getResources().getDrawable(R.drawable.ic_arrow_back_white_48px));
    }

    public void setRfidIntent(Intent intent) {
        byte[] tagId = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
        BigInteger n = new BigInteger(tagId);
        String uuid = String.valueOf(Math.abs(n.longValue()));
        readCardSuccessfully(uuid);
    }

    private void readCardSuccessfully(String uuid) {
        selectedCard.setId(Integer.valueOf(uuid));
        readCardPresenter.goToEditCards(userId, selectedCard);
    }

    @Override
    public void onShowProgress() {
        readDialog = new ReadNfcLoaderDialog();
        readDialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onHideProgress() {
        if (readDialog != null) {
            readDialog.dismiss();
        }
    }
}
