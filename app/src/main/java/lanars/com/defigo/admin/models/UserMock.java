package lanars.com.defigo.admin.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class UserMock implements Parcelable {
    private String name;
    private List<CardMock> cardMocks;

    public UserMock(String name, List<CardMock> cardMocks) {
        this.name = name;
        this.cardMocks = cardMocks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CardMock> getCardMocks() {
        return cardMocks;
    }

    public void setCardMocks(List<CardMock> cardMocks) {
        this.cardMocks = cardMocks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeList(this.cardMocks);
    }

    protected UserMock(Parcel in) {
        this.name = in.readString();
        this.cardMocks = new ArrayList<CardMock>();
        in.readList(this.cardMocks, CardMock.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserMock> CREATOR = new Parcelable.Creator<UserMock>() {
        @Override
        public UserMock createFromParcel(Parcel source) {
            return new UserMock(source);
        }

        @Override
        public UserMock[] newArray(int size) {
            return new UserMock[size];
        }
    };
}
