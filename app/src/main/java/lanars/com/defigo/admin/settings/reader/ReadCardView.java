package lanars.com.defigo.admin.settings.reader;

import lanars.com.defigo.common.base.BaseMvpView;

public interface ReadCardView extends BaseMvpView {

    void setCloseIcon(boolean isClose);
}
