package lanars.com.defigo.admin.settings.dweller.detail;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IAdminService;
import lanars.com.defigo.common.utils.AppConst;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class DwellerCardsPresenter extends BasePresenter<DwellerCardsView> {

    private User selectedUser;

    @Inject
    Router router;

    @Inject
    IAdminService adminService;

    @Override
    public void attachView(DwellerCardsView view) {
        super.attachView(view);
        getAllCards();
    }

    private void getAllCards() {
        adminService.getCards(selectedUser.getId())
                .subscribe(this::onSuccessGetCards, this::onErrorGetCards);
    }

    private void onErrorGetCards(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessGetCards(List<RfidModel> cardMocks) {
        getViewState().setCards(cardMocks);
    }

    public DwellerCardsPresenter(User selectedUser) {
        this.selectedUser = selectedUser;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void goToReadCard(RfidModel rfidModel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.CARD, rfidModel);
        router.navigateTo(RouteScreenName.READ_CARDS, bundle);
    }

    public void goToEditCards(RfidModel rfidModel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.CARD, rfidModel);
        router.navigateTo(RouteScreenName.EDIT_CARDS, bundle);
    }

    public void addCard() {
        RfidModel rfidModel = new RfidModel();

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.CARD, rfidModel);
        bundle.putLong(AppConst.USER_ID, selectedUser.getId());

        router.navigateTo(RouteScreenName.READ_CARDS, bundle);
    }

    public void deleteCard(RfidModel rfidModel) {
        adminService.deleteCard(rfidModel.getId())
                .subscribe(this::onSuccess, this::onError);
    }

    private void onSuccess(Object o) {
        getAllCards();
    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }
}
