package lanars.com.defigo.admin.settings.edit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseFragment;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.utils.AppConst;

public class EditCardFragment extends AdminBaseFragment implements EditCardFragmentView {

    private RfidModel selectedCard;
    private long userId;

    @BindView(R.id.tvCardId)
    TextView tvCardId;

    @BindView(R.id.etCardName)
    EditText etCardName;

    @InjectPresenter
    EditCardFragmentPresenter editCardFragmentPresenter;

    @ProvidePresenter
    public EditCardFragmentPresenter provide() {
        userId = getArguments().getLong(AppConst.USER_ID);
        return new EditCardFragmentPresenter(userId);
    }

    public static EditCardFragment newInstance(Bundle bundle) {
        EditCardFragment fragment = new EditCardFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        selectedCard = (RfidModel) getArguments().getSerializable(AppConst.CARD);
        userId = getArguments().getInt(AppConst.USER_ID);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initViews(View view) {
        if (selectedCard != null) {
            tvCardId.setText(String.format("%s %s", getString(R.string.card_id), selectedCard.getId()));
            etCardName.setText(selectedCard.getName());
        }
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_white_48px));
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.edit_card_fragment, container, false);

    }

    @Override
    protected String getTitle() {
        return getString(R.string.add_card);
    }

    @OnClick(R.id.btnSave)
    void saveChanges() {
        editCardFragmentPresenter.addCard(selectedCard.getId(), etCardName.getText().toString());
    }

    @Override
    public void handleBackClick() {
        editCardFragmentPresenter.onBack();
    }

    @Override
    public void setCloseIcon(boolean isClose) {
        imgBack.setImageDrawable(isClose ? getResources().getDrawable(R.drawable.ic_close_white_48px) : getResources().getDrawable(R.drawable.ic_arrow_back_white_48px));
    }
}
