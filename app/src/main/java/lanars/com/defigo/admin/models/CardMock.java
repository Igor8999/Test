package lanars.com.defigo.admin.models;

import android.os.Parcel;
import android.os.Parcelable;

public class CardMock implements Parcelable {
    private String name;

    private String displayName;

    private long id;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public CardMock(String name, String displayName, long id) {
        this.name = name;
        this.displayName = displayName;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.displayName);
        dest.writeLong(this.id);
    }

    protected CardMock(Parcel in) {
        this.name = in.readString();
        this.displayName = in.readString();
        this.id = in.readLong();
    }

    public static final Creator<CardMock> CREATOR = new Creator<CardMock>() {
        @Override
        public CardMock createFromParcel(Parcel source) {
            return new CardMock(source);
        }

        @Override
        public CardMock[] newArray(int size) {
            return new CardMock[size];
        }
    };
}
