package lanars.com.defigo.admin.settings.dweller.detail;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.RfidModel;

public interface DwellerCardsView extends BaseMvpView {

    void setCards(List<RfidModel> cards);
}
