package lanars.com.defigo.admin.settings.building;

import java.util.List;

import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.common.base.BaseMvpView;

public interface AdminPanelView extends BaseMvpView {

    void onShowLogoutButton(boolean isShow);

    void setBuildings(List<BuildingModel> buildings);

    void logoutView();

}
