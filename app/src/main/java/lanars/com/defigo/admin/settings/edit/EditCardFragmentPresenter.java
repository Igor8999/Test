package lanars.com.defigo.admin.settings.edit;

import com.arellomobile.mvp.InjectViewState;
import com.crashlytics.android.Crashlytics;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.network.service.IAdminService;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class EditCardFragmentPresenter extends BasePresenter<EditCardFragmentView> {

    private long userId;

    @Inject
    Router router;

    @Inject
    IAdminService adminService;

    @Override
    public void attachView(EditCardFragmentView view) {
        super.attachView(view);
        getViewState().setCloseIcon(true);
    }

    @Override
    public void detachView(EditCardFragmentView view) {
        super.detachView(view);
        getViewState().setCloseIcon(false);
    }

    public EditCardFragmentPresenter(long userId) {
        this.userId = userId;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void addCard(int id, String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("name", name);
        params.put("rfid", String.valueOf(id));
        params.put("disabled", false);

        adminService.addCard(params)
                .subscribe(this::onSuccess, this::onError);
    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
        Crashlytics.logException(throwable);
    }

    private void onSuccess(RfidModel rfidModel) {
        onBack();
        onBack();
    }
}

