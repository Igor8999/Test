package lanars.com.defigo.admin.settings.dweller.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.settings.listener.OnItemCardDeleteListener;
import lanars.com.defigo.admin.settings.listener.OnItemCardEditListener;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;

public class DwellerCardsAdapter extends RecyclerView.Adapter<DwellerCardsAdapter.ViewHolder> {

    private List<RfidModel> cardMocks = new ArrayList<>();

    private OnItemClickRecyclerListener onItemClickRecyclerListener;
    private OnItemCardDeleteListener onItemCardDeleteListener;
    private OnItemCardEditListener onItemCardEditListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dwellers_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.cardInfo.setOnClickListener(v -> onItemCardInfoClick(viewHolder));
//        viewHolder.btnEditCard.setOnClickListener(v -> onItemEditClick(viewHolder));
        viewHolder.btnDeleteCard.setOnClickListener(v -> onItemDeleteClick(viewHolder));
        return viewHolder;
    }

    private void onItemCardInfoClick(ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onItemClickRecyclerListener.onItemClick(position);
        }
    }

    private void onItemEditClick(ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onItemCardEditListener.onEditCard(position);
        }
    }

    private void onItemDeleteClick(ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onItemCardDeleteListener.onDelete(position);
        }
    }

    public DwellerCardsAdapter(List<RfidModel> rfidModelList, OnItemClickRecyclerListener onItemClickRecyclerListener,
                               OnItemCardDeleteListener onItemCardDeleteListener,
                               OnItemCardEditListener onItemCardEditListener) {
        this.cardMocks = rfidModelList;
        this.onItemClickRecyclerListener = onItemClickRecyclerListener;
        this.onItemCardDeleteListener = onItemCardDeleteListener;
        this.onItemCardEditListener = onItemCardEditListener;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RfidModel cardMock = cardMocks.get(position);
        holder.tvDisplayName.setText(cardMock.getName());
        holder.tvCardId.setText(String.format(Locale.getDefault(), "Card id: %d", cardMock.getId()));
        holder.tvNameCard.setText(String.format("%s %s", "Card name:", cardMock.getName()));
    }

    public List<RfidModel> getCardMocks() {
        return cardMocks;
    }

    @Override
    public int getItemCount() {
        return cardMocks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvNameCard)
        TextView tvNameCard;

        @BindView(R.id.tvCardId)
        TextView tvCardId;

        @BindView(R.id.tvDisplayName)
        TextView tvDisplayName;

        @BindView(R.id.llCardInfo)
        View cardInfo;

//        @BindView(R.id.btnEditCard)
//        ImageButton btnEditCard;

        @BindView(R.id.btnDeleteCard)
        ImageButton btnDeleteCard;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
