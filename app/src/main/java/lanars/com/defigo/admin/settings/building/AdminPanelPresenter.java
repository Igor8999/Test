package lanars.com.defigo.admin.settings.building;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.service.IAdminService;
import lanars.com.defigo.common.repository.user.IDataRepository;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.common.utils.IAppPreferences;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class AdminPanelPresenter extends BasePresenter<AdminPanelView> {
    @Inject
    Router router;

    @Inject
    IDataRepository dataRepository;

    @Inject
    IUserRepository userRepository;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IAdminService adminService;

    public AdminPanelPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().onShowLogoutButton(true);
        getAllBuildings();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getViewState().onShowLogoutButton(false);
    }

    private void getAllBuildings() {
        addToDisposable(
                adminService.getBuildings()
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .subscribe(this::onSuccessGetBuildings,
                                this::onGetErrorAllBuildings));

    }

    private void onSuccessGetBuildings(List<BuildingModel> buildingModels) {
        getViewState().setBuildings(buildingModels);
    }

    private void onGetErrorAllBuildings(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void onBack() {
        router.exit();
    }

    public void goToApartment(BuildingModel buildingModel) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.BUILDING_ID, buildingModel.getId());
        bundle.putString(AppConst.BUILDING_NAME, buildingModel.getAddress());
        router.navigateTo(RouteScreenName.DWELLERS, bundle);
    }

    public void clearData() {
        addToDisposable(userRepository.clearUsers()
                .flatMap((Function<Integer, ObservableSource<?>>) integer -> userRepository.clearRooms())
                .subscribe(this::logout));
    }

    private void logout(Object o) {
        if (appPreferences.clearData()) {
            getViewState().logoutView();
        }
    }
}
