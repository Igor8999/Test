package lanars.com.defigo.admin.settings.building.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;

public class BuildingsAdapter extends RecyclerView.Adapter<BuildingsAdapter.ViewHolder> {

    private List<BuildingModel> buildingModels;
    private OnItemClickRecyclerListener onItemClickRecyclerListener;

    public List<BuildingModel> getBuildingModels() {
        return buildingModels;
    }

    public BuildingsAdapter(List<BuildingModel> buildingModels, OnItemClickRecyclerListener onItemClickRecyclerListener) {
        this.buildingModels = buildingModels;
        this.onItemClickRecyclerListener = onItemClickRecyclerListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_building, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(v -> onItemClick(viewHolder));
        return viewHolder;
    }

    private void onItemClick(RecyclerView.ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onItemClickRecyclerListener.onItemClick(position);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BuildingModel buildingModel = buildingModels.get(position);
        holder.tvName.setText(buildingModel.getAddress());
    }

    @Override
    public int getItemCount() {
        return buildingModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
