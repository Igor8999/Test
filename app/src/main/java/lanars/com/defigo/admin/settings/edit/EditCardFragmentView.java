package lanars.com.defigo.admin.settings.edit;

import lanars.com.defigo.common.base.BaseMvpView;

public interface EditCardFragmentView extends BaseMvpView {
    void setCloseIcon(boolean isClose);
}
