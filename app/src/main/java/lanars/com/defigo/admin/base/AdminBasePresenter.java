package lanars.com.defigo.admin.base;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class AdminBasePresenter extends BasePresenter<AdminBaseView> {

    @Inject
    Router router;

    public AdminBasePresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void goToAdminPanel() {
        router.newRootScreen(RouteScreenName.ADMIN_PANEL);
    }
}
