package lanars.com.defigo.admin.settings.building;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseFragment;
import lanars.com.defigo.admin.models.BuildingModel;
import lanars.com.defigo.admin.settings.building.adapter.BuildingsAdapter;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;
import lanars.com.defigo.login.LoginActivity;

public class AdminPanelFragment extends AdminBaseFragment implements AdminPanelView, OnItemClickRecyclerListener {

    @BindView(R.id.rvBuildings)
    RecyclerView rvBuildings;

    @InjectPresenter
    AdminPanelPresenter adminPanelPresenter;

    private BuildingsAdapter buildingsAdapter;

    @Override
    protected void initViews(View view) {
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.admin_building_settings, container, false);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.admin_panel);
    }

    @Override
    public void handleBackClick() {
        //nothing
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgBack.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnLogout)
    void logout() {
        adminPanelPresenter.clearData();
    }

    @Override
    public void onShowLogoutButton(boolean isShow) {
        btnLogout.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setBuildings(List<BuildingModel> buildings) {
        buildingsAdapter = new BuildingsAdapter(buildings, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvBuildings.setLayoutManager(linearLayoutManager);
        rvBuildings.setAdapter(buildingsAdapter);
        rvBuildings.setHasFixedSize(true);
        rvBuildings.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onItemClick(int position) {
        adminPanelPresenter.goToApartment(buildingsAdapter.getBuildingModels().get(position));
    }

    @Override
    public void logoutView() {
        LoginActivity.start(getContext());
        getActivity().finish();
    }
}
