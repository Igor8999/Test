package lanars.com.defigo.admin.settings.dweller;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import butterknife.BindView;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseFragment;
import lanars.com.defigo.admin.settings.dweller.adapter.DwellerAdapter;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;
import lanars.com.defigo.common.utils.AppConst;

public class DwellerSettingsFragment extends AdminBaseFragment implements DwellerSettingsView, OnItemClickRecyclerListener {

    private DwellerAdapter dwellerAdapter;

    @InjectPresenter
    DwellerSettingsPresenter presenter;

    @BindView(R.id.rvDwellers)
    RecyclerView rvDwellers;

    @ProvidePresenter
    public DwellerSettingsPresenter provide() {
        int buildingId = getArguments().getInt(AppConst.BUILDING_ID);
        String apartmentName = getArguments().getString(AppConst.BUILDING_NAME);
        return new DwellerSettingsPresenter(buildingId, apartmentName);
    }

    public static DwellerSettingsFragment newInstance(Bundle bundle) {
        DwellerSettingsFragment fragment = new DwellerSettingsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews(View view) {

    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.admin_dweller_settings, container, false);
    }

    @Override
    protected String getTitle() {
        return presenter.provideApartmentName();
    }

    @Override
    public void handleBackClick() {
        presenter.onBack();
    }

    @Override
    public void onSetUsers(List<User> users) {
        dwellerAdapter = new DwellerAdapter(users, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvDwellers.setLayoutManager(linearLayoutManager);
        rvDwellers.setAdapter(dwellerAdapter);
        rvDwellers.setHasFixedSize(true);
        rvDwellers.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onItemClick(int position) {
        presenter.goToDwellerCards(dwellerAdapter.getUsers().get(position));
    }
}
