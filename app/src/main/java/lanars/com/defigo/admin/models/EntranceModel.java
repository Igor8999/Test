package lanars.com.defigo.admin.models;

import java.util.List;

import lanars.com.defigo.common.network.models.User;

public class EntranceModel {
    public String publicName;
    public String uuid;
    public int id;
    public List<User> members;

    public EntranceModel(String publicName, String uuid, int id) {
        this.publicName = publicName;
        this.uuid = uuid;
        this.id = id;
    }

    public String getPublicName() {
        return publicName;
    }

    public String getUuid() {
        return uuid;
    }

    public int getId() {
        return id;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
