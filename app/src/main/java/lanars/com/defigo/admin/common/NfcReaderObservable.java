package lanars.com.defigo.admin.common;

import android.content.Context;
import android.content.Intent;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class NfcReaderObservable {

    private PublishSubject<Intent> bus;

    private static NfcReaderObservable INSTANCE;

    private NfcReaderObservable() {
    }

    public static NfcReaderObservable getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NfcReaderObservable();
        }
        return INSTANCE;
    }

    public NfcReaderObservable requestReadNfc(Context context) {
        bus = PublishSubject.create();
        startImagePickHiddenActivity(context);
        return getInstance();
    }

    public Observable<Intent> toObservable() {
        return bus;
    }

    public void publishNfc(Intent intent) {
        bus.onNext(intent);
        bus.onComplete();
    }

    public void publishError(String errorMessage) {
        bus.onError(new Throwable(errorMessage));
        bus.onComplete();
    }

    private void startImagePickHiddenActivity(Context context) {
        Intent intent = new Intent(context, NfcHiddenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
