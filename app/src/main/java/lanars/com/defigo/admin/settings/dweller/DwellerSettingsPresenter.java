package lanars.com.defigo.admin.settings.dweller;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.admin.models.ApartmentModel;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IAdminService;
import lanars.com.defigo.common.utils.AppConst;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class DwellerSettingsPresenter extends BasePresenter<DwellerSettingsView> {

    @Inject
    Router router;

    @Inject
    IAdminService adminService;

    private int buildingId;
    private String apartmentName;

    public DwellerSettingsPresenter(int buildingId, String apartmentName) {
        DefigoApplication.getInstance().getAppComponent().inject(this);
        this.buildingId = buildingId;
        this.apartmentName = apartmentName;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getAllDwellers();
    }

    private void getAllDwellers() {
        addToDisposable(adminService.getApartments(this.buildingId)
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doAfterTerminate(() -> getViewState().onHideProgress())
                .subscribe(this::onSuccessGetUsers, this::onErrorGetUsers));
    }

    private void onErrorGetUsers(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessGetUsers(List<ApartmentModel> apartmentModels) {
        List<User> users = new ArrayList<>();
        for (ApartmentModel apartmentModel : apartmentModels) {
            users.addAll(apartmentModel.getMembers());
        }

        getViewState().onSetUsers(users);
    }

    public void onBack() {
        router.exit();
    }

    public void goToDwellerCards(User user) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.DWELLER_USER, user);
        router.navigateTo(RouteScreenName.DWELLER_CARDS, bundle);
    }

    public String provideApartmentName() {
        return apartmentName;
    }
}

