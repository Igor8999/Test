package lanars.com.defigo.admin.settings.reader;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;

public class ReadNfcLoaderDialog extends DialogFragment {

    @BindView(R.id.llScan)
    RelativeLayout llScan;

    @BindView(R.id.scanView)
    View scanView;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.read_nfc_read_dialog, null);
        ButterKnife.bind(this, view);
        scanView.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.move_scan));
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        return builder.create();
    }

}