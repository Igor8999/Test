package lanars.com.defigo.admin.settings.dweller.detail;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseFragment;
import lanars.com.defigo.admin.settings.dweller.detail.adapter.DwellerCardsAdapter;
import lanars.com.defigo.admin.settings.listener.OnItemCardDeleteListener;
import lanars.com.defigo.admin.settings.listener.OnItemCardEditListener;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;
import lanars.com.defigo.common.utils.AppConst;

public class DwellerCardsFragment extends AdminBaseFragment implements DwellerCardsView, OnItemClickRecyclerListener, OnItemCardDeleteListener, OnItemCardEditListener {

    private User selectedUser;

    @InjectPresenter
    DwellerCardsPresenter dwellerCardsPresenter;

    @BindView(R.id.rvCards)
    RecyclerView rvCards;

    private DwellerCardsAdapter dwellerCardsAdapter;

    @ProvidePresenter
    public DwellerCardsPresenter createDwellerCardsPresenter() {
        selectedUser = (User) getArguments().getSerializable(AppConst.DWELLER_USER);
        return new DwellerCardsPresenter(selectedUser);
    }

    public static DwellerCardsFragment newInstance(Bundle bundle) {
        DwellerCardsFragment fragment = new DwellerCardsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews(View view) {

    }

    @OnClick(R.id.tvAddCard)
    void addCard() {
        dwellerCardsPresenter.addCard();
    }

    @Override
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.admin_dweller_cards_fragment, container, false);
    }

    @Override
    protected String getTitle() {
        return selectedUser != null ? String.format("%s %s", selectedUser.getFirstName(), selectedUser.getLastName()) : null;
    }

    @Override
    public void handleBackClick() {
        dwellerCardsPresenter.onBack();
    }

    @Override
    public void setCards(List<RfidModel> cards) {
        dwellerCardsAdapter = new DwellerCardsAdapter(cards, this, this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvCards.setLayoutManager(linearLayoutManager);
        rvCards.setAdapter(dwellerCardsAdapter);
        rvCards.setHasFixedSize(true);
        rvCards.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

    }

    @Override
    public void onItemClick(int position) {
        dwellerCardsPresenter.goToReadCard(dwellerCardsAdapter.getCardMocks().get(position));
    }

    @Override
    public void onDelete(int position) {
        dwellerCardsPresenter.deleteCard(dwellerCardsAdapter.getCardMocks().get(position));
    }

    @Override
    public void onEditCard(int position) {
        dwellerCardsPresenter.goToEditCards(dwellerCardsAdapter.getCardMocks().get(position));
    }

    @Override
    public void onShowError(String errorMessage) {
        Alert.create(getActivity())
                .setText(errorMessage)
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }
}
