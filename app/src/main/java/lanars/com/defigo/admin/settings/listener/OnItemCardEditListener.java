package lanars.com.defigo.admin.settings.listener;

public interface OnItemCardEditListener {
    void onEditCard(int position);
}
