package lanars.com.defigo.admin.settings.dweller;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.User;

public interface DwellerSettingsView extends BaseMvpView {

    void onSetUsers(List<User> users);
}
