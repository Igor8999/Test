package lanars.com.defigo.admin.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuildingModel implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("enableRfid")
    @Expose
    private Boolean enableRfid;
    @SerializedName("enablePin")
    @Expose
    private Boolean enablePin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getEnableRfid() {
        return enableRfid;
    }

    public void setEnableRfid(Boolean enableRfid) {
        this.enableRfid = enableRfid;
    }

    public Boolean getEnablePin() {
        return enablePin;
    }

    public void setEnablePin(Boolean enablePin) {
        this.enablePin = enablePin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.address);
        dest.writeValue(this.enableRfid);
        dest.writeValue(this.enablePin);
    }

    public BuildingModel() {
    }

    protected BuildingModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.address = in.readString();
        this.enableRfid = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.enablePin = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<BuildingModel> CREATOR = new Parcelable.Creator<BuildingModel>() {
        @Override
        public BuildingModel createFromParcel(Parcel source) {
            return new BuildingModel(source);
        }

        @Override
        public BuildingModel[] newArray(int size) {
            return new BuildingModel[size];
        }
    };
}

