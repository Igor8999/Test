package lanars.com.defigo.admin.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseFragment;

public abstract class AdminBaseFragment extends BaseFragment {

    private FrameLayout frameContainer;

    @BindView(R.id.tvTitleAdmin)
    TextView tvTitleAdmin;

    @BindView(R.id.imgBack)
    protected ImageView imgBack;

    @BindView(R.id.btnLogout)
    protected ImageView btnLogout;

    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_admin_fragment, container, false);
        frameContainer = view.findViewById(R.id.fragAdminCont);
        setHasOptionsMenu(true);
        View childView = createView(inflater, container);
        addViewToContainer(childView);
        ButterKnife.bind(this, view);
        if (getTitle() != null) {
            tvTitleAdmin.setText(getTitle());
        }
        initViews(view);
        return view;
    }

    @OnClick(R.id.imgBack)
    public void onBackClick(View view) {
        handleBackClick();
    }

    protected abstract void initViews(View view);

    protected abstract View createView(LayoutInflater inflater, ViewGroup container);

    protected abstract String getTitle();

    public abstract void handleBackClick();

    private void addViewToContainer(View childView) {
        frameContainer.addView(childView);
    }
}
