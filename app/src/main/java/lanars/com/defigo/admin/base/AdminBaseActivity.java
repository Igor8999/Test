package lanars.com.defigo.admin.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.settings.building.AdminPanelFragment;
import lanars.com.defigo.admin.settings.dweller.DwellerSettingsFragment;
import lanars.com.defigo.admin.settings.dweller.detail.DwellerCardsFragment;
import lanars.com.defigo.admin.settings.edit.EditCardFragment;
import lanars.com.defigo.admin.settings.reader.ReadCardFragment;
import lanars.com.defigo.common.navigation.RouteScreenName;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;

public class AdminBaseActivity extends MvpAppCompatActivity implements AdminBaseView {

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter
    AdminBasePresenter adminBasePresenter;

    public static void start(Context context) {
        Intent starter = new Intent(context, AdminBaseActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DefigoApplication.getInstance().getAppComponent().inject(this);
        setContentView(R.layout.admin_activity);
        ButterKnife.bind(this);
        if (getSupportFragmentManager().findFragmentById(R.id.adminSettingsCont) == null) {
            adminBasePresenter.goToAdminPanel();
        }
    }

    private Navigator navigator = new SupportAppNavigator(this, R.id.adminSettingsCont) {
        @Override
        protected Intent createActivityIntent(Context context, String screenKey, Object data) {
            return null;
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case RouteScreenName.ADMIN_PANEL:
                    return new AdminPanelFragment();

                case RouteScreenName.DWELLERS:
                    return DwellerSettingsFragment.newInstance((Bundle) data);

                case RouteScreenName.DWELLER_CARDS:
                    return DwellerCardsFragment.newInstance((Bundle) data);

                case RouteScreenName.READ_CARDS:
                    return ReadCardFragment.newInstance((Bundle) data);

                case RouteScreenName.EDIT_CARDS:
                    return EditCardFragment.newInstance((Bundle) data);
            }
            return null;
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    public void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onShowError(String errorMessage) {

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

}
