package lanars.com.defigo.call;

import lanars.com.defigo.common.base.BaseMvpView;

public interface CallPreviewActivityView extends BaseMvpView {
    void openCallScreen();

    void closeScreen();

    void showCallerName(String name);

    void requestAudioPermission();

    void showPermissionError();
}
