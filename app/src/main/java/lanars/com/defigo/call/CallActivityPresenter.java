package lanars.com.defigo.call;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RtpReceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

import java.util.Arrays;
import java.util.LinkedList;

import javax.inject.Inject;

import io.socket.client.Socket;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.network.service.IDoorbellService;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.push.TrackerMessagingService;

import static lanars.com.defigo.SocketManager.CALL_CANDIDATE;
import static lanars.com.defigo.SocketManager.CALL_OFFER;
import static lanars.com.defigo.SocketManager.CALL_PING;
import static lanars.com.defigo.SocketManager.CALL_PONG;
import static lanars.com.defigo.SocketManager.CALL_STOPPED;
import static lanars.com.defigo.push.TrackerMessagingService.KEY_CALL_UUID;
import static lanars.com.defigo.push.TrackerMessagingService.KEY_DOORBELL_ID;
import static lanars.com.defigo.push.TrackerMessagingService.KEY_FROM_PUSH;

@InjectViewState
public class CallActivityPresenter extends MvpPresenter<CallActivityView> {
    private MediaConstraints mediaConstraints = new MediaConstraints();
    private PeerConnection peerConnection;

    private EglBase rootEglBase = EglBase.create();

    @Inject
    Context context;

    @Inject
    SocketManager socketManager;

    @Inject
    IDoorbellService doorbellService;

    @Inject
    IAppPreferences appPreferences;

    private Bundle bundle;

    CallActivityPresenter(Bundle extras) {
        this.bundle = extras;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    private void setCallInfoIfNotExist(Bundle callBundle) {
        String callUuid = socketManager.getCallUuid();

        if (TextUtils.isEmpty(callUuid)) {
            socketManager.setCallUuid(callBundle.getString(KEY_CALL_UUID, ""));

            if (TextUtils.isEmpty(socketManager.getCallUuid())) {
                Crashlytics.logException(new Throwable("ERROR! There is no call uuid at Call activity"));
            }
        }
    }

    private void checkCallType(Bundle bundle) {
        boolean isVideoEnabled = bundle.getBoolean(TrackerMessagingService.KEY_VIDEO_ENABLED);
        String doorbellName = bundle.getString(TrackerMessagingService.KEY_DOORBELL);

        if (!isVideoEnabled) {
            getViewState().showAudioCallPlaceholder();
            getViewState().showDoorbellName(doorbellName);
        }
    }

    private void socketStuff() {
        if (socketManager.getSocket() == null || !socketManager.getSocket().connected()) {
            socketManager.connectSocket(appPreferences.getAccessToken(), appPreferences.getCurrentUserId(), this::onSocketConnected);
            return;
        }

        onSocketConnected();
    }

    private void onSocketConnected(Object... args) {
        socketManager.getSocket()
                .on(CALL_OFFER, this::handleOfferSocketEvent)
                .on(CALL_CANDIDATE, this::handleIceCandidateSocketEvent)
                .on(CALL_STOPPED, this::handleCallStoppedEvent)
                .on(CALL_PING, this::handleServerPingEvent);

        socketManager.sendCallResponse();
    }

    private void handleOfferSocketEvent(Object... args) {
        Logger.d("OFFER " + Arrays.toString(args));
        try {
            JSONObject jsonObject = new JSONObject(args[0].toString());

            String sdpDescription = PeerConnectionClient.preferCodec(jsonObject.get("sdp").toString(), PeerConnectionClient.VIDEO_CODEC_H264, false);

            peerConnection.setRemoteDescription(sdpRemoteObserver, new SessionDescription(
                    SessionDescription.Type.OFFER,
                    sdpDescription)
            );

            peerConnection.createAnswer(sdpObserver, mediaConstraints);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleIceCandidateSocketEvent(Object... args) {
        Logger.d("socket event " + Arrays.toString(args));
        try {
            JSONObject jsonObject = new JSONObject(args[0].toString());

            String sdpMid = jsonObject.getString("sdpMid");
            int sdpMLineIndex = jsonObject.getInt("sdpMLineIndex");
            String candidate = jsonObject.getString("candidate");
            peerConnection.addIceCandidate(new IceCandidate(sdpMid, sdpMLineIndex, candidate));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleCallStoppedEvent(Object... objects) {
        getViewState().closeScreen();
    }

    private void handleServerPingEvent(Object... args) {
        Logger.d("socket event " + Arrays.toString(args));
        try {
            JSONObject jsonObject = new JSONObject(args[0].toString());

            socketManager.getSocket().emit(CALL_PONG, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void peerConnectionStuff() {
        PeerConnectionFactory.InitializationOptions.Builder builder = PeerConnectionFactory.InitializationOptions.builder(this.context);
        builder.setEnableInternalTracer(false);
        builder.setEnableVideoHwAcceleration(true);

        PeerConnectionFactory.InitializationOptions options = builder.createInitializationOptions();
        PeerConnectionFactory.initialize(options);

        PeerConnectionFactory.Options peerConnectionOptions = new PeerConnectionFactory.Options();
        PeerConnectionFactory peerConnectionFactory = new PeerConnectionFactory(peerConnectionOptions);
        peerConnectionFactory.setVideoHwAccelerationOptions(rootEglBase.getEglBaseContext(), rootEglBase.getEglBaseContext());

        LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<>();
        iceServers.add(new PeerConnection.IceServer("turn:api.dingdongme.com:3478", "snowsoft", "taie997"));
        iceServers.add(new PeerConnection.IceServer("stun:api.dingdongme.com:19302", "snowsoft", "taie997"));

        peerConnection = peerConnectionFactory.createPeerConnection(iceServers, mediaConstraints, peerConnectionObserver);

        MediaStream localMediaStream = peerConnectionFactory.createLocalMediaStream("com.lanars.DingDong.stream");

        AudioTrack localAudioTrack = peerConnectionFactory.createAudioTrack("com.lanars.DingDong.audio", peerConnectionFactory.createAudioSource(mediaConstraints));
        localAudioTrack.setEnabled(true);

        localMediaStream.addTrack(localAudioTrack);

        peerConnection.addStream(localMediaStream);
    }

    private PeerConnection.Observer peerConnectionObserver = new PeerConnection.Observer() {
        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {
            Logger.d("onSignalingChange");
        }

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
            Logger.d("onIceConnectionChange");
        }

        @Override
        public void onIceConnectionReceivingChange(boolean b) {
            Logger.d("onIceConnectionReceivingChange");
        }

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
            Logger.d("onIceGatheringChange");
        }

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {
            socketManager.sendIceCandidate(iceCandidate);
        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
            Logger.d("onIceCandidatesRemoved");
        }

        @Override
        public void onAddStream(MediaStream mediaStream) {
            //TODO check onAddTrack method call
            if (mediaStream.videoTracks.size() <= 0) {
                return;
            }

            getViewState().onAddStream(mediaStream);
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
            Logger.d("onRemoveStream");
        }

        @Override
        public void onDataChannel(DataChannel dataChannel) {
            Logger.d("onDataChannel");
        }

        @Override
        public void onRenegotiationNeeded() {
            Logger.d("onRenegotiationNeeded");
        }

        @Override
        public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
            //TODO check onAddStream method call
            Logger.d(" peer connection onAddTrack " + mediaStreams.length);
            getViewState().onAddTrack(rtpReceiver, mediaStreams);
        }
    };

    private SdpObserver sdpObserver = new SdpObserver() {
        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {
            String sdpDescription = PeerConnectionClient.preferCodec(sessionDescription.description, PeerConnectionClient.VIDEO_CODEC_H264, false);

            peerConnection.setLocalDescription(sdpLocalObserver, new SessionDescription(SessionDescription.Type.ANSWER, sdpDescription));
            Logger.d("CREATE ANSWER " + sessionDescription.description);
            socketManager.sendAnswer(sessionDescription);
        }

        @Override
        public void onSetSuccess() {
            Logger.d("onAddTrack");
        }

        @Override
        public void onCreateFailure(String s) {
            Logger.d("onAddTrack " + s);
        }

        @Override
        public void onSetFailure(String s) {
            Logger.d("onAddTrack " + s);
        }
    };

    private SdpObserver sdpLocalObserver = new SdpObserver() {
        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {
            Logger.d("sdpLocalObserver onCreateSuccess " + sessionDescription.type);
        }

        @Override
        public void onSetSuccess() {
            Logger.d("sdpLocalObserver onSetSuccess");
        }

        @Override
        public void onCreateFailure(String s) {
            Logger.d("sdpLocalObserver onCreateFailure " + s);
        }

        @Override
        public void onSetFailure(String s) {
            Logger.d("sdpLocalObserver onSetFailure " + s);
        }
    };

    private SdpObserver sdpRemoteObserver = new SdpObserver() {
        @Override
        public void onCreateSuccess(SessionDescription sessionDescription) {
            Logger.d("sdpRemoteObserver onCreateSuccess " + sessionDescription.toString());
        }

        @Override
        public void onSetSuccess() {
            Logger.d("sdpRemoteObserver onSetSuccess");
        }

        @Override
        public void onCreateFailure(String s) {
            Logger.d("sdpRemoteObserver onCreateFailure " + s);
        }

        @Override
        public void onSetFailure(String s) {
            Logger.d("sdpRemoteObserver onSetFailure " + s);
        }
    };

    public void handleOnDeclineClick() {
        String callUuid = socketManager.getCallUuid();

        if (TextUtils.isEmpty(callUuid)) {
            callUuid = bundle.getString(KEY_CALL_UUID);
        }

        socketManager.brakeCall(socketManager.getUserId(), callUuid, this::handleExitScreen);
    }

    private void handleExitScreen(Object... objects) {
        boolean isFromPush = bundle.getBoolean(KEY_FROM_PUSH, false);

        if (isFromPush) {
            socketManager.disconnect();
        }

        getViewState().closeScreen();
    }

    public EglBase.Context provideEglContext() {
        return rootEglBase.getEglBaseContext();
    }

    public void preparePeerConnection() {
        setCallInfoIfNotExist(this.bundle);
        checkCallType(this.bundle);

        peerConnectionStuff();
        socketStuff();
    }

    public void destroyCall() {
        peerConnection.dispose();
        peerConnection = null;
        Socket socket = socketManager.getSocket();
        if (socket == null) {
            return;
        }

        socket.off(CALL_OFFER)
                .off(CALL_CANDIDATE)
                .off(CALL_STOPPED)
                .off(CALL_PING);
    }

    public void openDoor() {
        String doorbellId = bundle.getString(KEY_DOORBELL_ID);

        if (TextUtils.isEmpty(doorbellId)) {
            return;
        }

        doorbellService
                .openDoorbell(Integer.parseInt(doorbellId))
                .subscribe(responseBody -> {

                }, Crashlytics::logException);
    }
}
