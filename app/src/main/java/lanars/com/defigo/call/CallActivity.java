package lanars.com.defigo.call;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.webrtc.MediaStream;
import org.webrtc.RendererCommon;
import org.webrtc.RtpReceiver;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoRenderer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;

public class CallActivity extends MvpAppCompatActivity implements CallActivityView {
    @InjectPresenter
    CallActivityPresenter presenter;

    @BindView(R.id.surfaceView)
    SurfaceViewRenderer surfaceViewRenderer;

    @BindView(R.id.rlAudioCallPlaceholder)
    ViewGroup rlAudioCallPlaceholder;

    @BindView(R.id.tvDoorbellName)
    TextView tvDoorbellName;

    private AudioManager audioManager;

    @ProvidePresenter
    public CallActivityPresenter providePresenter() {
        return new CallActivityPresenter(getIntent().getExtras());
    }

    public static Intent constructIntent(Context context, Bundle bundle) {
        Intent starter = new Intent(context, CallActivity.class);
        starter.putExtras(bundle);
        return starter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ButterKnife.bind(this);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        audioManager.setMicrophoneMute(false);
        audioManager.setSpeakerphoneOn(true);
        presenter.preparePeerConnection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Window wind = this.getWindow();
        wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        wakeUpScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroyCall();
        audioManager.setSpeakerphoneOn(false);
    }

    private void wakeUpScreen() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire(90 * 1000);
    }

    @OnClick(R.id.btnDecline)
    public void declineCall(View view) {
        presenter.handleOnDeclineClick();
    }

    @OnClick(R.id.btnOpenTheDoor)
    public void openTheDoor(View view) {
        presenter.openDoor();
    }

    @Override
    public void onAddStream(MediaStream mediaStream) {
        runOnUiThread(() -> {
            surfaceViewRenderer.init(presenter.provideEglContext(), new RendererCommon.RendererEvents() {
                @Override
                public void onFirstFrameRendered() {

                }

                @Override
                public void onFrameResolutionChanged(int i, int i1, int i2) {

                }
            });
            VideoRenderer videoRenderer = new VideoRenderer(surfaceViewRenderer);

            if (mediaStream.videoTracks.size() > 0) {
                mediaStream.videoTracks.get(0).addRenderer(videoRenderer);
            }
        });
    }

    @Override
    public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
        runOnUiThread(() -> {
            VideoRenderer videoRenderer = new VideoRenderer(surfaceViewRenderer);
            if (mediaStreams.length > 0) {
                if (mediaStreams[0].videoTracks.size() > 0) {
                    mediaStreams[0].videoTracks.get(0).addRenderer(videoRenderer);
                }
            }
        });
    }

    @Override
    public void closeScreen() {
        finish();
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    @Override
    public void showAudioCallPlaceholder() {
        surfaceViewRenderer.setVisibility(View.GONE);
        rlAudioCallPlaceholder.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDoorbellName(String name) {
        tvDoorbellName.setText(name);
    }
}
