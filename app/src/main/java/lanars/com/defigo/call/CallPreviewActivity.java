package lanars.com.defigo.call;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.crashlytics.android.Crashlytics;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.utils.Utils;
import lanars.com.defigo.push.DismissNotificationService;
import lanars.com.defigo.push.TrackerMessagingService;

import static android.media.AudioAttributes.USAGE_MEDIA;

public class CallPreviewActivity extends MvpAppCompatActivity implements CallPreviewActivityView {

    private MediaPlayer mediaPlayer;
    // TODO fix me
    public static boolean isRunning;
    private Vibrator vibrator;

    public static Intent constructIntent(Context context, Bundle bundle) {
        Intent starter = new Intent(context, CallPreviewActivity.class);
        if (bundle == null) {
            bundle = new Bundle();
        }
        starter.putExtras(bundle);
        return starter;
    }

    @InjectPresenter
    CallPreviewActivityPresenter presenter;

    @ProvidePresenter
    public CallPreviewActivityPresenter providePresenter() {
        return new CallPreviewActivityPresenter(getIntent().getExtras());
    }

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_preview);
        ButterKnife.bind(this);

        Intent dismissNotificationServiceIntent = DismissNotificationService.constructIntent(
                this, TrackerMessagingService.CALL_NOTIFICATION_ID, null);
        startService(dismissNotificationServiceIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isRunning = true;
        setupVibrator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Window wind = this.getWindow();
        wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        wakeUpScreen();
        initMediaPlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopMediaPlayerPlay();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isRunning = false;
        cancelVibrator();
    }

    private void setupVibrator() {
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] vibrationPattern = {0, 200, 500};
        if (vibrator != null) {
            vibrator.vibrate(vibrationPattern, 0);
        }
    }

    private void cancelVibrator() {
        if (vibrator != null) {
            vibrator.cancel();
        }
    }

    @OnClick(R.id.btnDecline)
    public void onDeclineClick(View view) {
        presenter.handleOnDeclineClick();
    }

    @OnClick(R.id.btnAnswer)
    public void onAnswerClick(View view) {
        presenter.handleOnAnswerClick();
    }

    @Override
    public void openCallScreen() {
        Intent intent = CallActivity.constructIntent(this, getIntent().getExtras());
        startActivity(intent);
        finish();
    }

    @Override
    public void closeScreen() {
        finish();
    }

    private void wakeUpScreen() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire(90 * 1000);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    @Override
    public void showCallerName(String name) {
        tvTitle.setText(name);
    }

    @Override
    public void requestAudioPermission() {
        RxPermissions rxPermissions = new RxPermissions(CallPreviewActivity.this);
        rxPermissions
                .request(Manifest.permission.RECORD_AUDIO)
                .subscribe(CallPreviewActivity.this::onSuccessPermission, CallPreviewActivity.this::onErrorPermission);
    }

    private void onErrorPermission(Throwable throwable) {
        presenter.onErrorPermission(throwable);
    }

    private void onSuccessPermission(Boolean isGranted) {
        presenter.onPermissionRequestSuccess(isGranted);
    }

    @Override
    public void onShowError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPermissionError() {
        Utils.showPermissionRationaleAlert(this, R.string.audio_permission_rationale_text);
    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

    //TODO move logic to presenter
    private void initMediaPlayer() {
        String savedSoundFilePath = presenter.getSavedSoundFilePath();
        Uri soundUri;

        if (TextUtils.isEmpty(savedSoundFilePath)) {
            soundUri = Uri.parse("android.resource://" + getPackageName() + "/raw/dingdong");
        } else {
            File file = new File(savedSoundFilePath);
            soundUri = Uri.fromFile(file);
        }

        try {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(USAGE_MEDIA)
                    .build();

            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioAttributes(audioAttributes);
            mediaPlayer.setDataSource(this, soundUri);
            mediaPlayer.setLooping(true);
            mediaPlayer.setOnPreparedListener(MediaPlayer::start);
            mediaPlayer.prepareAsync();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Crashlytics.logException(throwable);
        }
    }

    public void stopMediaPlayerPlay() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Crashlytics.logException(throwable);
        }
    }
}
