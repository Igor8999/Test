package lanars.com.defigo.call;

import com.arellomobile.mvp.MvpView;

import org.webrtc.MediaStream;
import org.webrtc.RtpReceiver;

public interface CallActivityView extends MvpView {
    void onAddStream(MediaStream mediaStream);

    void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams);

    void closeScreen();

    void showAudioCallPlaceholder();

    void showDoorbellName(String name);
}
