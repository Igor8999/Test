package lanars.com.defigo.call;

import android.os.Bundle;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.utils.IAppPreferences;

import static lanars.com.defigo.push.TrackerMessagingService.KEY_ADDRESS;
import static lanars.com.defigo.push.TrackerMessagingService.KEY_CALL_UUID;
import static lanars.com.defigo.push.TrackerMessagingService.KEY_DOORBELL;
import static lanars.com.defigo.push.TrackerMessagingService.KEY_FROM_PUSH;

@InjectViewState
public class CallPreviewActivityPresenter extends MvpPresenter<CallPreviewActivityView> {

    @Inject
    SocketManager socketManager;

    @Inject
    IAppPreferences appPreferences;

    private Bundle bundle;

    public CallPreviewActivityPresenter(Bundle bundle) {
        this.bundle = bundle;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if (socketManager.getSocket() == null || !socketManager.getSocket().connected()) {
            socketManager.connectSocket(appPreferences.getAccessToken(), appPreferences.getCurrentUserId(), args -> onSocketConnected());
            return;
        }

        onSocketConnected();
    }

    private void onSocketConnected() {
        socketManager.getSocket()
                .on(SocketManager.CALL_STOPPED, this::handleCallStoppedEvent)
                .on(SocketManager.CALL_STATUS, this::handleCallStatus);

        socketManager.checkCallStatus(bundle.getString(KEY_CALL_UUID));
    }

    @Override
    public void attachView(CallPreviewActivityView view) {
        super.attachView(view);
        String callerName = String.format("%s %s", bundle.getString(KEY_DOORBELL), bundle.getString(KEY_ADDRESS));
        getViewState().showCallerName(callerName);
    }

    private void handleCallStoppedEvent(Object... objects) {
        Logger.d("CALL STOPPED");
        getViewState().closeScreen();
    }

    private void handleCallStatus(Object... args) {
        Logger.d("PREVIEW CALL STATUS" + Arrays.toString(args));
        try {
            JSONObject jsonObject = new JSONObject(args[0].toString());

            String isActiveString = jsonObject.getString("isActive");

            boolean isActive = Boolean.parseBoolean(isActiveString);

            if (!isActive) {
                Logger.e("Call is inactive");
                handleExitScreen();
                Crashlytics.logException(new Throwable("Call is inactive"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void handleOnDeclineClick() {
        String callUuid = socketManager.getCallUuid();

        if (TextUtils.isEmpty(callUuid)) {
            callUuid = bundle.getString(KEY_CALL_UUID);
        }

        socketManager.brakeCall(socketManager.getUserId(), callUuid, this::handleExitScreen);
    }

    private void handleExitScreen(Object... objects) {
        boolean isFromPush = bundle.getBoolean(KEY_FROM_PUSH, false);

        if (isFromPush) {
            socketManager.disconnect();
        }

        getViewState().closeScreen();
    }

    public void handleOnAnswerClick() {
        getViewState().requestAudioPermission();
    }

    public void onPermissionRequestSuccess(Boolean isGranted) {
        if (isGranted) {
            getViewState().openCallScreen();
        } else {
            getViewState().showPermissionError();
        }
    }

    public void onErrorPermission(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public String getSavedSoundFilePath() {
        return appPreferences.getCurrentSoundFilePath();
    }
}
