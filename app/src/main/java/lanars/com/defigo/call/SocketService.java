package lanars.com.defigo.call;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import javax.inject.Inject;

import io.socket.client.Ack;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.utils.IAppPreferences;

public class SocketService extends IntentService {
    public static final String SOCKET_EVENT_KEY = "SOCKET_EVENT_KEY";
    public static final String CALL_UUID_KEY = "CALL_UUID_KEY";
    public static final int SEND_BRAKE_CALL_EVENT = 1;

    @Inject
    SocketManager socketManager;

    @Inject
    IAppPreferences appPreferences;

    public SocketService() {
        super(SocketService.class.getName());
    }

    public static void start(Context context, @Nullable Bundle bundle) {
        Intent starter = new Intent(context, SocketService.class);
        if (bundle != null) {
            starter.putExtras(bundle);
        }
        context.startService(starter);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        connectToSocket(args -> handleIntentExtrasOptions(intent));
    }

    private void connectToSocket(Ack ack) {
        String token = appPreferences.getAccessToken();
        if (!TextUtils.isEmpty(token)) {
            if (socketManager.getSocket() != null && socketManager.getSocket().connected()) {
                ack.call();
            } else {
                socketManager.connectSocket(token, (int) appPreferences.getCurrentUserId(), ack);
            }
        }
    }

    private void handleIntentExtrasOptions(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        Bundle bundle = intent.getExtras();

        if (bundle == null) {
            return;
        }

        int eventType = bundle.getInt(SOCKET_EVENT_KEY, -1);
        String callUuid = bundle.getString(CALL_UUID_KEY, "Call uuid is NULL!");

        switch (eventType) {
            case SEND_BRAKE_CALL_EVENT:
                socketManager.brakeCall(socketManager.getUserId(), callUuid, args -> {
                    socketManager.disconnect();
                });
                break;
        }
    }
}
