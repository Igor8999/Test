package lanars.com.defigo.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;

public class SettingsMenuFragment extends MvpAppCompatFragment implements SettingsContainerView {

    @InjectPresenter
    SettingsContainerPresenter mainPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_menu_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.smrDoorbellSettings)
    public void onDoorbellSettingsPress(View view) {
        mainPresenter.goToDoorbellSettings();
    }

    @OnClick(R.id.smrProfileSettings)
    public void onProfileSettingsPress(View view) {
        mainPresenter.goToProfileSettings();
    }

    @OnClick(R.id.smrSoundSettings)
    public void onSoundSettingsPress(View view) {
        mainPresenter.goToSoundsSettings();
    }

    @OnClick(R.id.smrNotificationsSettings)
    public void onNotificationsSettingsPress(View view) {
        mainPresenter.goToNotificationsSettings();
    }

    @OnClick(R.id.smrCallSettings)
    public void onCallSettingsPress(View view) {
        mainPresenter.goToCallSettings();
    }
}
