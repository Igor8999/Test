package lanars.com.defigo.settings.rfid.edit;

import com.arellomobile.mvp.InjectViewState;
import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.network.service.IRfidService;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class EditUserRfidCardFragmentPresenter extends BasePresenter<EditUserRfidCardView> {

    private int userId;

    @Inject
    Router router;

    @Inject
    IRfidService rfidService;

    @Override
    public void attachView(EditUserRfidCardView view) {
        super.attachView(view);
        getViewState().setCloseIcon(true);
    }

    @Override
    public void detachView(EditUserRfidCardView view) {
        super.detachView(view);
        getViewState().setCloseIcon(false);
    }

    public EditUserRfidCardFragmentPresenter(int userId) {
        this.userId = userId;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void updateCard(RfidModel rfidModel) {
        rfidService.updateRfid(rfidModel.getId(), rfidModel.getName(), rfidModel.getPin(), !rfidModel.isDisabled())
                .subscribe(this::onSuccess, this::onError);

    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
        Crashlytics.logException(throwable);
    }

    private void onSuccess(RfidModel rfidModel) {
        onBack();
    }
}

