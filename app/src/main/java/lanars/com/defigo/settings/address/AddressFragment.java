package lanars.com.defigo.settings.address;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.ui.alert.Alert;

public class AddressFragment extends BaseSettingsFragment implements AddressView {

    @BindView(R.id.etStreet)
    EditText etStreet;

    @BindView(R.id.etZip)
    EditText etZip;

    @BindView(R.id.etCountry)
    EditText etCountry;

    @BindView(R.id.etCity)
    EditText etCity;

    @InjectPresenter
    AddressPresenter addressPresenter;

    @Override
    protected String getTitle() {
        return getString(R.string.address);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.address_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.btnSubmit)
    void submit() {
        addressPresenter.submitAddress(
                etStreet.getText().toString(),
                etZip.getText().toString(),
                etCountry.getText().toString(),
                etCity.getText().toString()
        );
    }

    @Override
    public void handleBackClick() {
        addressPresenter.onBack();
    }

    @Override
    public void showStreet(String street) {
        if (!TextUtils.isEmpty(street)) {
            etStreet.setText(street);
        }
    }

    @Override
    public void showZip(String zip) {
        if (!TextUtils.isEmpty(zip)) {
            etZip.setText(zip);
        }
    }

    @Override
    public void showCountry(String country) {
        if (!TextUtils.isEmpty(country)) {
            etCountry.setText(country);
        }
    }

    @Override
    public void showCity(String city) {
        if (!TextUtils.isEmpty(city)) {
            etCity.setText(city);
        }
    }

    @Override
    public void onShowSuccessUserUpdate() {
        Alert.create(getActivity())
                .setDuration(2000)
                .setText(R.string.changes_saved)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }
}
