package lanars.com.defigo.settings.callingsequence.membersettings;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.repository.user.IUserRepository;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class CallingSequenceSettingsPresenter extends BasePresenter<CallingSequenceSettingView> {

    @Inject
    Router router;

    @Inject
    IUserRepository userRepository;

    private int typeCallingInstance;

    public CallingSequenceSettingsPresenter(int typeCallingInstance) {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void getSequenceUsers(int typeCallingInstance) {

        //todo stub!
        userRepository.getSequenceUser(typeCallingInstance)
                .subscribe(sequenceUsers -> getViewState().setCallingSequenceMembers(sequenceUsers));
    }
}

