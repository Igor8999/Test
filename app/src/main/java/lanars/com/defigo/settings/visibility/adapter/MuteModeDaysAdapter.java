package lanars.com.defigo.settings.visibility.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.DayOfWeek;

public class MuteModeDaysAdapter extends RecyclerView.Adapter<MuteModeDaysAdapter.ViewHolder> {

    private List<DayOfWeek> dayOfWeeks;

    public MuteModeDaysAdapter(List<DayOfWeek> dayOfWeeks) {
        this.dayOfWeeks = dayOfWeeks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mute_days, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(v -> {
            int position = viewHolder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                DayOfWeek selectedDay = dayOfWeeks.get(position);
                dayOfWeeks.get(position).setSelected(!selectedDay.isSelected());
                notifyItemChanged(position);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DayOfWeek dayOfWeek = dayOfWeeks.get(position);
        holder.tvName.setText(dayOfWeek.getName());
        holder.imgStatusActivate.setVisibility(dayOfWeek.isSelected() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return dayOfWeeks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imSelect)
        ImageView imgStatusActivate;

        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public List<DayOfWeek> getDayOfWeeks() {
        return dayOfWeeks;
    }
}
