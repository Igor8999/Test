package lanars.com.defigo.settings.visibility.adapter;

import android.view.View;

public class VisibilityOptionsDisableViewHolder extends VisibilityOptionsBaseViewHolder {

    public VisibilityOptionsDisableViewHolder(View itemView) {
        super(itemView);
    }
}
