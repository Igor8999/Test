package lanars.com.defigo.settings.visibility;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;

public interface VisibilityOptionsView extends BaseMvpView {

    void setVisibilityDoors(List<VisibilityDoorModel> visibilityDoors);
}
