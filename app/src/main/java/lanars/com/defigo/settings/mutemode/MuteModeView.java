package lanars.com.defigo.settings.mutemode;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.DayOfWeek;

public interface MuteModeView extends BaseMvpView {

    void setDaysOfWeek(List<DayOfWeek> daysOfWeek);
}
