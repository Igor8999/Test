package lanars.com.defigo.settings.mutemode;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.repository.user.IDataRepository;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class MuteModePresenter extends BasePresenter<MuteModeView> {

    @Inject
    Router router;

    @Inject
    IDataRepository dataRepository;

    public static final String MUTE_DAY_PROVIDE_ACCESS = "mute_day_provide_access";

    public MuteModePresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    void onBack() {
        router.exit();
    }

    public void getDaysOfWeek(String keyMuteDays) {
        addToDisposable(dataRepository.getAllDaysOfWeek(keyMuteDays)
                .subscribe(this::setDaysInView, this::onErrorGetDays));
    }

    private void setDaysInView(List<DayOfWeek> dayOfWeeks) {
        getViewState().setDaysOfWeek(dayOfWeeks);
    }

    private void onErrorGetDays(Throwable throwable) {

    }

    public void saveMuteDays(List<DayOfWeek> dayOfWeeks, String keyMuteDays) {
        dataRepository.saveDayOfWeeks(dayOfWeeks, keyMuteDays);
    }
}
