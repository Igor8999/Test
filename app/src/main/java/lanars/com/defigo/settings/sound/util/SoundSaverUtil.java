package lanars.com.defigo.settings.sound.util;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.common.utils.IAppPreferences;

public final class SoundSaverUtil {
    public static void saveSound(String soundUrl, IAppPreferences appPreferences, String fileName) {
        File file = getSoundFileByName(fileName);

        //noinspection ResultOfMethodCallIgnored
        file.delete();
        appPreferences.saveCurrentSound(null, null);

        InputStream input = null;
        OutputStream output = null;

        try {
            URL url = new URL(soundUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            input = new BufferedInputStream(connection.getInputStream());
            output = new FileOutputStream(file);

            byte data[] = new byte[1024];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

        } catch (Throwable exception) {
            handleCrash(exception, appPreferences);
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }

                if (input != null) {
                    input.close();
                }
            } catch (Throwable exception) {
                handleCrash(exception, appPreferences);
            }
        }

        appPreferences.saveCurrentSound(soundUrl, file.getPath());
    }

    @NonNull
    private static File getSoundFileByName(String fileName) {
        return new File(getExternalStoragePublicDirectory(), fileName);
    }

    private static File getExternalStoragePublicDirectory() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES);
    }

    private static void handleCrash(Throwable exception, IAppPreferences appPreferences) {
        Logger.e(exception, "sound error");
        appPreferences.saveCurrentSound(null, null);
        exception.printStackTrace();
        Crashlytics.logException(exception);
    }

    public static String getFileNameFromUrl(String soundUrl) {
        if (soundUrl == null || soundUrl.length() == 0) {
            return "";
        }

        int lastSlashIndex = soundUrl.lastIndexOf("/");
        int lastDotIndex = soundUrl.lastIndexOf(".");

        if (lastSlashIndex == -1 || lastDotIndex == -1) {
            return "";
        }

        String soundName = soundUrl.substring(lastSlashIndex + 1, lastDotIndex);

        return soundName.replace("+", " ");
    }

    public static void checkIfSoundExist(String soundUrl, IAppPreferences appPreferences) {
        String soundFileName = getFileNameFromUrl(soundUrl);

        if (TextUtils.isEmpty(soundFileName)) {
            return;
        }

        File soundFile = getSoundFileByName(soundFileName);

        if (!soundFile.exists()) {
            return;
        }

        Completable
                .fromAction(() -> saveSound(soundUrl, appPreferences, soundFileName))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> Logger.d("Sound is downloaded successfully"), throwable -> {
                    throwable.printStackTrace();
                    Crashlytics.logException(throwable);
                });
    }
}
