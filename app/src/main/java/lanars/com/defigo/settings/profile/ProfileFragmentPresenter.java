package lanars.com.defigo.settings.profile;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.service.IDeviceService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.DeviceIdUtil;
import lanars.com.defigo.common.utils.IAppPreferences;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ProfileFragmentPresenter extends BasePresenter<ProfileSettingsView> {

    @Inject
    Router router;

    @Inject
    Context appContext;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IUserRepository userRepository;

    @Inject
    IDeviceService deviceService;

    @Inject
    SocketManager socketManager;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().onShowLogoutButton(true);
    }

    public ProfileFragmentPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void goToPersonalInfo() {
        router.navigateTo(RouteScreenName.PERSONAL_INFO);
    }

    public void goToAddressSettings() {
        router.navigateTo(RouteScreenName.ADDRESS);
    }

    public void goToChangePassword() {
        router.navigateTo(RouteScreenName.CHANGE_PASSWORD);
    }

    public void goToEmailSettings() {
        router.navigateTo(RouteScreenName.EMAIL);
    }

    public void goToPhoneSettings() {
        router.navigateTo(RouteScreenName.CHANGE_PHONE_NUMBER);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getViewState().onShowLogoutButton(false);
    }

    public void clearData() {
        //todo need improve logic
        String deviceId = FirebaseInstanceId.getInstance().getId();
        DeviceIdUtil.deleteDeviceId(deviceService, deviceId);

        addToDisposable(userRepository.clearUsers()
                .flatMap((Function<Integer, ObservableSource<?>>) integer -> userRepository.clearRooms())
                .subscribe(this::logout));
    }

    private void logout(Object o) {
        socketManager.disconnect();
        if (appPreferences.clearData()) {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }
            getViewState().logoutView();
        }
    }

    public void goToLanguageSettings() {
        router.navigateTo(RouteScreenName.LANGUAGE);
    }
}
