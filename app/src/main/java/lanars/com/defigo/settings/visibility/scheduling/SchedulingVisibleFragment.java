package lanars.com.defigo.settings.visibility.scheduling;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.models.VisibilityOptions;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.settings.ui.DoorbellDataPicker;

public class SchedulingVisibleFragment extends BaseSettingsFragment implements SchedulingVisibleView {

    @InjectPresenter
    SchedulingVisiblePresenter schedulingVisiblePresenter;

    @ProvidePresenter
    public SchedulingVisiblePresenter createSchedulingVisiblePresenter() {
        return new SchedulingVisiblePresenter(visibilityDoorModel);
    }

    @BindView(R.id.llMuteMode)
    LinearLayout llMuteMode;

    @BindView(R.id.swMuteMode)
    SwitchCompat swMuteMode;

    @BindView(R.id.doorbellDataPicker)
    DoorbellDataPicker doorbellDataPicker;

    private VisibilityDoorModel visibilityDoorModel;

    public static SchedulingVisibleFragment newInstance(Bundle bundle) {
        SchedulingVisibleFragment fragment = new SchedulingVisibleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected String getTitle() {
        return getString(R.string.set_schedule_entrances);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.shedule_entrances_fragment, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        visibilityDoorModel = (VisibilityDoorModel) getArguments().getSerializable(AppConst.DOORBEL_VISIBILITY);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initViews(View view) {
        schedulingVisiblePresenter.addToDisposable(RxCompoundButton.checkedChanges(swMuteMode)
                .subscribe(this::checkMuteMode));
        doorbellDataPicker.setOnClickDoorbellDayOfWeek(this::goToMuteDaysOfWeekScreen);
        doorbellDataPicker.setOnDoorbellTimeSetListener((view1, hourOfDay, minute, isSelectFrom) -> schedulingVisiblePresenter.setTimeInView(isSelectFrom, hourOfDay, minute));
    }

    private void checkMuteMode(Boolean isChecked) {
        llMuteMode.setVisibility(isChecked ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.llSchedule)
    void muteModeChange() {
        swMuteMode.setChecked(!swMuteMode.isChecked());
    }

    void goToMuteDaysOfWeekScreen() {
        schedulingVisiblePresenter.goToMuteDaysOfWeek();
    }

    @Override
    public void handleBackClick() {
        schedulingVisiblePresenter.onBack();
    }

    @Override
    public void onShowError(String errorMessage) {
        Alert.create(getActivity())
                .setText(errorMessage)
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

    @Override
    public void setMuteDaysText(String muteDaysText) {
        doorbellDataPicker.setMuteDaysText(muteDaysText);
    }

    @OnClick(R.id.btnSubmit)
    void submit() {
        schedulingVisiblePresenter.setMuteMode(swMuteMode.isChecked(), doorbellDataPicker.getStartTimeStr(), doorbellDataPicker.getEndTimeStr(), visibilityDoorModel.getId(), visibilityDoorModel.getIsSingleButton());
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setTimeFrom(int hourOfDay, int minute) {
        doorbellDataPicker.setStartTime(String.format("%02d:%02d", hourOfDay, minute));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setTimeTo(int hourOfDay, int minute) {
        doorbellDataPicker.setEndTime(String.format("%02d:%02d", hourOfDay, minute));
    }

    @Override
    public void onShowSuccessUserUpdate() {
        Alert.create(getActivity())
                .setDuration(2000)
                .setText(R.string.changes_saved)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @Override
    public void setShowScheduleSettings(VisibilityDoorModel visibilityDoorModel) {
        VisibilityOptions visibilityOptions = visibilityDoorModel.getVisibilityOptions();
        if (visibilityOptions == null) {
            return;
        }
        swMuteMode.setChecked(visibilityOptions.getDaysOfWeek() != null);
        if (visibilityOptions.getDaysOfWeek() != null && !visibilityOptions.getDaysOfWeek().isEmpty()) {
            schedulingVisiblePresenter.getMuteDaysFromIndexes(visibilityOptions.getDaysOfWeek());
        }
        if (!TextUtils.isEmpty(visibilityOptions.getEndTime())) {
            doorbellDataPicker.setEndTime(visibilityOptions.getEndTime().substring(0, 5));
        }
        if (!TextUtils.isEmpty(visibilityOptions.getStartTime())) {
            doorbellDataPicker.setStartTime(visibilityOptions.getStartTime().substring(0, 5));
        }
    }
}
