package lanars.com.defigo.settings.profile;

import com.arellomobile.mvp.MvpView;

public interface ProfileSettingsView extends MvpView {

    void onShowLogoutButton(boolean isShow);

    void logoutView();
}
