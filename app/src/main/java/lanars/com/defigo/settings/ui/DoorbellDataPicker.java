package lanars.com.defigo.settings.ui;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;

public class DoorbellDataPicker extends FrameLayout implements TimePickerDialog.OnTimeSetListener {

    private TimePickerDialog timePickerDialog;

    private OnDoorbellTimeSetListener onDoorbellTimeSetListener;

    private OnClickDoorbellDayOfWeek onClickDoorbellDayOfWeek;

    @BindView(R.id.llDayOfWeek)
    LinearLayout llDayOfWeek;

    @BindView(R.id.tvMuteDays)
    TextView tvMuteDays;

    @BindView(R.id.tvFrom)
    TextView tvFrom;

    @BindView(R.id.tvTo)
    TextView tvTo;

    private final int START_HOURS = 11;

    private final int START_MINUTS = 30;

    private boolean isSelectFrom;

    public DoorbellDataPicker(@NonNull Context context) {
        super(context);
    }

    public DoorbellDataPicker(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public DoorbellDataPicker(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public DoorbellDataPicker(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        inflate(getContext(), R.layout.doorbell_data_picker, this);
        ButterKnife.bind(this);
        timePickerDialog = new TimePickerDialog(context, R.style.TimePickerStyle, this, START_HOURS, START_MINUTS, true);
        timePickerDialog.setTitle("");
    }

    @OnClick(R.id.llFrom)
    void setTimeFrom() {
        isSelectFrom = true;
        timePickerDialog.show();
    }

    @OnClick(R.id.llTo)
    void setTimeTo() {
        isSelectFrom = false;
        timePickerDialog.show();
    }

    @OnClick(R.id.llDayOfWeek)
    void goToMuteDaysOfWeekScreen() {
        onClickDoorbellDayOfWeek.onClickDayOfWeek();
    }

    public void setOnClickDoorbellDayOfWeek(OnClickDoorbellDayOfWeek onClickDoorbellDayOfWeek) {
        this.onClickDoorbellDayOfWeek = onClickDoorbellDayOfWeek;
    }

    public void setOnDoorbellTimeSetListener(OnDoorbellTimeSetListener onDoorbellTimeSetListener) {
        this.onDoorbellTimeSetListener = onDoorbellTimeSetListener;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        onDoorbellTimeSetListener.onTimeSet(view, hourOfDay, minute, isSelectFrom);
    }

    public interface OnDoorbellTimeSetListener {
        void onTimeSet(TimePicker timePickerView, int hourOfDay, int minute, boolean isSelectFrom);
    }

    public interface OnClickDoorbellDayOfWeek {
        void onClickDayOfWeek();
    }

    public void setStartTime(String startTime) {
        tvFrom.setText(startTime);
    }

    public void setEndTime(String endTime) {
        tvTo.setText(endTime);
    }

    public void setMuteDaysText(String muteDaysStr) {
        tvMuteDays.setText(muteDaysStr);
    }

    public String getStartTimeStr() {
        return tvFrom.getText().toString();
    }

    public String getEndTimeStr() {
        return tvTo.getText().toString();
    }
}
