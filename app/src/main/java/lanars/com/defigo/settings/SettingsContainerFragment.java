package lanars.com.defigo.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.orhanobut.logger.Logger;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.cropimage.CropFragment;
import lanars.com.defigo.settings.access.ProvideAccessListFragment;
import lanars.com.defigo.settings.access.ProvideAccessScheduleFragment;
import lanars.com.defigo.settings.actions.ActionsSettingsFragment;
import lanars.com.defigo.settings.actions.opendoors.OpenDoorsFragment;
import lanars.com.defigo.settings.address.AddressFragment;
import lanars.com.defigo.settings.call.CallSettingsFragment;
import lanars.com.defigo.settings.callingsequence.CallingSequenceMenuFragment;
import lanars.com.defigo.settings.callingsequence.membersettings.CallingSequenceSettingsFragment;
import lanars.com.defigo.settings.doorbell.DoorbellSettingsFragment;
import lanars.com.defigo.settings.email.EmailSettingsFragment;
import lanars.com.defigo.settings.language.LanguageSettingsFragment;
import lanars.com.defigo.settings.layout.LayoutSettingsFragment;
import lanars.com.defigo.settings.mutemode.MuteModeDaysFragment;
import lanars.com.defigo.settings.notifications.NotificationsSettingsFragment;
import lanars.com.defigo.settings.password.ChangePasswordFragment;
import lanars.com.defigo.settings.phonenumber.PhoneNumberSettingsFragment;
import lanars.com.defigo.settings.pincode.ChangePinCodeFragment;
import lanars.com.defigo.settings.profile.ProfileSettingsFragment;
import lanars.com.defigo.settings.profile.personal.PersonalInfoFragment;
import lanars.com.defigo.settings.rfid.RfidCardSettingsFragment;
import lanars.com.defigo.settings.rfid.edit.EditUserRfidCardFragment;
import lanars.com.defigo.settings.sound.SoundSettingsFragment;
import lanars.com.defigo.settings.visibility.VisibilityOptionsFragment;
import lanars.com.defigo.settings.visibility.scheduling.SchedulingVisibleFragment;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;

public class SettingsContainerFragment extends MvpAppCompatFragment implements SettingsContainerView {

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter
    SettingsContainerPresenter settingsContainerPresenter;

    private Navigator navigator;

    private Map<String, Fragment> screensFragmentMap = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getChildFragmentManager().findFragmentById(R.id.settingsContainer) == null) {
            settingsContainerPresenter.goToSettingsMenu();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(getNavigator());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_fragment, container, false);
    }

    @Override
    public void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.settingsContainer) {

                @Override
                protected Intent createActivityIntent(Context context, String screenKey, Object data) {
                    Logger.d("createActivityIntent " + screenKey);
                    return null;
                }

                @Override
                protected Fragment createFragment(String screenKey, Object data) {
                    Logger.d("createFragment " + screenKey);
                    initScreens(data);
                    return screensFragmentMap.get(screenKey);
                }
            };
        }
        return navigator;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && settingsContainerPresenter != null) {
            settingsContainerPresenter.onBackToSettingsFragment();
        }
    }

    public void initScreens(Object data) {
        if (screensFragmentMap.isEmpty() || data != null) {
            screensFragmentMap.clear();
            screensFragmentMap.put(RouteScreenName.SETTINGS_MENU, new SettingsMenuFragment());
            screensFragmentMap.put(RouteScreenName.DOORBEL_SETTINGS, new DoorbellSettingsFragment());
            screensFragmentMap.put(RouteScreenName.ACTIONS_SETTINGS, new ActionsSettingsFragment());
            screensFragmentMap.put(RouteScreenName.RFID_SETTINGS, new RfidCardSettingsFragment());
            screensFragmentMap.put(RouteScreenName.CHANGE_PIN, ChangePinCodeFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.LAYOUT_SETTINGS, new LayoutSettingsFragment());
            screensFragmentMap.put(RouteScreenName.CROP_FRAGMENT, CropFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.PROVIDE_ACCESS, new ProvideAccessListFragment());
            screensFragmentMap.put(RouteScreenName.PROFILE_SETTINGS, new ProfileSettingsFragment());
            screensFragmentMap.put(RouteScreenName.PERSONAL_INFO, new PersonalInfoFragment());
            screensFragmentMap.put(RouteScreenName.ADDRESS, new AddressFragment());
            screensFragmentMap.put(RouteScreenName.CHANGE_PASSWORD, new ChangePasswordFragment());
            screensFragmentMap.put(RouteScreenName.EMAIL, new EmailSettingsFragment());
            screensFragmentMap.put(RouteScreenName.CHANGE_PHONE_NUMBER, new PhoneNumberSettingsFragment());
            screensFragmentMap.put(RouteScreenName.VISIBILITY_OPTIONS, new VisibilityOptionsFragment());
            screensFragmentMap.put(RouteScreenName.SHEDULING_VISIBILITY, SchedulingVisibleFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.MUTE_MODE, MuteModeDaysFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.OPEN_DOORS_ACTION, OpenDoorsFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.CALLING_SEQUENCE_SETTINGS, CallingSequenceSettingsFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.CALLING_SEQUENCE_MENU, new CallingSequenceMenuFragment());
            screensFragmentMap.put(RouteScreenName.NOTIFICATIONS_SETTINGS, new NotificationsSettingsFragment());
            screensFragmentMap.put(RouteScreenName.SOUND_SETTINGS, new SoundSettingsFragment());
            screensFragmentMap.put(RouteScreenName.SCHEDULE_PROVIDE_ACCESS, ProvideAccessScheduleFragment.newInstance((Bundle) data));
            screensFragmentMap.put(RouteScreenName.LANGUAGE, new LanguageSettingsFragment());
            screensFragmentMap.put(RouteScreenName.CALL_SETTINGS, new CallSettingsFragment());
            screensFragmentMap.put(RouteScreenName.EDIT_CARDS, EditUserRfidCardFragment.newInstance((Bundle) data));
        }
    }
}