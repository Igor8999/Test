package lanars.com.defigo.settings.doorbell;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class DoorbellSettingsPresenter extends BasePresenter<DoorbellSettingView> {

    @Inject
    Router router;

    public DoorbellSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void goToActions() {
        router.navigateTo(RouteScreenName.ACTIONS_SETTINGS);
    }

    public void goToRfidManagement() {
        router.navigateTo(RouteScreenName.RFID_SETTINGS);
    }

    public void goToLayoutSettings() {
        router.navigateTo(RouteScreenName.LAYOUT_SETTINGS);
    }

    public void goToProvideAccess() {
        router.navigateTo(RouteScreenName.PROVIDE_ACCESS);
    }

    public void goToVisibilityOptions() {
        router.navigateTo(RouteScreenName.VISIBILITY_OPTIONS);
    }

    public void goToCallSequenceOptions() {
        router.navigateTo(RouteScreenName.CALLING_SEQUENCE_MENU);
    }
}
