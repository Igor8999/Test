package lanars.com.defigo.settings;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class SettingsContainerPresenter extends BasePresenter<SettingsContainerView> {

    @Inject
    Router router;

    public SettingsContainerPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void goToSettingsMenu() {
        router.navigateTo(RouteScreenName.SETTINGS_MENU);
    }

    public void goToDoorbellSettings() {
        router.navigateTo(RouteScreenName.DOORBEL_SETTINGS);
    }

    public void goToProfileSettings() {
        router.navigateTo(RouteScreenName.PROFILE_SETTINGS);
    }

    public void goToNotificationsSettings() {
        router.navigateTo(RouteScreenName.NOTIFICATIONS_SETTINGS);
    }

    public void onBackToSettingsFragment() {
        router.backTo(RouteScreenName.SETTINGS_MENU);
    }

    public void goToSoundsSettings() {
        router.navigateTo(RouteScreenName.SOUND_SETTINGS);
    }

    public void goToCallSettings() {
        router.navigateTo(RouteScreenName.CALL_SETTINGS);
    }
}
