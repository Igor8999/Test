package lanars.com.defigo.settings.call;

import com.arellomobile.mvp.InjectViewState;
import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import io.reactivex.Observable;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBUser;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class CallSettingsPresenter extends BasePresenter<CallSettingsView> {

    @Inject
    Router router;

    @Inject
    IUserService userService;

    @Inject
    IUserRepository userRepository;

    public CallSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        addToDisposable(userRepository.usersMy()
                .flatMap(user -> Observable.just(UserConverter.convertToDatabaseUser(user)))
                .doOnNext(user -> userRepository.insertUsers())
                .subscribe(this::onSuccess, this::onError));
    }

    private void onSuccess(DBUser dbUser) {
        getViewState().setVideoEnabled(dbUser.hasVideo);
    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
        Crashlytics.logException(throwable);
    }

    public void onBack() {
        router.exit();
    }

    public void onSwitchStatusChanged(boolean isChecked) {
        boolean result = !isChecked;

        getViewState().setVideoEnabled(result);
        UserUpdateRequestBody.Builder requestBody = new UserUpdateRequestBody.Builder();
        requestBody.setHasVideo(RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(result)));
        addToDisposable(userService.usersMyUpdate(requestBody.build())
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doAfterTerminate(() -> getViewState().onHideProgress())
                .map(UserConverter::convertToDatabaseUser)
                .doOnNext(dbUser -> userRepository.updateUsers(dbUser))
                .ignoreElements()
                .subscribe(this::onSuccessUpdatingCallSettings, this::onErrorUpdatingCallSettings));
    }

    private void onSuccessUpdatingCallSettings() {
        getViewState().onSuccessUpdate(R.string.success_update_call_setting);
    }

    private void onErrorUpdatingCallSettings(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

}
