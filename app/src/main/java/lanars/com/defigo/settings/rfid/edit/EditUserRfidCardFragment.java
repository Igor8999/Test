package lanars.com.defigo.settings.rfid.edit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.utils.AppConst;

public class EditUserRfidCardFragment extends BaseSettingsFragment implements EditUserRfidCardView {

    private RfidModel selectedCard;
    private int userId;

    @BindView(R.id.tvCardId)
    TextView tvCardId;

    @BindView(R.id.etCardName)
    EditText etCardName;

    @InjectPresenter
    EditUserRfidCardFragmentPresenter presenter;

    @ProvidePresenter
    public EditUserRfidCardFragmentPresenter provide() {
        userId = getArguments().getInt(AppConst.USER_ID);
        return new EditUserRfidCardFragmentPresenter(userId);
    }

    public static EditUserRfidCardFragment newInstance(Bundle bundle) {
        EditUserRfidCardFragment fragment = new EditUserRfidCardFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        selectedCard = (RfidModel) getArguments().getSerializable(AppConst.CARD);
        userId = getArguments().getInt(AppConst.USER_ID);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initViews(View view) {
        if (selectedCard != null) {
            tvCardId.setText(String.format("%s %s", getString(R.string.card_id), selectedCard.getId()));
            etCardName.setText(selectedCard.getName());
        }
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_white_48px));
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.edit_card_fragment, container, false);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.add_card);
    }

    @OnClick(R.id.btnSave)
    void saveChanges() {
        selectedCard.setName(etCardName.getText().toString());
        presenter.updateCard(selectedCard);
    }

    @Override
    public void handleBackClick() {
        presenter.onBack();
    }

    @Override
    public void setCloseIcon(boolean isClose) {
        imgBack.setImageDrawable(isClose ? getResources().getDrawable(R.drawable.ic_close_white_48px) : getResources().getDrawable(R.drawable.ic_arrow_back_white_48px));
    }
}
