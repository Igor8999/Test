package lanars.com.defigo.settings.actions.opendoors;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.StatusModel;

public interface OpenDoorsView extends BaseMvpView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void setActionsInView(ActionsOptionsModel actionsOptionsModel);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void successUpdateRulesAction(StatusModel statusModel);

    void setMuteDaysText(String muteDaysStr);

    void setTimeFrom(int hourOfDay, int minute);

    void setTimeTo(int hourOfDay, int minute);

    void onShowLoadingProgress();
}
