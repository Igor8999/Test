package lanars.com.defigo.settings.phonenumber;

import lanars.com.defigo.common.base.BaseMvpView;

public interface PhoneNumberView extends BaseMvpView {

    void checkPermissionAndGetPhoneNumber();

    void setCurrentPhoneNumber(String phoneNumber);

    void verifyCodeSuccess();

    void onShowSuccessUserUpdate();

    void onChangePhoneButtonEnable(boolean isVisible);

    void showStatusVerified(boolean isVerified);
}
