package lanars.com.defigo.settings.notifications;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.NotificationModel;
import lanars.com.defigo.common.repository.user.IDataRepository;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class NotificationsSettingsPresenter extends BasePresenter<NotificationsSettingsView> {

    @Inject
    IDataRepository dataRepository;

    @Inject
    Router router;

    public NotificationsSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void getNotificationsSettings() {
        dataRepository.getNotificationsSettings()
                .subscribe(this::getSuccessNotificationsSettings,
                        this::getErrorNotificationsSettings);
    }

    private void getSuccessNotificationsSettings(List<NotificationModel> notificationModels) {
        getViewState().getNotificationsSettingsInView(notificationModels);
    }

    private void getErrorNotificationsSettings(Throwable throwable) {

    }

    public void onBack() {
        router.exit();
    }
}
