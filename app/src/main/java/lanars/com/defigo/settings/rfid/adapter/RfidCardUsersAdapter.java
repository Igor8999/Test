package lanars.com.defigo.settings.rfid.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.RfidModel;

public class RfidCardUsersAdapter extends RecyclerView.Adapter<RfidCardUsersAdapter.ViewHolder> {

    private final OnItemClick onClickListener;
    private final OnItemClick onEditListener;
    private List<RfidModel> rfidModelList;

    public RfidCardUsersAdapter(List<RfidModel> rfidModelList, OnItemClick onClickListener, OnItemClick onEditListener) {
        this.rfidModelList = rfidModelList;
        this.onClickListener = onClickListener;
        this.onEditListener = onEditListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rfid_card_settings, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RfidModel rfidModel = rfidModelList.get(position);
        Context context = holder.itemView.getContext();

        holder.tvNameUser.setText(rfidModel.getName());
        holder.tvIdUser.setText(String.valueOf(rfidModel.getId()));

        int rfidDrawableResId = rfidModel.isDisabled()
                ? R.drawable.card
                : R.drawable.card_q;

        Drawable drawable = context.getDrawable(rfidDrawableResId);
        holder.imgStatusActivate.setImageDrawable(drawable);

        int statusTextResId = rfidModel.isDisabled()
                ? R.string.deactivated
                : R.string.activated;

        holder.tvStatusActivate.setText(statusTextResId);
        int statusTextColor = rfidModel.isDisabled()
                ? ContextCompat.getColor(context, R.color.colorTextSettings)
                : ContextCompat.getColor(context, R.color.colorPrimaryDark);

        holder.tvStatusActivate.setTextColor(statusTextColor);

        holder.llRfidContainer.setOnClickListener(v -> onClickListener.onClick(holder.getAdapterPosition()));
        holder.btnEdit.setOnClickListener(v -> onEditListener.onClick(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return rfidModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgStatusActivate)
        ImageView imgStatusActivate;

        @BindView(R.id.tvIdUser)
        TextView tvIdUser;

        @BindView(R.id.tvNameUser)
        TextView tvNameUser;

        @BindView(R.id.tvStatusActivate)
        TextView tvStatusActivate;

        @BindView(R.id.llRfidContainer)
        ViewGroup llRfidContainer;

        @BindView(R.id.btnEdit)
        ImageButton btnEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClick {
        void onClick(int position);
    }
}
