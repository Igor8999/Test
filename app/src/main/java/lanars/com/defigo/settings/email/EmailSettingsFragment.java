package lanars.com.defigo.settings.email;

import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.Utils;

public class EmailSettingsFragment extends BaseSettingsFragment implements EmailSettingsView {

    @InjectPresenter
    EmailSettingsPresenter emailSettingsPresenter;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.tvVerified)
    TextView tvVerified;

    @BindView(R.id.tvNoVerified)
    TextView tvNoVerified;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @Override
    protected String getTitle() {
        return getString(R.string.email_management);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.email_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {

    }

    @OnTextChanged(value = R.id.etEmail, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void changePhoneEnable(Editable editable) {
        emailSettingsPresenter.changeEmailEnable(editable.toString());
    }

    @OnClick(R.id.btnSubmit)
    void changeEmail() {
        Utils.hideKeyboard(getActivity());
        emailSettingsPresenter.changeEmail(etEmail.getText().toString());
    }

    @Override
    public void showUserEmail(String email) {
        if (!TextUtils.isEmpty(email)) {
            etEmail.setText(email);
        }
    }

    @Override
    public void onShowSuccessUserUpdate() {
        Alert.create(getActivity())
                .setDuration(2000)
                .setText(R.string.changes_saved)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @Override
    public void onChangeEmailButtonEnable(boolean isEnabled) {
        showStatusVerified(isEnabled);
    }

    @Override
    public void showStatusVerified(boolean isVerified) {
        tvNoVerified.setVisibility(isVerified ? View.GONE : View.VISIBLE);
        tvVerified.setVisibility(isVerified ? View.VISIBLE : View.GONE);
        btnSubmit.setVisibility(isVerified ? View.GONE : View.VISIBLE);
    }

    @Override
    public void enterCorrectEmailAlert() {
        Alert.create(getActivity())
                .setText(getString(R.string.enter_email))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void handleBackClick() {
        emailSettingsPresenter.onBack();
    }

}
