package lanars.com.defigo.settings.visibility.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.TypeVisibilityOptions;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;

public class VisibilityOptionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<VisibilityDoorModel> visibilityDoorModels;

    public List<VisibilityDoorModel> getVisibilityDoorModels() {
        return visibilityDoorModels;
    }

    private OnItemClickRecyclerListener onItemClickRecyclerListener;

    public VisibilityOptionsAdapter(List<VisibilityDoorModel> visibilityDoorModels, OnItemClickRecyclerListener onItemClickRecyclerListener) {
        this.visibilityDoorModels = visibilityDoorModels;
        this.onItemClickRecyclerListener = onItemClickRecyclerListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TypeVisibilityOptions.TYPE_CALL_VISIBILITY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visibility_doors_call_adapter, parent, false);
                VisibilityOptionsCallViewHolder visibilityOptionsCallViewHolder = new VisibilityOptionsCallViewHolder(view);
                setOnClickListenerVisibility(visibilityOptionsCallViewHolder);
                return visibilityOptionsCallViewHolder;
            case TypeVisibilityOptions.TYPE_DISABLE_VISIBILITY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visibility_doors_disable_adapter, parent, false);
                VisibilityOptionsDisableViewHolder visibilityOptionsDisableViewHolder = new VisibilityOptionsDisableViewHolder(view);
                setOnClickListenerVisibility(visibilityOptionsDisableViewHolder);
                return visibilityOptionsDisableViewHolder;
            case TypeVisibilityOptions.TYPE_SCHEDULE_VISABILITY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visibility_doors_shedule_adapter, parent, false);
                VisibilityOptionsScheduleViewHolder visibilityOptionsScheduleViewHolder = new VisibilityOptionsScheduleViewHolder(view);
                setOnClickListenerVisibility(visibilityOptionsScheduleViewHolder);
                return visibilityOptionsScheduleViewHolder;
            default:
                return null;
        }
    }

    private void setOnClickListenerVisibility(VisibilityOptionsBaseViewHolder visibilityOptionsBaseViewHolder) {
        visibilityOptionsBaseViewHolder.itemView.setOnClickListener(v -> {
            int position = visibilityOptionsBaseViewHolder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onItemClickRecyclerListener.onItemClick(position);
            }
        });

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        VisibilityDoorModel visibilityDoorModel = visibilityDoorModels.get(position);
        switch (viewType) {
            case TypeVisibilityOptions.TYPE_CALL_VISIBILITY:
                VisibilityOptionsCallViewHolder visibilityOptionsCallViewHolder = (VisibilityOptionsCallViewHolder) holder;
                visibilityOptionsCallViewHolder.tvNameUser.setText(visibilityDoorModel.getPublicName());
                break;
            case TypeVisibilityOptions.TYPE_DISABLE_VISIBILITY:
                VisibilityOptionsDisableViewHolder visibilityOptionsDisableViewHolder = (VisibilityOptionsDisableViewHolder) holder;
                visibilityOptionsDisableViewHolder.tvNameUser.setText(visibilityDoorModel.getPublicName());
                break;
            case TypeVisibilityOptions.TYPE_SCHEDULE_VISABILITY:
                VisibilityOptionsScheduleViewHolder visibilityOptionsScheduleViewHolder = (VisibilityOptionsScheduleViewHolder) holder;
                visibilityOptionsScheduleViewHolder.tvNameUser.setText(visibilityDoorModel.getPublicName());
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        VisibilityDoorModel visibilityDoorModel = visibilityDoorModels.get(position);
        if (visibilityDoorModel.getVisibilityOptions().getDisabled() != null && visibilityDoorModel.getVisibilityOptions().getDisabled()) {
            return TypeVisibilityOptions.TYPE_DISABLE_VISIBILITY;
        }
        if (visibilityDoorModel.getVisibilityOptions().getDaysOfWeek() == null) {
            return TypeVisibilityOptions.TYPE_CALL_VISIBILITY;
        }
        return TypeVisibilityOptions.TYPE_SCHEDULE_VISABILITY;
    }

    @Override
    public int getItemCount() {
        return visibilityDoorModels.size();
    }

}
