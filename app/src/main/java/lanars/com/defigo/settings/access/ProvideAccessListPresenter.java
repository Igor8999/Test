package lanars.com.defigo.settings.access;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.AppConst;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ProvideAccessListPresenter extends BasePresenter<ProvideAccessListView> {

    @Inject
    Router router;

    @Inject
    IUserRepository userRepository;

    @Inject
    IUserService userService;

    public static final int PROVIDE_LIMIT = 10;

    public ProvideAccessListPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    ProvideAccessListPresenter(Router router, IUserRepository userRepository) {
        this.router = router;
        this.userRepository = userRepository;
    }

    @Override
    public void attachView(ProvideAccessListView view) {
        super.attachView(view);
        getViewState().getProvideUsers();
    }

    public void onBack() {
        router.exit();
    }

    public void getProvideUsers() {
        addToDisposable(
                userRepository.getSharedAccessByUser()
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .subscribe(this::setProvideUsersSuccess,
                                this::onErrorGetProvideUsers));
    }

    public void searchUsers(String search, int page, int limit) {
        addToDisposable(
                userRepository.users(search, page * limit, limit)
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .subscribe(
                                this::onSuccessGetFoundUsers,
                                this::onErrorGetFoundUsers
                        ));
    }

    private void setProvideUsersSuccess(List<User> users) {
        getViewState().setProvideUsers(users);
    }

    private void onErrorGetProvideUsers(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void goToProvideAccessScheduling(User user) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.PROVIDE_USER, user);
        router.navigateTo(RouteScreenName.SCHEDULE_PROVIDE_ACCESS, bundle);
    }

    private void onSuccessGetFoundUsers(List<User> users) {
        getViewState().setAddFoundUsers(users);
    }

    public void deleteShareAccessUser(User user) {
        addToDisposable(userRepository.removeSharedAccess(user.getId())
                .subscribe(statusModel -> onDeleteProvideSuccess(user), this::onDeleteProvideError));
    }

    private void onDeleteProvideSuccess(User user) {
        getViewState().onDeleteUser(user);
    }

    private void onDeleteProvideError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onErrorGetFoundUsers(Throwable throwable) {
        getViewState().onShowError(throwable.getMessage());
    }
}
