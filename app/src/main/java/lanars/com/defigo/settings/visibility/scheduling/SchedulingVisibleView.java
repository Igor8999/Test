package lanars.com.defigo.settings.visibility.scheduling;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;

public interface SchedulingVisibleView extends BaseMvpView {

    void setMuteDaysText(String muteDaysText);

    void setTimeFrom(int hourOfDay, int minute);

    void setTimeTo(int hourOfDay, int minute);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onShowSuccessUserUpdate();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void setShowScheduleSettings(VisibilityDoorModel visibilityDoorModel);
}
