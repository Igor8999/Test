package lanars.com.defigo.settings.sound.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.SoundModel;

public class SoundSettingsAdapter extends RecyclerView.Adapter<SoundSettingsAdapter.ViewHolder> {

    private List<SoundModel> soundModels;
    private OnItemClick onItemClick;

    public SoundSettingsAdapter(List<SoundModel> soundModels, OnItemClick onItemClick) {
        this.soundModels = soundModels;
        this.onItemClick = onItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sounds_settings, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(v -> {
            int position = viewHolder.getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                onItemClick.onSoundClick(position);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SoundModel soundModel = soundModels.get(position);
        holder.tvName.setText(soundModel.getName());
        holder.imgSelect.setVisibility(soundModel.isSelected() ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return soundModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imSelect)
        ImageView imgSelect;

        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClick {
        void onSoundClick(int position);
    }
}
