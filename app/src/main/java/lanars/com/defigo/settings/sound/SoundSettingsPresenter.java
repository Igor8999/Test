package lanars.com.defigo.settings.sound;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.InjectViewState;
import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.SoundModel;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IDataRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.settings.sound.util.SoundSaverUtil;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class SoundSettingsPresenter extends BasePresenter<SoundSettingView> {

    @Inject
    Router router;

    @Inject
    IDataRepository dataRepository;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    Context context;

    @Inject
    IUserService userService;

    private List<SoundModel> soundModels;

    private MediaPlayer mediaPlayer;

    public SoundSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void getSounds() {
        addToDisposable(dataRepository.getSounds()
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doOnTerminate(() -> getViewState().onHideProgress())
                .subscribe(this::onSuccessGetSounds, this::onErrorGetSoundsModel));
    }

    private void onErrorGetSoundsModel(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessGetSounds(List<SoundModel> soundModels) {
        getViewState().setSoundsInView(this.soundModels = soundModels, this::handleOnItemClick);
        selectSavedSoundIfExists();
    }

    private void selectSavedSoundIfExists() {
        String savedSoundUrl = appPreferences.getCurrentSoundUrl();

        SoundSaverUtil.checkIfSoundExist(savedSoundUrl, appPreferences);

        for (SoundModel soundModel : soundModels) {
            if (savedSoundUrl.equals(soundModel.getUrl())) {
                soundModel.setSelected(true);
                getViewState().notifyDataChanges();
                return;
            }
        }
    }

    private void handleOnItemClick(int position) {
        deselectAllSounds();
        SoundModel soundModel = soundModels.get(position);
        checkMediaPlayerAndPlay(soundModel.getUrl());

        soundModel.setSelected(!soundModel.isSelected());
        getViewState().notifyDataChanges();
    }

    private void checkMediaPlayerAndPlay(String url) {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnPreparedListener(this::startPlay);
        }

        mediaPlayer.stop();
        mediaPlayer.reset();

        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            getViewState().onShowProgress();
        } catch (IOException exception) {
            Crashlytics.logException(exception);
            exception.printStackTrace();
            getViewState().onHideProgress();
        }
    }

    private void startPlay(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        getViewState().onHideProgress();
    }

    private void deselectAllSounds() {
        for (SoundModel soundModel : soundModels) {
            soundModel.setSelected(false);
        }
    }

    public void releaseMediaPlayerIfPlaying() {
        if (mediaPlayer == null) {
            return;
        }

        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer = null;
    }

    public void handleDonePress() {
        getViewState().requestExternalStorageWritePermission();
    }

    @Nullable
    private SoundModel getSelectedSoundModel() {
        for (SoundModel soundModel : soundModels) {
            if (soundModel.isSelected()) {
                return soundModel;
            }
        }

        return null;
    }

    public void onErrorPermission(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void onPermissionRequestSuccess(Boolean isGranted) {
        if (isGranted) {
            saveSelectedSound();
        } else {
            getViewState().showPermissionError();
        }
    }

    private void saveSelectedSound() {
        SoundModel soundModel = getSelectedSoundModel();

        if (soundModel == null) {
            return;
        }

        getViewState().onShowProgress();
        releaseMediaPlayerIfPlaying();

        Observable
                .just(soundModel.getUrl())
                .doOnNext(soundUrl -> SoundSaverUtil.saveSound(soundUrl, appPreferences, SoundSaverUtil.getFileNameFromUrl(soundUrl)))
                .doOnNext(this::saveUserCurrentSound)
                .ignoreElements()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleOnSuccess, this::handleOnError);
    }

    private void saveUserCurrentSound(String soundUrl) {
        UserUpdateRequestBody body = new UserUpdateRequestBody.Builder()
                .currentSound(RequestBody.create(MediaType.parse("multipart/form-data"), soundUrl))
                .build();

        userService.usersMyUpdate(body)
                .ignoreElements()
                .subscribe(this::onSaveUserSuccess, this::onSaveUserError);
    }

    private void onSaveUserError(Throwable throwable) {
        throwable.printStackTrace();
        Crashlytics.logException(throwable);
    }

    private void onSaveUserSuccess() {
        // TODO nothing
    }

    private void handleOnSuccess() {
        getViewState().onHideProgress();
        getViewState().moveBack();
    }

    private void handleOnError(Throwable throwable) {
        getViewState().onHideProgress();
        getViewState().onShowError(throwable.getLocalizedMessage());
        Crashlytics.logException(throwable);
    }
}
