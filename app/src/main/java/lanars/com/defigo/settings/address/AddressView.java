package lanars.com.defigo.settings.address;

import lanars.com.defigo.common.base.BaseMvpView;

public interface AddressView extends BaseMvpView {

    void showStreet(String street);

    void showZip(String zip);

    void showCountry(String country);

    void showCity(String country);

    void onShowSuccessUserUpdate();
}
