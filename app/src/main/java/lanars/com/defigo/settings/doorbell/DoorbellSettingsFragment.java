package lanars.com.defigo.settings.doorbell;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;

public class DoorbellSettingsFragment extends BaseSettingsFragment implements DoorbellSettingView {

    @InjectPresenter
    DoorbellSettingsPresenter doorbellSettingsPresenter;

    @Override
    protected String getTitle() {
        return getString(R.string.doorbell_settings);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.doorbell_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
    }

    @OnClick(R.id.llActions)
    void goToActions() {
        doorbellSettingsPresenter.goToActions();
    }

    @OnClick(R.id.llRfidManagement)
    void goToRfidManagement() {
        doorbellSettingsPresenter.goToRfidManagement();
    }

    @OnClick(R.id.llLayout)
    void goToLayoutSettings() {
        doorbellSettingsPresenter.goToLayoutSettings();
    }

    @OnClick(R.id.llProvideAccess)
    void goToProvideAccess() {
        doorbellSettingsPresenter.goToProvideAccess();
    }

    @OnClick(R.id.llVisibilityOptions)
    void goToVisibilityOptions() {
        doorbellSettingsPresenter.goToVisibilityOptions();
    }

    @OnClick(R.id.llCallSequence)
    void goToCallSequenceOptions() {
        doorbellSettingsPresenter.goToCallSequenceOptions();
    }

    @Override
    public void handleBackClick() {
        doorbellSettingsPresenter.onBack();
    }
}
