package lanars.com.defigo.settings.actions;

import lanars.com.defigo.common.base.BaseMvpView;

public interface ActionSettingsView extends BaseMvpView{
    void onShowErrorAnim();
}
