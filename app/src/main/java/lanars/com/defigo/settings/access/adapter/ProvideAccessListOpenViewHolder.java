package lanars.com.defigo.settings.access.adapter;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import ru.rambler.libs.swipe_layout.SwipeLayout;

public class ProvideAccessListOpenViewHolder extends ProvideAccessBaseViewHolder {

    @BindView(R.id.imgProvideTimer)
    ImageView imgProvideTimer;

    @BindView(R.id.tvStatusProvide)
    TextView tvStatusProvide;

    @BindView(R.id.swLayout)
    SwipeLayout swLayout;

    @BindView(R.id.btnDelete)
    ImageButton btnDelete;

    public ProvideAccessListOpenViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
