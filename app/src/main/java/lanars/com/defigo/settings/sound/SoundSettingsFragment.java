package lanars.com.defigo.settings.sound;

import android.Manifest;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.SoundModel;
import lanars.com.defigo.common.utils.Utils;
import lanars.com.defigo.settings.sound.adapter.SoundSettingsAdapter;

public class SoundSettingsFragment extends BaseSettingsFragment implements SoundSettingView {

    @InjectPresenter
    SoundSettingsPresenter presenter;

    @BindView(R.id.rvSounds)
    RecyclerView rvSounds;

    @BindView(R.id.imgDone)
    ImageView imgDone;

    private SoundSettingsAdapter adapter;

    @Override
    protected String getTitle() {
        return getString(R.string.sound_settings);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.sound_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        imgDone.setVisibility(View.VISIBLE);
        presenter.getSounds();
    }

    @Override
    public void handleBackClick() {
        moveBack();
    }

    @Override
    public void setSoundsInView(List<SoundModel> soundModels, SoundSettingsAdapter.OnItemClick onItemClick) {
        adapter = new SoundSettingsAdapter(soundModels, onItemClick);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvSounds.setLayoutManager(linearLayoutManager);
        rvSounds.setAdapter(adapter);
        rvSounds.setHasFixedSize(true);
        rvSounds.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void notifyDataChanges() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.releaseMediaPlayerIfPlaying();
    }

    @OnClick(R.id.imgDone)
    public void onDonePress(View view) {
        presenter.handleDonePress();
    }

    @Override
    public void moveBack() {
        presenter.onBack();
    }

    @Override
    public void requestExternalStorageWritePermission() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(SoundSettingsFragment.this::onSuccessPermission, SoundSettingsFragment.this::onErrorPermission);
    }

    private void onSuccessPermission(Boolean isGranted) {
        presenter.onPermissionRequestSuccess(isGranted);
    }

    private void onErrorPermission(Throwable throwable) {
        presenter.onErrorPermission(throwable);
    }

    @Override
    public void showPermissionError() {
        Utils.showPermissionRationaleAlert(getActivity(), R.string.storage_permission_rationale_text);
    }
}
