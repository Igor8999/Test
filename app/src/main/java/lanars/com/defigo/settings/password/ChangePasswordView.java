package lanars.com.defigo.settings.password;

import android.support.annotation.StringRes;

import lanars.com.defigo.common.base.BaseMvpView;

public interface ChangePasswordView extends BaseMvpView {
    void showValidationError(@StringRes int textResId);

    void onSuccessUpdate(@StringRes int successTextResId);

    void hideKeyboard();

}
