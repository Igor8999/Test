package lanars.com.defigo.settings.access;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.Schedule;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.settings.ui.DoorbellDataPicker;

public class ProvideAccessScheduleFragment extends BaseSettingsFragment implements ProvideAccessScheduleView {

    @BindView(R.id.llMuteMode)
    LinearLayout llMuteMode;

    @BindView(R.id.doorbellDataPicker)
    DoorbellDataPicker doorbellDataPicker;

    @BindView(R.id.swMuteMode)
    SwitchCompat swMuteMode;

    private User selectProvideUser;

    @InjectPresenter
    ProvideAccessSchedulePresenter provideAccessSchedulePresenter;

    @ProvidePresenter
    public ProvideAccessSchedulePresenter createProvidePresenter() {
        return new ProvideAccessSchedulePresenter(selectProvideUser);
    }

    public static ProvideAccessScheduleFragment newInstance(Bundle bundle) {
        ProvideAccessScheduleFragment fragment = new ProvideAccessScheduleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        selectProvideUser = (User) getArguments().getSerializable(AppConst.PROVIDE_USER);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.set_schedule);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.provide_access_schedule_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        provideAccessSchedulePresenter.addToDisposable(
                RxCompoundButton.checkedChanges(swMuteMode)
                        .subscribe(this::checkMuteMode));
        doorbellDataPicker.setOnClickDoorbellDayOfWeek(this::goToMuteDaysOfWeekScreen);
        doorbellDataPicker.setOnDoorbellTimeSetListener((view1, hourOfDay, minute, isSelectFrom) -> provideAccessSchedulePresenter.setTimeInView(isSelectFrom, hourOfDay, minute));
    }

    private void goToMuteDaysOfWeekScreen() {
        provideAccessSchedulePresenter.goToMuteDaysOfWeekScreen();
    }

    private void checkMuteMode(Boolean isChecked) {
        llMuteMode.setVisibility(isChecked ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.btnSubmit)
    protected void submit() {
        provideAccessSchedulePresenter.submit(swMuteMode.isChecked(), selectProvideUser, doorbellDataPicker.getStartTimeStr(), doorbellDataPicker.getEndTimeStr());
    }

    @Override
    public void handleBackClick() {
        provideAccessSchedulePresenter.onBack();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setTimeFrom(int hourOfDay, int minute) {
        doorbellDataPicker.setStartTime(String.format("%02d:%02d", hourOfDay, minute));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setTimeTo(int hourOfDay, int minute) {
        doorbellDataPicker.setEndTime(String.format("%02d:%02d", hourOfDay, minute));
    }

    @Override
    public void setMuteDaysText(String stringDaysOfWeeks) {
        doorbellDataPicker.setMuteDaysText(stringDaysOfWeeks);
    }

    @Override
    public void onSuccessProvideUser() {
        Alert.create(getActivity())
                .setText(R.string.provide_access_success)
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void setProvideSettings(Schedule schedule) {
        swMuteMode.setChecked(true);
        if (schedule.getDaysOfWeek() != null && !schedule.getDaysOfWeek().isEmpty()) {
            provideAccessSchedulePresenter.getMuteDaysFromIndexes(schedule.getDaysOfWeek());
        }
        if (!TextUtils.isEmpty(schedule.getEndTime())) {
            doorbellDataPicker.setEndTime(schedule.getEndTime().substring(0, 5));
        }

        if (!TextUtils.isEmpty(schedule.getStartTime())) {
            doorbellDataPicker.setStartTime(schedule.getStartTime().substring(0, 5));
        }
    }

}
