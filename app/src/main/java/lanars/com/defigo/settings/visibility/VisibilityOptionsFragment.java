package lanars.com.defigo.settings.visibility;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;
import lanars.com.defigo.settings.visibility.adapter.VisibilityOptionsAdapter;

public class VisibilityOptionsFragment extends BaseSettingsFragment implements VisibilityOptionsView, OnItemClickRecyclerListener {

    @BindView(R.id.rvVisibilityDoors)
    RecyclerView rvVisibilityDoors;

    private VisibilityOptionsAdapter adapter;

    @Override
    protected String getTitle() {
        return getString(R.string.visibility_options);
    }

    @InjectPresenter
    VisibilityOptionsPresenter visibilityOptionsPresenter;

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.visibility_options_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        visibilityOptionsPresenter.getAllVisibilityDoorbels();
    }

    @Override
    public void handleBackClick() {
        visibilityOptionsPresenter.onBack();
    }

    @Override
    public void setVisibilityDoors(List<VisibilityDoorModel> visibilityDoors) {
        adapter = new VisibilityOptionsAdapter(visibilityDoors, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvVisibilityDoors.setLayoutManager(linearLayoutManager);
        rvVisibilityDoors.setAdapter(adapter);
        rvVisibilityDoors.setHasFixedSize(true);
    }

    @Override
    public void onItemClick(int position) {
        visibilityOptionsPresenter.onItemClickVisibilityOptions(adapter.getVisibilityDoorModels().get(position));
    }
}
