package lanars.com.defigo.settings.phonenumber;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBUser;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class PhoneNumberPresenter extends BasePresenter<PhoneNumberView> {

    @Inject
    Router router;

    @Inject
    Context appContext;

    @Inject
    IUserRepository userRepository;

    @Inject
    IUserService userService;

    @Inject
    IAppPreferences appPreferences;
    private DBUser currentUser;

    @Override
    public void attachView(PhoneNumberView view) {
        super.attachView(view);
        getPhoneNumberUser();
    }

    public PhoneNumberPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    PhoneNumberPresenter(Router router, IUserRepository userRepository, IAppPreferences appPreferences, IUserService userService) {
        this.router = router;
        this.userRepository = userRepository;
        this.appPreferences = appPreferences;
        this.userService = userService;
    }

    protected void getPhoneNumberUser() {
        getViewState().onShowProgress();

        addToDisposable(
                userRepository
                        .getUserById(appPreferences.getCurrentUserId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::showPhoneNumberUser, this::onGetErrorPhoneNumberUser));
    }

    private void onGetErrorPhoneNumberUser(Throwable throwable) {
        getViewState().checkPermissionAndGetPhoneNumber();
        getViewState().onHideProgress();
    }

    private void showPhoneNumberUser(DBUser dbUser) {
        this.currentUser = dbUser;
        //not used TextUtils.empty() because phone can be empty
        if (dbUser.phone == null) {
            getViewState().checkPermissionAndGetPhoneNumber();
            return;
        }
        getViewState().setCurrentPhoneNumber(dbUser.phone);
        getViewState().onHideProgress();
    }

    public void onBack() {
        router.exit();
    }

    public void getPhoneNumberLocal(String currentPhoneNumber) {
        currentUser.phone = currentPhoneNumber;
        getViewState().setCurrentPhoneNumber(currentPhoneNumber);
    }

    public void changePhoneNumber(String phoneNumber) {
        addToDisposable(
                userService.usersMyUpdate(getUserUpdateRequestBodyPhone(phoneNumber))
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .map(UserConverter::convertToDatabaseUser)
                        .doOnNext(updateUser())
                        .subscribe(this::onSuccessUserUpdate,
                                this::onErrorUserUpdate));
    }

    private void onSuccessUserUpdate(DBUser dbUser) {
        getViewState().onShowSuccessUserUpdate();
    }

    @NonNull
    protected UserUpdateRequestBody getUserUpdateRequestBodyPhone(String phoneNumber) {
        return new UserUpdateRequestBody.Builder()
                .phone(getRequestBodyParams(phoneNumber))
                .build();
    }

    @Nullable
    private RequestBody getRequestBodyParams(String inputParams) {
        return inputParams != null
                ? RequestBody.create(MediaType.parse("multipart/form-data"), inputParams)
                : null;
    }

    private void onErrorUserUpdate(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    @NonNull
    private Consumer<DBUser> updateUser() {
        return user -> userRepository.updateUsers(user);
    }

    public void changePhoneEnable(String newPhoneNumber) {
        if (currentUser == null) {
            return;
        }
        getViewState().onChangePhoneButtonEnable(currentUser.phone == null || currentUser.phone.equals(newPhoneNumber));

    }
}
