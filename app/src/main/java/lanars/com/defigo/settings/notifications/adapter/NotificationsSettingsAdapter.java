package lanars.com.defigo.settings.notifications.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.NotificationModel;

public class NotificationsSettingsAdapter extends RecyclerView.Adapter<NotificationsSettingsAdapter.ViewHolder> {

    private List<NotificationModel> notificationModels;

    public NotificationsSettingsAdapter(List<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifications_settings, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(v -> {
             int position = viewHolder.getAdapterPosition();
             if(position!= RecyclerView.NO_POSITION){
                 notificationModels.get(position).setSelected(!notificationModels.get(position).isSelected());
                 notifyItemChanged(position);
             }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationModel notificationModel = notificationModels.get(position);
        holder.tvName.setText(notificationModel.getName());
        holder.imgSelect.setVisibility(notificationModel.isSelected() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imSelect)
        ImageView imgSelect;

        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public List<NotificationModel> getNotificationModels() {
        return notificationModels;
    }
}
