package lanars.com.defigo.settings.language;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.LanguageModel;
import lanars.com.defigo.common.repository.user.IDataRepository;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class LanguageSettingsPresenter extends BasePresenter<LanguageSeetingsView> {

    @Inject
    Router router;

    @Inject
    IDataRepository dataRepository;

    public LanguageSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    void onBack() {
        router.exit();
    }

    public void getLanguages() {
        dataRepository.getLanguages()
                .subscribe(this::onSuccessGetLanguages, this::onErrorGetLanguages);

    }

    private void onErrorGetLanguages(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessGetLanguages(List<LanguageModel> languageModels) {
        getViewState().setLanguages(languageModels);
    }
}
