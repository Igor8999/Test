package lanars.com.defigo.settings.sound;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.SoundModel;
import lanars.com.defigo.settings.sound.adapter.SoundSettingsAdapter;

public interface SoundSettingView extends BaseMvpView {

    void setSoundsInView(List<SoundModel> notificationModels, SoundSettingsAdapter.OnItemClick onItemClick);

    void notifyDataChanges();

    void moveBack();

    void requestExternalStorageWritePermission();

    void showPermissionError();
}
