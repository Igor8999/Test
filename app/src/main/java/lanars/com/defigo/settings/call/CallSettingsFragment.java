package lanars.com.defigo.settings.call;

import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.ui.alert.Alert;

public class CallSettingsFragment extends BaseSettingsFragment implements CallSettingsView {

    @InjectPresenter
    CallSettingsPresenter callSettingsPresenter;

    @BindView(R.id.swEnableVideo)
    SwitchCompat swEnableVideo;

    @Override
    protected String getTitle() {
        return getString(R.string.call_settings);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.call_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {

    }

    @Override
    public void handleBackClick() {
        callSettingsPresenter.onBack();
    }

    @Override
    public void onSuccessUpdate(int successTextResId) {
        Alert.create(getActivity())
                .setDuration(2500)
                .setText(successTextResId)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @OnClick(R.id.llContainer)
    public void onContainerClick(View view) {
        callSettingsPresenter.onSwitchStatusChanged(swEnableVideo.isChecked());
    }

    @Override
    public void setVideoEnabled(boolean isEnabled) {
        swEnableVideo.setChecked(isEnabled);
    }
}
