package lanars.com.defigo.settings.rfid;

import android.os.Bundle;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.network.service.IRfidService;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.settings.pincode.ChangePinCodeFragment;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class RfidCardSettingsPresenter extends BasePresenter<RfidCardSettingsView> {

    @Inject
    Router router;

    @Inject
    IRfidService rfidService;

    @Inject
    IAppPreferences appPreferences;

    private List<RfidModel> rfidModelList = new ArrayList<>();

    public RfidCardSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void goToChangePinCode() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.TYPE_PIN, ChangePinCodeFragment.TYPE_RFID_PIN);
        if (rfidModelList.size() > 0) {
            bundle.putInt(ChangePinCodeFragment.KEY_RFID_ID, rfidModelList.get(0).getId());
        }
        router.navigateTo(RouteScreenName.CHANGE_PIN, bundle);
    }

    @Override
    public void attachView(RfidCardSettingsView view) {
        super.attachView(view);
        getRfidUsers();
    }

    public void getRfidUsers() {
        addToDisposable(rfidService.getRfidList()
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doAfterTerminate(() -> getViewState().onHideProgress())
                .doOnNext(rfidModelList -> {
                    if (rfidModelList.size() > 0) {
                        String pin = rfidModelList.get(0).getPin();
                        if (!TextUtils.isEmpty(pin)) {
                            getViewState().setChangePinLabelText(R.string.change_pin_label);
                            getViewState().setDeletePinVisibility(true);
                        }
                    }
                })
                .subscribe(this::onGetSuccessUsers, this::onError));
    }

    private void onGetSuccessUsers(List<RfidModel> rfidModelList) {
        this.rfidModelList = rfidModelList;
        getViewState().setUsers(rfidModelList);
    }

    public void deletePin() {
        if (rfidModelList.size() > 0) {
            rfidService
                    .deleteRfidPin(rfidModelList.get(0).getId())
                    .doOnNext(rfidModel -> appPreferences.savePin(null, ChangePinCodeFragment.TYPE_RFID_PIN))
                    .ignoreElements()
                    .subscribe(this::onDeletePinSuccess, this::onError);
        }
    }

    private void onDeletePinSuccess() {
        getViewState().setChangePinLabelText(R.string.setup_pin);
        getViewState().setDeletePinVisibility(false);
    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void handleItemClick(int position) {
        RfidModel rfidModel = rfidModelList.get(position);

        addToDisposable(
                rfidService
                        .changeRfidStatus(rfidModel.getId(), rfidModel.isDisabled())
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .subscribe(this::changeRfidSuccess, this::onError)
        );
    }

    private void changeRfidSuccess(RfidModel rfidModel) {
        for (RfidModel model : rfidModelList) {
            if (model.getId() == rfidModel.getId()) {
                model.setDisabled(rfidModel.isDisabled());
            }
        }
        getViewState().notifyDataChanged();
        getViewState().onSuccessUpdate(R.string.rfid_status_updated);
    }

    public void handleOnEditClick(int position) {
        RfidModel rfidModel = rfidModelList.get(position);

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.CARD, rfidModel);

        router.navigateTo(RouteScreenName.EDIT_CARDS, bundle);
    }
}
