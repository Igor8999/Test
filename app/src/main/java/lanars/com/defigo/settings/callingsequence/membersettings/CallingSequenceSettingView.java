package lanars.com.defigo.settings.callingsequence.membersettings;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.SequenceUser;

public interface CallingSequenceSettingView extends BaseMvpView {

    void setCallingSequenceMembers(List<SequenceUser> sequenceMembers);
}
