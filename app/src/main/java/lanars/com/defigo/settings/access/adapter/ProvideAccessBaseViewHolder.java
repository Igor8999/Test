package lanars.com.defigo.settings.access.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;

public class ProvideAccessBaseViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imgUser)
    ImageView imgUser;

    @BindView(R.id.tvNameUser)
    TextView tvNameUser;

    public ProvideAccessBaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
