package lanars.com.defigo.settings.actions.opendoors;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.settings.mutemode.MuteModeDaysFragment;
import lanars.com.defigo.settings.ui.DoorbellDataPicker;

public class OpenDoorsFragment extends BaseSettingsFragment implements OpenDoorsView {

    @InjectPresenter
    OpenDoorsPresenter openDoorsPresenter;

    @ProvidePresenter
    public OpenDoorsPresenter createOpenDoorsPresenter() {
        return new OpenDoorsPresenter(typeOpenDoors);
    }

    @BindView(R.id.llMuteMode)
    LinearLayout llMuteMode;

    @BindView(R.id.doorbellDataPicker)
    DoorbellDataPicker doorbellDataPicker;

    @BindView(R.id.tvChangePin)
    TextView tvChangePin;

    @BindView(R.id.swMuteMode)
    SwitchCompat swMuteMode;

    private String typeOpenDoors;

    public static OpenDoorsFragment newInstance(Bundle bundle) {
        OpenDoorsFragment fragment = new OpenDoorsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        typeOpenDoors = getArguments().getString(AppConst.OPEN_DOORS_TYPE);
        super.onCreate(savedInstanceState);

    }

    @Override
    protected String getTitle() {
        return getOpenDoorsTitle();
    }

    private String getOpenDoorsTitle() {
        switch (typeOpenDoors) {
            case MuteModeDaysFragment.MUTE_DAYS_OPEN_INSTANTLY:
                return getString(R.string.open_doors_instantly);
            case MuteModeDaysFragment.MUTE_DAYS_OPEN_ON_PIN:
                return getString(R.string.open_doors_with_pin);
            default:
                return "";
        }
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.open_doors_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        tvChangePin.setVisibility(typeOpenDoors.equals(MuteModeDaysFragment.MUTE_DAYS_OPEN_INSTANTLY) ? View.GONE : View.VISIBLE);
        openDoorsPresenter.addToDisposable(RxCompoundButton.checkedChanges(swMuteMode)
                .subscribe(this::checkMuteMode));
        doorbellDataPicker.setOnClickDoorbellDayOfWeek(this::goToMuteDaysOfWeekScreen);
        doorbellDataPicker.setOnDoorbellTimeSetListener((view1, hourOfDay, minute, isSelectFrom) -> openDoorsPresenter.setTimeInView(isSelectFrom, hourOfDay, minute));
    }

    private void checkMuteMode(Boolean isChecked) {
        llMuteMode.setVisibility(isChecked ? View.VISIBLE : View.GONE);
    }

    void goToMuteDaysOfWeekScreen() {
        openDoorsPresenter.goToMuteDaysOfWeekScreen(typeOpenDoors);
    }

    @OnClick(R.id.tvChangePin)
    void goToChangePin() {
        openDoorsPresenter.goToChangePin();
    }

    @OnClick(R.id.llSchedule)
    void muteModeChange() {
        swMuteMode.setChecked(!swMuteMode.isChecked());
    }

    @OnClick(R.id.btnSubmit)
    void submit() {
        openDoorsPresenter.submit(swMuteMode.isChecked(),
                typeOpenDoors,
                doorbellDataPicker.getStartTimeStr(),
                doorbellDataPicker.getEndTimeStr());
    }

    @Override
    public void handleBackClick() {
        openDoorsPresenter.onBack();
    }

    @Override
    public void setActionsInView(ActionsOptionsModel actionsOptionsModel) {
        swMuteMode.setChecked(actionsOptionsModel.getIsActive() != null ? actionsOptionsModel.getIsActive() : false);
        if (actionsOptionsModel.getDaysOfWeek() != null && !actionsOptionsModel.getDaysOfWeek().isEmpty()) {
            openDoorsPresenter.getMuteDaysFromIndexes(actionsOptionsModel.getDaysOfWeek());
        }
        if (!TextUtils.isEmpty(actionsOptionsModel.getEndTime())) {
            doorbellDataPicker.setEndTime(actionsOptionsModel.getEndTime().substring(0, 5));
        }

        if (!TextUtils.isEmpty(actionsOptionsModel.getStartTime())) {
            doorbellDataPicker.setStartTime(actionsOptionsModel.getStartTime().substring(0, 5));
        }
    }

    @Override
    public void successUpdateRulesAction(StatusModel statusModel) {
        Alert.create(getActivity())
                .setText(R.string.success_update_action)
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void setMuteDaysText(String muteDaysStr) {
        doorbellDataPicker.setMuteDaysText(muteDaysStr);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setTimeFrom(int hourOfDay, int minute) {
        doorbellDataPicker.setStartTime(String.format("%02d:%02d", hourOfDay, minute));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setTimeTo(int hourOfDay, int minute) {
        doorbellDataPicker.setEndTime(String.format("%02d:%02d", hourOfDay, minute));
    }

    @Override
    public void onShowLoadingProgress() {

    }

}
