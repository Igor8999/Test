package lanars.com.defigo.settings.layout;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;

public interface LayoutSettingsView extends BaseMvpView {

    @StateStrategyType(SkipStrategy.class)
    void showPickerPhotoDialog();

    @StateStrategyType(SkipStrategy.class)
    void changeLayoutSettingsView(boolean isOwner);

    void showOrHideLayoutSettingsView(boolean isOwner);

    void getImageFromStorageUpload();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void uploadImageView(String nameImageFile);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void uploadImageUrl(String imageUrl);

    @StateStrategyType(SkipStrategy.class)
    void updateRoomSuccess(UpdateRoomModel updateRoomModel);

    void showName(String name);

    void setLayoutTitle(String title);

    void setLayoutTypeText(String title);

    void showDescription(String description);

    void setEmptyImageText(String string);

    void showMessage(String string);

    //test
    void showLayoutSettings(RoomLayoutModel roomLayoutModel, boolean isIndividual);

    void setImageVisibility(boolean isImageEnabled);
}
