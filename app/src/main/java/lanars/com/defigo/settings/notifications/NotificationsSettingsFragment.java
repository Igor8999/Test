package lanars.com.defigo.settings.notifications;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.NotificationModel;
import lanars.com.defigo.settings.notifications.adapter.NotificationsSettingsAdapter;

public class NotificationsSettingsFragment extends BaseSettingsFragment implements NotificationsSettingsView {

    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    @InjectPresenter
    NotificationsSettingsPresenter notificationsSettingsPresenter;

    private NotificationsSettingsAdapter adapter;

    @Override
    protected String getTitle() {
        return getString(R.string.notifications_settings);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.notifications_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        notificationsSettingsPresenter.getNotificationsSettings();
    }

    @Override
    public void handleBackClick() {
        notificationsSettingsPresenter.onBack();
    }

    @Override
    public void getNotificationsSettingsInView(List<NotificationModel> notificationModels) {
        adapter = new NotificationsSettingsAdapter(notificationModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvNotifications.setLayoutManager(linearLayoutManager);
        rvNotifications.setAdapter(adapter);
        rvNotifications.setHasFixedSize(true);
        rvNotifications.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }
}
