package lanars.com.defigo.settings.visibility.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;

public class VisibilityOptionsBaseViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvNameDoor)
    TextView tvNameUser;

    public VisibilityOptionsBaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
