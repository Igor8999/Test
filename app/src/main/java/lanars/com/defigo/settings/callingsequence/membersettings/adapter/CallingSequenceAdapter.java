package lanars.com.defigo.settings.callingsequence.membersettings.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.SequenceUser;
import lanars.com.defigo.common.ui.listener.OnItemCallingSimultaneousClickListener;
import lanars.com.defigo.common.ui.listener.OnItemClickCallingSequenceListener;
import lanars.com.defigo.settings.callingsequence.membersettings.CallingSequenceSettingsFragment;

public class CallingSequenceAdapter extends RecyclerView.Adapter<CallingSequenceBaseViewHolder> {

    private List<SequenceUser> sequenceUsers;
    private OnItemClickCallingSequenceListener onItemClickCallingSequenceListener;
    private OnItemCallingSimultaneousClickListener onItemCallingSimultaneousClickListener;

    public CallingSequenceAdapter(List<SequenceUser> sequenceUsers,
                                  OnItemClickCallingSequenceListener onItemClickCallingSequanceListener,
                                  OnItemCallingSimultaneousClickListener onItemCallingSimultaniusClickListener) {
        this.sequenceUsers = sequenceUsers;
        this.onItemClickCallingSequenceListener = onItemClickCallingSequanceListener;
        this.onItemCallingSimultaneousClickListener = onItemCallingSimultaniusClickListener;
    }

    public CallingSequenceAdapter(List<SequenceUser> sequenceUsers) {
        this.sequenceUsers = sequenceUsers;
    }

    @Override
    public CallingSequenceBaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {

            case CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SEQUENTIAL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sequintial_call, parent, false);
                CallingSequenceViewHolder sequenceViewHolder = new CallingSequenceViewHolder(view);
                setOnClickCallingSequenceListener(sequenceViewHolder);
                return sequenceViewHolder;

            case CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SIMULTANEOUS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sequintial_call, parent, false);
                CallingSimultaneousViewHolder simultaneousViewHolder = new CallingSimultaneousViewHolder(view);
                setOnClickCallingSimultaneousListener(simultaneousViewHolder);
                return simultaneousViewHolder;
            default:
                return null;
        }
    }

    private void setOnClickCallingSimultaneousListener(CallingSimultaneousViewHolder simultaneousViewHolder) {
        simultaneousViewHolder.itemView.setOnClickListener(v -> {
            int position = simultaneousViewHolder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onItemCallingSimultaneousClickListener.onItemClickSimultaneous(position);
            }
        });
    }

    private void setOnClickCallingSequenceListener(CallingSequenceViewHolder sequenceViewHolder) {
        sequenceViewHolder.itemView.setOnClickListener(v -> {
            int position = sequenceViewHolder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onItemClickCallingSequenceListener.onItemClickSequence(position);
            }
        });
    }

    @Override
    public void onBindViewHolder(CallingSequenceBaseViewHolder holder, int position) {
        SequenceUser sequenceUser = sequenceUsers.get(position);
        holder.tvNameUser.setText(sequenceUser.getName());
        int viewType = holder.getItemViewType();
        switch (viewType) {
            case CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SEQUENTIAL:
                if (sequenceUser.isCallEnabled() || sequenceUser.getIndexCall() != 0) {
                    holder.btnStatusSequentialCall.setBackground(holder.itemView.getResources().getDrawable(R.drawable.round_btn_background_select));
                    holder.btnStatusSequentialCall.setTextColor(holder.itemView.getResources().getColor(android.R.color.white));
                    holder.btnStatusSequentialCall.setText(String.valueOf(sequenceUser.getIndexCall()));
                } else {
                    holder.btnStatusSequentialCall.setBackground(holder.itemView.getResources().getDrawable(R.drawable.round_btn_background_unselect));
                }
                break;
            case CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SIMULTANEOUS:
                if (sequenceUser.isCallEnabled()) {
                    holder.btnStatusSequentialCall.setBackground(holder.itemView.getResources().getDrawable(R.drawable.checkmark_2));
                } else {
                    holder.btnStatusSequentialCall.setBackground(holder.itemView.getResources().getDrawable(R.drawable.round_btn_background_unselect));
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return sequenceUsers.size();
    }

    @Override
    public int getItemViewType(int position) {
        SequenceUser sequenceUser = sequenceUsers.get(position);
        if (sequenceUser.isSequenceType()) {
            return CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SEQUENTIAL;
        }
        return CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SIMULTANEOUS;
    }

    public List<SequenceUser> getSequenceUsers() {
        return sequenceUsers;
    }
}
