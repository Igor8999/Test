package lanars.com.defigo.settings.pincode;

import android.support.annotation.StringRes;

import lanars.com.defigo.common.base.BaseMvpView;

public interface ChangePinView extends BaseMvpView {

    void showCurrentPin(String pin);

    void savePinSuccessfully();

    void onShowError(@StringRes int textResId);

}
