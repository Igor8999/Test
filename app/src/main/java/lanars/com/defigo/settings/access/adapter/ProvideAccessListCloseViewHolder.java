package lanars.com.defigo.settings.access.adapter;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import ru.rambler.libs.swipe_layout.SwipeLayout;

public class ProvideAccessListCloseViewHolder extends ProvideAccessBaseViewHolder {

    @BindView(R.id.tvNoProvideAccess)
    TextView tvNoProvideAccess;

    @BindView(R.id.swLayout)
    SwipeLayout swLayout;

    @BindView(R.id.btnDelete)
    ImageButton btnDelete;

    public ProvideAccessListCloseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}
