package lanars.com.defigo.settings.callingsequence;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.settings.callingsequence.membersettings.CallingSequenceSettingsFragment;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class CallingSequenceMenuPresenter extends BasePresenter<CallingSequenceMenuView> {

    @Inject
    Router router;

    public CallingSequenceMenuPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void goToSimultaneousCall() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.SEQUENCE_TYPE, CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SIMULTANEOUS);
        router.navigateTo(RouteScreenName.CALLING_SEQUENCE_SETTINGS, bundle);
    }

    public void goToCallSequence() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.SEQUENCE_TYPE, CallingSequenceSettingsFragment.TYPE_CALLING_SEQUENCE_SEQUENTIAL);
        router.navigateTo(RouteScreenName.CALLING_SEQUENCE_SETTINGS, bundle);
    }
}
