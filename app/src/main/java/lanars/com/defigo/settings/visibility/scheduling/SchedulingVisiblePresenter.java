package lanars.com.defigo.settings.visibility.scheduling;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.network.service.IRoomService;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IDataRepository;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.settings.mutemode.MuteModeDaysFragment;
import okhttp3.ResponseBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class SchedulingVisiblePresenter extends BasePresenter<SchedulingVisibleView> {

    @Inject
    Router router;

    @Inject
    IDataRepository dataRepository;

    private boolean isReapetedAttach;

    @Inject
    IUserService userService;

    @Inject
    IRoomService roomService;

    @Inject
    IAppPreferences appPreferences;

    private VisibilityDoorModel visibilityDoorModel;

    @Override
    public void attachView(SchedulingVisibleView view) {
        super.attachView(view);
        if (isReapetedAttach) {
            getSelectedMuteDays();
        }
        isReapetedAttach = true;
    }

    public SchedulingVisiblePresenter(VisibilityDoorModel visibilityDoorModel) {
        this.visibilityDoorModel = visibilityDoorModel;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        setShowScheduleSettings();
    }

    private void setShowScheduleSettings() {
        getViewState().setShowScheduleSettings(visibilityDoorModel);
    }

    void onBack() {
        router.exit();
    }

    public void goToMuteDaysOfWeek() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.KEY_MUTE_DAYS, MuteModeDaysFragment.MUTE_DAYS_VISIBILITY);
        router.navigateTo(RouteScreenName.MUTE_MODE, bundle);
    }

    public void getSelectedMuteDays() {
        addToDisposable(dataRepository.getAllDaysOfWeek(MuteModeDaysFragment.MUTE_DAYS_VISIBILITY)
                .subscribe(this::setMuteDaysInView, this::onErrorGetDays));
    }

    private void onErrorGetDays(Throwable throwable) {

    }

    private void setMuteDaysInView(List<DayOfWeek> dayOfWeeks) {
        getViewState().setMuteDaysText(getStringDaysOfWeeks(dayOfWeeks));
    }

    @NonNull
    private String getStringDaysOfWeeks(List<DayOfWeek> dayOfWeeks) {
        StringBuilder stringBuilder = new StringBuilder();
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            if (dayOfWeek.isSelected()) {
                stringBuilder.append(dayOfWeek.getName().substring(0, 3))
                        .append("  ");
            }
        }
        return stringBuilder.toString();
    }

    public void setMuteMode(boolean isEnabled, String fromTime, String toTime, long doorbellId, boolean isSingleButton) {
        Map<String, Object> params = new HashMap<>();
        params.put("disabled", !isEnabled);
        params.put("endTime", toTime);
        params.put("startTime", fromTime);
        params.put("daysOfWeek", getArrayMuteDays());
        addToDisposable(getChangeVisibleObservable(doorbellId, isSingleButton, params)
                .subscribe(
                        this::onSuccessChangeVisible,
                        this::onErrorChangeVisible));
    }

    private void onSuccessChangeVisible(ResponseBody responseBody) {
        getViewState().onShowSuccessUserUpdate();
    }

    private Observable<ResponseBody> getChangeVisibleObservable(long doorbellId, boolean isSingleButton, Map<String, Object> params) {
        return isSingleButton ?
                roomService.changeVisibilityOptionsFamily(params, doorbellId) :
                userService.changeVisibilityOptionsIndividual(params, doorbellId);
    }

    private void onErrorChangeVisible(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private Object[] getArrayMuteDays() {
        List<Integer> dayIndexes = new ArrayList<>();
        for (DayOfWeek dayOfWeek : appPreferences.getMuteDays(MuteModeDaysFragment.MUTE_DAYS_VISIBILITY)) {
            if (dayOfWeek.isSelected()) {
                dayIndexes.add(dayOfWeek.getIndex());
            }
        }
        return dayIndexes.toArray();
    }

    public void setTimeInView(boolean isSelectFrom, int hourOfDay, int minute) {
        if (isSelectFrom) {
            getViewState().setTimeFrom(hourOfDay, minute);
            return;
        }
        getViewState().setTimeTo(hourOfDay, minute);
    }

    public void getMuteDaysFromIndexes(List<Integer> dayMuteIndexes) {
        getViewState().setMuteDaysText(getMuteDaysFromIndexesStr(dayMuteIndexes));
    }

    private String getMuteDaysFromIndexesStr(List<Integer> dayMuteIndexes) {
        StringBuilder stringBuilder = new StringBuilder();
        //todo need refactor
        List<DayOfWeek> listDays = dataRepository.getAllDaysOfWeek(MuteModeDaysFragment.MUTE_DAYS_VISIBILITY)
                .toList()
                .blockingGet()
                .get(0);
        for (int i = 0; i < listDays.size(); i++) {
            if (dayMuteIndexes.contains(listDays.get(i).getIndex())) {
                listDays.get(i).setSelected(true);
                stringBuilder.append(listDays.get(i).getName().substring(0, 3))
                        .append("  ");
            } else {
                listDays.get(i).setSelected(false);
            }
        }
        saveSelectedDays(listDays);
        return stringBuilder.toString();
    }

    private void saveSelectedDays(List<DayOfWeek> listDays) {
        dataRepository.saveDayOfWeeks(listDays, MuteModeDaysFragment.MUTE_DAYS_VISIBILITY);
    }

}
