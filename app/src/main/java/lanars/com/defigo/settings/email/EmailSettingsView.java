package lanars.com.defigo.settings.email;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;

public interface EmailSettingsView extends BaseMvpView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showUserEmail(String email);

    void onShowSuccessUserUpdate();

    void onChangeEmailButtonEnable(boolean isEnabled);

    void showStatusVerified(boolean isVerified);

    void enterCorrectEmailAlert();
}
