package lanars.com.defigo.settings.callingsequence.membersettings;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.SequenceUser;
import lanars.com.defigo.common.ui.listener.OnItemCallingSimultaneousClickListener;
import lanars.com.defigo.common.ui.listener.OnItemClickCallingSequenceListener;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.settings.callingsequence.membersettings.adapter.CallingSequenceAdapter;

public class CallingSequenceSettingsFragment extends BaseSettingsFragment implements CallingSequenceSettingView, OnItemCallingSimultaneousClickListener, OnItemClickCallingSequenceListener {

    public static final int TYPE_CALLING_SEQUENCE_SIMULTANEOUS = 0;
    public static final int TYPE_CALLING_SEQUENCE_SEQUENTIAL = 1;

    private int typeCallingInstance;

    @BindView(R.id.rvSequenceMembers)
    RecyclerView rvSequenceMembers;

    @BindView(R.id.tvTitleSequence)
    TextView tvSequenceTitle;

    private CallingSequenceAdapter adapter;

    @InjectPresenter
    CallingSequenceSettingsPresenter callingSequenceSettingsPresenter;

    @ProvidePresenter
    public CallingSequenceSettingsPresenter createCallingSequencePresenter() {
        return new CallingSequenceSettingsPresenter(typeCallingInstance);
    }

    public static CallingSequenceSettingsFragment newInstance(Bundle bundle) {
        CallingSequenceSettingsFragment fragment = new CallingSequenceSettingsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        typeCallingInstance = getArguments().getInt(AppConst.SEQUENCE_TYPE);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getTitle() {
        return getSequenceTitle();
    }

    private String getSequenceTitle() {
        return typeCallingInstance == TYPE_CALLING_SEQUENCE_SIMULTANEOUS ? getString(R.string.simultaneous_call) : getString(R.string.sequential_call);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.calling_sequence_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        imgBack.setVisibility(View.GONE);
        imgClose.setVisibility(View.VISIBLE);
        callingSequenceSettingsPresenter.getSequenceUsers(typeCallingInstance);
        tvSequenceTitle.setText(typeCallingInstance == TYPE_CALLING_SEQUENCE_SIMULTANEOUS ? getString(R.string.simultaneous_title) : getString(R.string.sequence_title));
    }

    @Override
    public void handleBackClick() {
        //nothing
    }

    @OnClick(R.id.imgClose)
    void onBack() {
        callingSequenceSettingsPresenter.onBack();
    }

    @Override
    public void setCallingSequenceMembers(List<SequenceUser> sequenceMembers) {
        adapter = new CallingSequenceAdapter(sequenceMembers, this, this);
        rvSequenceMembers.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvSequenceMembers.setLayoutManager(linearLayoutManager);
        rvSequenceMembers.setHasFixedSize(true);
        rvSequenceMembers.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onItemClickSimultaneous(int position) {
        adapter.getSequenceUsers().get(position).setCallEnabled(!adapter.getSequenceUsers().get(position).isCallEnabled());
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onItemClickSequence(int position) {
        updateOrderSequenceUsers(position);
    }

    private void updateOrderSequenceUsers(int position) {
        SequenceUser currentSequenceUser = adapter.getSequenceUsers().get(position);
        if (currentSequenceUser.getIndexCall() == 0) {
            currentSequenceUser.setIndexCall(getLastIndexInOrderSequenceMembers() + 1);
            adapter.notifyDataSetChanged();
        } else {
            decreaseIndexCall(currentSequenceUser.getIndexCall());
            currentSequenceUser.setIndexCall(0);
        }
    }

    private int getLastIndexInOrderSequenceMembers() {
        int index = 0;
        for (SequenceUser sequenceUser : adapter.getSequenceUsers()) {
            if (sequenceUser.getIndexCall() > index) {
                index = sequenceUser.getIndexCall();
            }
        }
        return index;
    }

    private void decreaseIndexCall(int indexCall) {
        for (SequenceUser sequenceUser : adapter.getSequenceUsers()) {
            if (sequenceUser.getIndexCall() != 0 && sequenceUser.getIndexCall() > indexCall) {
                sequenceUser.setIndexCall(sequenceUser.getIndexCall() - 1);
            }
        }
        adapter.notifyDataSetChanged();
    }

}
