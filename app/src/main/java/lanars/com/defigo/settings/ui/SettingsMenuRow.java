package lanars.com.defigo.settings.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import lanars.com.defigo.R;

public class SettingsMenuRow extends RelativeLayout {
    public SettingsMenuRow(Context context) {
        super(context);
    }

    public SettingsMenuRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public SettingsMenuRow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public SettingsMenuRow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SettingsMenuRow);
        Drawable drawable = typedArray.getDrawable(R.styleable.SettingsMenuRow_android_src);
        String rowText = typedArray.getString(R.styleable.SettingsMenuRow_android_text);
        typedArray.recycle();
        inflate(getContext(), R.layout.settings_menu_row, this);

        ImageView imageView = findViewById(R.id.ivIcon);
        TextView textView = findViewById(R.id.tvText);

        imageView.setImageDrawable(drawable);
        textView.setText(rowText);
    }
}
