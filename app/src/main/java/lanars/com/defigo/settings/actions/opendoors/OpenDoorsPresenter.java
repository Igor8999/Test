package lanars.com.defigo.settings.actions.opendoors;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.ActionsOptionsModel;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.repository.user.IDataRepository;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.settings.mutemode.MuteModeDaysFragment;
import lanars.com.defigo.settings.pincode.ChangePinCodeFragment;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class OpenDoorsPresenter extends BasePresenter<OpenDoorsView> {

    @Inject
    Router router;

    @Inject
    Context appContext;

    @Inject
    IUserRepository userRepository;

    @Inject
    IDataRepository dataRepository;

    @Inject
    IAppPreferences appPreferences;

    private String typeOpenDoors;
    private boolean isReapetedAttach;

    public OpenDoorsPresenter(String typeOpenDoors) {
        this.typeOpenDoors = typeOpenDoors;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void attachView(OpenDoorsView view) {
        super.attachView(view);
        if (isReapetedAttach) {
            getSelectedMuteDays(typeOpenDoors);
        }
        isReapetedAttach = true;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getActionsOptions(typeOpenDoors);
    }

    public void goToChangePin() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.TYPE_PIN, ChangePinCodeFragment.TYPE_ACTION_PIN);
        router.navigateTo(RouteScreenName.CHANGE_PIN, bundle);
    }

    public void goToMuteDaysOfWeekScreen(String typeOpenDoors) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.KEY_MUTE_DAYS, typeOpenDoors);
        router.navigateTo(RouteScreenName.MUTE_MODE, bundle);
    }

    public void submit(boolean isActive, String typeOpenDoors, String startTime, String endTime) {
        //todo change this in future after changed api
        if (typeOpenDoors.equals(MuteModeDaysFragment.MUTE_DAYS_OPEN_ON_PIN)) {
            if (TextUtils.isEmpty(appPreferences.getPin(ChangePinCodeFragment.TYPE_ACTION_PIN))) {
                getViewState().onShowError(appContext.getString(R.string.enter_pin));
                return;
            }
        }
        Map<String, Object> params = createUpdateActionsParams(isActive, typeOpenDoors, startTime, endTime);
        userRepository.getAllRooms()
                .toObservable()
                .map(this::getRoomId)
                .flatMap(integer -> updateActions(integer, params))
                .subscribe(this::getSuccessUpdateRulesAction, this::onErrorUpdateRulesAction);
    }

    @NonNull
    private Map<String, Object> createUpdateActionsParams(boolean isActive, String typeOpenDoors, String startTime, String endTime) {
        Map<String, Object> params = new HashMap<>();
        params.put("pin", appPreferences.getPin(ChangePinCodeFragment.TYPE_ACTION_PIN));
        params.put("isActive", isActive);
        params.put("type", typeOpenDoors);
        if (isActive) {
            params.put("endTime", endTime);
            params.put("startTime", startTime);
            params.put("daysOfWeek", getDayOfWeeksArray());
        }
        return params;
    }

    private Observable<StatusModel> updateActions(Integer integer, Map<String, Object> params) {
        return userRepository.updateRulesActions(integer, params);
    }

    @NonNull
    private Object[] getDayOfWeeksArray() {
        List<DayOfWeek> selectMuteDays = appPreferences.getMuteDays(typeOpenDoors);
        List<Integer> indexMuteDays = new ArrayList<>();
        if (selectMuteDays != null) {
            for (DayOfWeek dayOfWeek : selectMuteDays) {
                if (dayOfWeek.isSelected()) {
                    indexMuteDays.add(dayOfWeek.getIndex());
                }
            }
        }
        return indexMuteDays.toArray();
    }

    public void onBack() {
        router.exit();
    }

    private void onErrorUpdateRulesAction(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void getSuccessUpdateRulesAction(StatusModel statusModel) {
        getViewState().successUpdateRulesAction(statusModel);
    }

    public void getActionsOptions(String typeOpenDoors) {
        //todo tmp room id

        addToDisposable(userRepository.getAllRooms()
                .toObservable()
                .map(this::getRoomId)
                .flatMap(integer -> getActionsOptions(integer, typeOpenDoors))
                .subscribe(this::onGetActionsOptionsSuccess, this::onGetErrorOptionsScreen));

    }

    private Integer getRoomId(List<DBRoom> dbRooms) {
        return dbRooms.get(0).getId();
    }

    private Observable<ActionsOptionsModel> getActionsOptions(Integer roomId, String typeOpenDoors) {
        return userRepository.getActionsOptions(roomId, typeOpenDoors);
    }

    private void onGetActionsOptionsSuccess(ActionsOptionsModel actionsOptionsModel) {
        if (!TextUtils.isEmpty(actionsOptionsModel.getPin())) {
            appPreferences.savePin(actionsOptionsModel.getPin(), ChangePinCodeFragment.TYPE_ACTION_PIN);
        }
        getViewState().setActionsInView(actionsOptionsModel);
    }

    public void getSelectedMuteDays(String keySaveMuteDays) {
        addToDisposable(dataRepository.getAllDaysOfWeek(keySaveMuteDays)
                .subscribe(this::setMuteDaysInView, this::onErrorGetDays));
    }

    private void setMuteDaysInView(List<DayOfWeek> dayOfWeeks) {
        getViewState().setMuteDaysText(getStringDaysOfWeeks(dayOfWeeks));
    }

    @NonNull
    private String getStringDaysOfWeeks(List<DayOfWeek> dayOfWeeks) {
        StringBuilder stringBuilder = new StringBuilder();
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            if (dayOfWeek.isSelected()) {
                stringBuilder.append(dayOfWeek.getName().substring(0, 3))
                        .append("  ");
            }
        }
        return stringBuilder.toString();
    }

    //onErrors
    private void onErrorGetDays(Throwable throwable) {

    }

    private void onGetErrorOptionsScreen(Throwable throwable) {

    }

    public void setTimeInView(boolean isSelectFrom, int hourOfDay, int minute) {
        if (isSelectFrom) {
            getViewState().setTimeFrom(hourOfDay, minute);
            return;
        }
        getViewState().setTimeTo(hourOfDay, minute);
    }

    public void getMuteDaysFromIndexes(List<Integer> dayMuteIndexes) {
        getViewState().setMuteDaysText(getMuteDaysFromIndexesStr(dayMuteIndexes));
    }

    //todo need refactor
    private String getMuteDaysFromIndexesStr(List<Integer> dayMuteIndexes) {
        StringBuilder stringBuilder = new StringBuilder();
        List<DayOfWeek> listDays = dataRepository.getAllDaysOfWeek(typeOpenDoors)
                .toList()
                .blockingGet()
                .get(0);
        for (int i = 0; i < listDays.size(); i++) {
            if (dayMuteIndexes.contains(listDays.get(i).getIndex())) {
                listDays.get(i).setSelected(true);
                stringBuilder.append(listDays.get(i).getName().substring(0, 3))
                        .append("  ");
            } else {
                listDays.get(i).setSelected(false);
            }
        }
        saveSelectedDays(listDays);
        return stringBuilder.toString();
    }

    private void saveSelectedDays(List<DayOfWeek> listDays) {
        dataRepository.saveDayOfWeeks(listDays, typeOpenDoors);
    }
}
