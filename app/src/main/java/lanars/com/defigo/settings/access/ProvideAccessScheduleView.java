package lanars.com.defigo.settings.access;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.Schedule;

public interface ProvideAccessScheduleView extends BaseMvpView {

    void setTimeFrom(int hourOfDay, int minute);

    void setTimeTo(int hourOfDay, int minute);

    void setMuteDaysText(String stringDaysOfWeeks);

    void onSuccessProvideUser();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void setProvideSettings(Schedule schedule);
}
