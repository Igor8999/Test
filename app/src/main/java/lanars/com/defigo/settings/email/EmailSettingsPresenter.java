package lanars.com.defigo.settings.email;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBUser;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class EmailSettingsPresenter extends BasePresenter<EmailSettingsView> {

    @Inject
    Router router;

    @Inject
    IUserRepository userRepository;

    @Inject
    IUserService userService;

    @Inject
    IAppPreferences appPreferences;

    private DBUser currentUser;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getEmailUser();
    }

    @VisibleForTesting
    EmailSettingsPresenter(Router router, IUserRepository userRepository, IAppPreferences appPreferences, IUserService userService) {
        this.router = router;
        this.userRepository = userRepository;
        this.appPreferences = appPreferences;
        this.userService = userService;
    }

    public EmailSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    protected void getEmailUser() {
        getViewState().onShowProgress();

        addToDisposable(
                userRepository
                        .getUserById(appPreferences.getCurrentUserId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::showEmailUser, this::onGetErrorEmailUser));
    }

    private void onGetErrorEmailUser(Throwable throwable) {
        getViewState().onHideProgress();
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void showEmailUser(DBUser dbUser) {
        getViewState().onHideProgress();
        currentUser = dbUser;
        getViewState().showUserEmail(dbUser.email);
    }

    public void onBack() {
        router.exit();
    }

    public void changeEmail(String email) {
        if (!validateEmail(email)) {
            return;
        }
        UserUpdateRequestBody userUpdateRequestBody = new UserUpdateRequestBody.Builder()
                .email(getRequestBodyParams(email))
                .build();
        addToDisposable(
                userService.usersMyUpdate(userUpdateRequestBody)
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .map(UserConverter::convertToDatabaseUser)
                        .doOnNext(updateUser())
                        .ignoreElements()
                        .subscribe(this::onSuccessUserUpdate, this::onErrorUserUpdate));
    }

    private boolean validateEmail(String email) {
        if (TextUtils.isEmpty(email) || !email.contains("@")) {
            getViewState().enterCorrectEmailAlert();
            return false;
        }
        return true;
    }

    @Nullable
    private RequestBody getRequestBodyParams(String inputParams) {
        return inputParams != null
                ? RequestBody.create(MediaType.parse("multipart/form-data"), inputParams)
                : null;
    }

    private void onErrorUserUpdate(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessUserUpdate() {
        getViewState().onShowSuccessUserUpdate();
    }

    @NonNull
    private Consumer<DBUser> updateUser() {
        return user -> userRepository.updateUsers(user);
    }

    public void changeEmailEnable(String inputEmail) {
        if (currentUser != null && currentUser.email != null) {
            getViewState().onChangeEmailButtonEnable(currentUser.email.equals(inputEmail));
            return;
        }
        getViewState().onChangeEmailButtonEnable(false);
    }
}
