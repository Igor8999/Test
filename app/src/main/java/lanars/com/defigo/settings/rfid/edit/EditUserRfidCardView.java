package lanars.com.defigo.settings.rfid.edit;

import lanars.com.defigo.common.base.BaseMvpView;

public interface EditUserRfidCardView extends BaseMvpView {
    void setCloseIcon(boolean isClose);
}
