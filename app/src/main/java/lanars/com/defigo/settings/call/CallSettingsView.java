package lanars.com.defigo.settings.call;

import android.support.annotation.StringRes;

import lanars.com.defigo.common.base.BaseMvpView;

public interface CallSettingsView extends BaseMvpView {
    void setVideoEnabled(boolean isEnabled);

    void onSuccessUpdate(@StringRes int successTextResId);
}
