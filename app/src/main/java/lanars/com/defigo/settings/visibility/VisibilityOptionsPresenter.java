package lanars.com.defigo.settings.visibility;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.VisibilityDoorModel;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.AppConst;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class VisibilityOptionsPresenter extends BasePresenter<VisibilityOptionsView> {
    @Inject
    Router router;

    @Inject
    IUserRepository userRepository;

    public VisibilityOptionsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    void getAllVisibilityDoorbels() {
        addToDisposable(userRepository.getUsersDoorbellsVisibility()
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doAfterTerminate(() -> getViewState().onHideProgress())
                .subscribe(this::onSuccessGetVisibilityDoors, this::onErrorGetVisibilityDoors));
    }

    private void onErrorGetVisibilityDoors(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessGetVisibilityDoors(List<VisibilityDoorModel> visibilityDoorModels) {
        getViewState().setVisibilityDoors(visibilityDoorModels);
    }

    void onBack() {
        router.exit();
    }

    public void onItemClickVisibilityOptions(VisibilityDoorModel visibilityDoorModel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConst.DOORBEL_VISIBILITY, visibilityDoorModel);
        router.navigateTo(RouteScreenName.SHEDULING_VISIBILITY, bundle);
    }
}
