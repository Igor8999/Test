package lanars.com.defigo.settings.password;

import com.arellomobile.mvp.InjectViewState;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.service.IAuthService;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ChangePasswordPresenter extends BasePresenter<ChangePasswordView> {

    @Inject
    Router router;

    @Inject
    IAuthService authService;

    public ChangePasswordPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void submit(String currentPassword, String newPassword, String confirmNewPassword) {
        if (newPassword.length() < 6) {
            getViewState().showValidationError(R.string.password_short);
            return;
        }

        if (!newPassword.equals(confirmNewPassword)) {
            getViewState().showValidationError(R.string.new_confirm_password_not_equal);
            return;
        }

        if (currentPassword.equals(newPassword)) {
            getViewState().showValidationError(R.string.use_new_password);
            return;
        }

        getViewState().hideKeyboard();

        Map<String, String> params = new HashMap<>();
        params.put("oldPassword", currentPassword);
        params.put("newPassword", newPassword);

        addToDisposable(
                authService
                        .changePassword(params)
                        .ignoreElements()
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .subscribe(this::onSuccess, this::onError)
        );
    }

    private void onSuccess() {
        getViewState().onSuccessUpdate(R.string.success_update_password);
    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }


}
