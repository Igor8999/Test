package lanars.com.defigo.settings.profile.personal;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.FileUtils;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.database.model.DBUser;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class PersonalInfoPresenter extends BasePresenter<PersonalInfoView> {

    @Inject
    Router router;

    @Inject
    Context appContext;

    @Inject
    IUserRepository userRepository;

    @Inject
    IUserService userService;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IFileUtils fileUtils;

    private String nameImageFile;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().getCurrentImageProfileUpload();
        userRepository
                .getUserById(appPreferences.getCurrentUserId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showPersonalInfo);
    }

    private void showPersonalInfo(DBUser dbUser) {
        getViewState().showFirstName(dbUser.firstName);
        getViewState().showLastName(dbUser.lastName);
        getViewState().showUsername(dbUser.userName);
        getViewState().uploadPhoto(dbUser.imgUrl);
    }

    public PersonalInfoPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void showSelectImageDialog() {
        getViewState().showPickerPhotoDialog();
    }

    public void uploadImage(Sources sources) {
        addToDisposable(
                RxImagePicker.with(DefigoApplication.getInstance())
                        .requestImage(sources)
                        .doOnComplete(() -> getViewState().onShowProgress())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::cropSelectedImage, this::onErrorUpload));
    }

    private void cropSelectedImage(@NonNull Uri uri) {
        nameImageFile = fileUtils.getUniqueFileName(FileUtils.IMAGE_PROFILE);
        getViewState().onHideProgress();
        Bundle bundle = new Bundle();
        bundle.putParcelable("uri", uri);
        bundle.putString("nameImage", nameImageFile);
        router.navigateTo(RouteScreenName.CROP_FRAGMENT, bundle);
    }

    private void onErrorUpload(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void deleteSelectedPhoto() {
        File diskCacheDir = fileUtils.getDiskCacheDir(DefigoApplication.getInstance(), nameImageFile);
        if (diskCacheDir != null) {
            diskCacheDir.delete();
        }
        uploadImageView();
    }

    public void submit(String firstName, String lastName, String userName) {
        File image = getAvatarImage();
        RequestBody requestImageFile = image != null
                ? RequestBody.create(MediaType.parse("image/jpeg"), image)
                : null;
        MultipartBody.Part multipartBodyImage = requestImageFile != null
                ? MultipartBody.Part.createFormData("image", image.getName(), requestImageFile)
                : null;
        UserUpdateRequestBody userUpdateRequestBody = new UserUpdateRequestBody.Builder()
                .fileImage(multipartBodyImage)
                .userName(getRequestBodyParams(userName))
                .lName(getRequestBodyParams(lastName))
                .fName(getRequestBodyParams(firstName))
                .build();
        addToDisposable(userService.usersMyUpdate(userUpdateRequestBody)
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doAfterTerminate(() -> getViewState().onHideProgress())
                .map(UserConverter::convertToDatabaseUser)
                .doOnNext(updateUser())
                .subscribe(this::onSuccessUserUpdate, this::onErrorUserUpdate));
    }

    @Nullable
    private RequestBody getRequestBodyParams(String inputParams) {
        return !TextUtils.isEmpty(inputParams)
                ? RequestBody.create(MediaType.parse("multipart/form-data"), inputParams)
                : null;
    }

    private void onSuccessUserUpdate(DBUser dbUser) {
        getViewState().submitSuccess();
    }

    private Observable<UpdateRoomModel> updateInfoRoom(Integer roomId, MultipartBody.Part multipartBodyImage) {
        return userService.updateInfoRoom(roomId, multipartBodyImage, null, null);
    }

    private Integer getRoomId(List<DBRoom> dbRooms) {
        return dbRooms.get(0).getId();
    }

    //need add imgUrl room for the user
    private ObservableSource<?> getImageUrlUpdate(UpdateRoomModel updateRoomModel, Map<String, String> params) {
        params.put("imgUrl", updateRoomModel.getImgUrl());
        return Observable.just(updateRoomModel);
    }

    private void onErrorUserUpdate(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    @NonNull
    private Consumer<DBUser> updateUser() {
        return user -> userRepository.updateUsers(user);
    }

    public File getAvatarImage() {
        return fileUtils.getDiskCacheDir(DefigoApplication.getInstance(), nameImageFile);
    }

    void uploadImageView() {
        if (fileUtils.checkExistFile(appContext, nameImageFile)) {
            getViewState().uploadImageView(nameImageFile);
            return;
        }
        getViewState().uploadImageView(null);
    }

    @Override
    public void onDestroy() {
        deleteSelectedPhoto();
        super.onDestroy();
    }
}

