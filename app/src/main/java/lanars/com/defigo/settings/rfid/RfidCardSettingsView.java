package lanars.com.defigo.settings.rfid;

import android.support.annotation.StringRes;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.RfidModel;

public interface RfidCardSettingsView extends BaseMvpView {
    void setUsers(List<RfidModel> users);

    void setDeletePinVisibility(boolean isVisible);

    void setChangePinLabelText(@StringRes int labelTextResId);

    void onSuccessUpdate(@StringRes int textResId);

    void notifyDataChanged();
}
