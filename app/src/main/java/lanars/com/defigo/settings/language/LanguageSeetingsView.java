package lanars.com.defigo.settings.language;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.LanguageModel;

public interface LanguageSeetingsView extends BaseMvpView {

          void setLanguages(List<LanguageModel> languageModels);
}
