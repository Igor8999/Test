package lanars.com.defigo.settings.notifications;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.NotificationModel;

public interface NotificationsSettingsView extends BaseMvpView {

    void getNotificationsSettingsInView(List<NotificationModel> notificationModels);
}
