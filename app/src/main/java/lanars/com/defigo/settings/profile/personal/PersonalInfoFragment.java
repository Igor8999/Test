package lanars.com.defigo.settings.profile.personal;

import android.Manifest;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mlsdev.rximagepicker.Sources;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.ui.UploadPhotoView;
import lanars.com.defigo.common.ui.alert.Alert;

public class PersonalInfoFragment extends BaseSettingsFragment implements PersonalInfoView {

    @BindView(R.id.upload_view)
    UploadPhotoView uploadPhotoView;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.etFirstName)
    EditText etFirstName;

    @BindView(R.id.etLastName)
    EditText etLastName;

    @BindView(R.id.etUsername)
    EditText etUsername;

    @InjectPresenter
    PersonalInfoPresenter personalInfoPresenter;

    private RxPermissions rxPermissions;

    @Override
    protected String getTitle() {
        return getString(R.string.personal_info);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.personal_info_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
    }

    @Override
    public void handleBackClick() {
        personalInfoPresenter.onBack();
    }

    @OnClick(R.id.btnSubmit)
    void submit() {
        personalInfoPresenter.submit(
                etFirstName.getText().toString().trim(),
                etLastName.getText().toString().trim(),
                etUsername.getText().toString().trim());
    }

    @OnClick(R.id.upload_view)
    void uploadPhoto() {
        checkPermissionsAndGetImage();
    }

    private void checkPermissionsAndGetImage() {
        rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .subscribe(this::OnSuccessPermission,
                        this::onErrorPermission);
    }

    private void OnSuccessPermission(Boolean isAprove) {
        if (isAprove) {
            personalInfoPresenter.showSelectImageDialog();
        }
    }

    private void onErrorPermission(Throwable throwable) {

    }

    @Override
    public void showPickerPhotoDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.select_photo)
                .setPositiveButton(R.string.from_camera, (dialog, which) -> {
                    personalInfoPresenter.uploadImage(Sources.CAMERA);
                })
                .setNeutralButton(R.string.delete, (dialog, which) -> personalInfoPresenter.deleteSelectedPhoto())
                .setNegativeButton(R.string.from_gallery, (dialog, which) -> personalInfoPresenter.uploadImage(Sources.GALLERY))
                .create()
                .show();
    }

    @Override
    public void submitSuccess() {
        Alert.create(getActivity())
                .setDuration(2500)
                .setText(R.string.changes_saved)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @Override
    public void uploadImageView(String fileName) {
        uploadPhotoView.uploadImage(fileName);
    }

    @Override
    public void getCurrentImageProfileUpload() {
        personalInfoPresenter.uploadImageView();
    }

    @Override
    public void showFirstName(String name) {
        if (!TextUtils.isEmpty(name)) {
            etFirstName.setText(name);
            etFirstName.setSelection(name.length());
        }
    }

    @Override
    public void showLastName(String name) {
        if (!TextUtils.isEmpty(name)) {
            etLastName.setText(name);
            etLastName.setSelection(name.length());
        }
    }

    @Override
    public void showUsername(String name) {
        if (!TextUtils.isEmpty(name)) {
            etUsername.setText(name);
            etUsername.setSelection(name.length());
        }
    }

    @Override
    public void uploadPhoto(String imgUrl) {
        if (!TextUtils.isEmpty(imgUrl)) {
            uploadPhotoView.uploadImageUrl(imgUrl);
        }
    }
}
