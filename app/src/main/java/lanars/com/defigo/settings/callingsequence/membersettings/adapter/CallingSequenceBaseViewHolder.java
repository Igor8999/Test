package lanars.com.defigo.settings.callingsequence.membersettings.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;

public class CallingSequenceBaseViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.btnStatusSequentialCall)
    Button btnStatusSequentialCall;

    @BindView(R.id.tvNameUser)
    TextView tvNameUser;

    @BindView(R.id.imgUser)
    ImageView imgUser;

    public CallingSequenceBaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
