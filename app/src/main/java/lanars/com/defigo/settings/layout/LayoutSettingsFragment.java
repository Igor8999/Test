package lanars.com.defigo.settings.layout;

import android.Manifest;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;
import com.mlsdev.rximagepicker.Sources;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTouch;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import lanars.com.defigo.common.ui.UploadPhotoView;
import lanars.com.defigo.common.ui.alert.Alert;

public class LayoutSettingsFragment extends BaseSettingsFragment implements LayoutSettingsView {

    @BindView(R.id.tvLayoutName)
    TextView tvLayoutName;

    @BindView(R.id.tvTypeLayout)
    TextView tvTypeLayout;

    @BindView(R.id.tvInfoOwner)
    TextView tvInfoOwner;

    @BindView(R.id.layout_settings)
    LinearLayout layout_settings;

    @BindView(R.id.rgLayout)
    RadioGroup rgLayout;

    @BindView(R.id.upload_view)
    UploadPhotoView uploadPhotoView;

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etDescription)
    EditText etDescription;

    @BindView(R.id.rbFamily)
    RadioButton rbFamily;

    @InjectPresenter
    LayoutSettingsPresenter layoutSettingsPresenter;

    public static final int LAYOUT_FAMILY = 0;

    public static final int LAYOUT_INDIVIDUAL = 2;

    private int layoutType = LAYOUT_INDIVIDUAL;

    @Override
    protected String getTitle() {
        return getString(R.string.adjust_my_layout);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.layout_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        layoutSettingsPresenter.addToDisposable(
                RxRadioGroup.checkedChanges(rgLayout)
                        .skipInitialValue()
                        .subscribe(this::onSelectLayoutType));
    }

    private void onSelectLayoutType(Integer position) {
        RadioButton btn = rgLayout.findViewById(position);
        layoutType = rgLayout.indexOfChild(btn);
        layoutSettingsPresenter.getLayoutSettings(layoutType);
    }

    @OnClick(R.id.upload_view)
    void uploadImage() {
        checkPermissionsAndGetImage();
    }

    @OnClick(R.id.btnSubmit)
    void onSubmitButtonPress() {
        layoutSettingsPresenter.submit(layoutType, etName.getText().toString(),
                etDescription.getText().toString());
    }

    @Override
    public void handleBackClick() {
        layoutSettingsPresenter.onBack();
    }

    private void checkPermissionsAndGetImage() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .subscribe(this::onSuccessPermission, this::onErrorPermission);
    }

    private void onErrorPermission(Throwable throwable) {
        throwable.printStackTrace();
    }

    private void onSuccessPermission(Boolean isApprove) {
        if (isApprove) {
            layoutSettingsPresenter.showSelectImageDialog();
        } else {
            onShowError(getString(R.string.storage_permission_rationale));
        }
    }

    @Override
    public void showPickerPhotoDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.select_photo)
                .setPositiveButton(R.string.from_camera, (dialog, which) -> {
                    layoutSettingsPresenter.uploadImageFromSource(Sources.CAMERA);
                })
                .setNegativeButton(R.string.from_gallery, (dialog, which) -> layoutSettingsPresenter.uploadImageFromSource(Sources.GALLERY))
                .create()
                .show();
    }

    @Override
    public void changeLayoutSettingsView(boolean isOwner) {
        rgLayout.check(isOwner ? R.id.rbFamily : R.id.rbIndividual);
        layoutSettingsPresenter.getLayoutSettings(layoutType);
    }

    @Override
    public void showOrHideLayoutSettingsView(boolean isOwner) {
        layout_settings.setVisibility(isOwner ? View.VISIBLE : View.GONE);
        tvInfoOwner.setVisibility(isOwner ? View.GONE : View.VISIBLE);
        rbFamily.setEnabled(isOwner);
    }

    @OnTouch(R.id.etName)
    public boolean onEditTextClick(View view, MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_UP) {
            return false;
        }

        if (etName.getText().length() > 0) {
            return false;
        }

        view.postDelayed(this::showSelectionPopup, 400);
        return false;
    }

    boolean isEmpty;

    @OnTextChanged(value = R.id.etName, callback = OnTextChanged.Callback.BEFORE_TEXT_CHANGED)
    public void beforeEmailInput(Editable editable) {
        isEmpty = editable.length() == 0;
    }

    @OnTextChanged(value = R.id.etName, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterEmailInput(Editable editable) {
        if (!isEmpty && editable.length() == 0) {
            showSelectionPopup();
        }
    }

    private void showSelectionPopup() {
        PopupMenu popup = new PopupMenu(getContext(), etName, Gravity.TOP);
        populatePopupMenu(popup, layoutSettingsPresenter.provideMenuItems());
        popup.setOnMenuItemClickListener(item -> layoutSettingsPresenter.onNameItemClick(item.getItemId()));
        popup.show();
    }

    private void populatePopupMenu(PopupMenu popup, List<String> menusList) {
        for (int index = 0; index < menusList.size(); index++) {
            popup.getMenu().add(0, index, Menu.NONE, menusList.get(index));
        }
    }

    @Override
    public void getImageFromStorageUpload() {
        layoutSettingsPresenter.uploadStorageImageInView();
    }

    @Override
    public void uploadImageView(String nameImageFile) {
        uploadPhotoView.uploadImage(nameImageFile);
    }

    @Override
    public void uploadImageUrl(String imageUrl) {
        getActivity().runOnUiThread(() -> uploadPhotoView.uploadImageUrl(imageUrl));
    }

    @Override
    public void updateRoomSuccess(UpdateRoomModel updateRoomModel) {
        Alert.create(getActivity())
                .setText(R.string.success_update_room_info)
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void showName(String name) {
        if (!TextUtils.isEmpty(name)) {
            etName.setText(name);
            etName.setSelection(name.length());
        }
    }

    @Override
    public void setLayoutTitle(String title) {
        tvLayoutName.setText(title);
    }

    @Override
    public void setLayoutTypeText(String title) {
        tvTypeLayout.setText(title);
    }

    @Override
    public void showDescription(String description) {
        if (!TextUtils.isEmpty(description)) {
            etDescription.setText(description);
        }
    }

    @Override
    public void setEmptyImageText(String emptyText) {
        uploadPhotoView.setEmptyText(emptyText);
    }

    @Override
    public void showMessage(String string) {
        //
    }

    @Override
    public void showLayoutSettings(RoomLayoutModel roomLayoutModel, boolean isIndividual) {
        if (isIndividual) {
            if (!TextUtils.isEmpty(roomLayoutModel.getIndividualSettings().getHeader())) {
                etName.setText(roomLayoutModel.getIndividualSettings().getHeader());
                etName.setSelection(roomLayoutModel.getIndividualSettings().getHeader().length());
            }
        }
        if (!TextUtils.isEmpty(roomLayoutModel.getRoomSettings().getHeader())) {
            etDescription.setText(roomLayoutModel.getRoomSettings().getHeader());
        }
    }

    @Override
    public void setImageVisibility(boolean isImageEnabled) {
        uploadPhotoView.setVisibility(isImageEnabled
                ? View.VISIBLE
                : View.GONE
        );
    }
}
