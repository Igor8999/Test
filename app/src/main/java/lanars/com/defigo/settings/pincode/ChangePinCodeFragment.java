package lanars.com.defigo.settings.pincode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseFragment;
import lanars.com.defigo.common.utils.AppConst;

public class ChangePinCodeFragment extends BaseFragment implements ChangePinView {

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.btnChangePin)
    Button btnChangePin;

    @BindView(R.id.etPin)
    EditText etPin;

    @InjectPresenter
    ChangePinPresenter changePinPresenter;

    @ProvidePresenter
    public ChangePinPresenter provide() {
        int rfidId;

        if (getArguments() == null) {
            rfidId = -1;
        } else {
            rfidId = getArguments().getInt(KEY_RFID_ID, -1);
        }

        return new ChangePinPresenter(rfidId);
    }

    private String typePin;

    public static final String KEY_RFID_ID = "KEY_RFID_ID";
    public static final String TYPE_RFID_PIN = "rfid_pin";
    public static final String TYPE_ACTION_PIN = "action_pin";

    public static ChangePinCodeFragment newInstance(Bundle bundle) {
        ChangePinCodeFragment fragment = new ChangePinCodeFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        typePin = getArguments().getString(AppConst.TYPE_PIN);
        super.onCreate(savedInstanceState);
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.change_pincode_settings_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        changePinPresenter.getCurrentPin(typePin);
    }

    @OnClick(R.id.imgBack)
    void onBack() {
        changePinPresenter.onBack();
    }

    @OnClick(R.id.btnChangePin)
    public void handleChangePinPress() {
        changePinPresenter.savePin(etPin.getText().toString(), typePin);
    }

    @Override
    public void showCurrentPin(String pin) {
        etPin.setText(pin);
    }

    @Override
    public void savePinSuccessfully() {
        Toast.makeText(getContext(), R.string.pin_changed, Toast.LENGTH_LONG).show();
        changePinPresenter.onBack();
    }

    @Override
    public void onShowError(@StringRes int textResId) {
        onShowError(getString(textResId));
    }
}
