package lanars.com.defigo.settings.layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;

import com.arellomobile.mvp.InjectViewState;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.UpdateRoomModel;
import lanars.com.defigo.common.network.models.roomsettings.RoomLayoutModel;
import lanars.com.defigo.common.network.service.IRoomService;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.FileUtils;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import lanars.com.defigo.database.model.DBRoom;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class LayoutSettingsPresenter extends BasePresenter<LayoutSettingsView> {

    @Inject
    Router router;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IRoomService roomService;

    @Inject
    IUserService userService;

    @Inject
    IFileUtils fileUtils;

    @Inject
    Context appContext;

    @Inject
    IUserRepository userRepository;

    private RoomLayoutModel roomLayoutModel;

    private String nameImageFile;

    public LayoutSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    LayoutSettingsPresenter(Router router, IAppPreferences appPreferences, IRoomService roomService,
                            IUserService userService,
                            IUserRepository userRepository, IFileUtils fileUtils, Context context, RoomLayoutModel roomLayoutModel) {
        this.appPreferences = appPreferences;
        this.roomService = roomService;
        this.userService = userService;
        this.userRepository = userRepository;
        this.fileUtils = fileUtils;
        this.appContext = context;
        this.router = router;
        this.roomLayoutModel = roomLayoutModel;
    }

    @Override
    public void attachView(LayoutSettingsView view) {
        super.attachView(view);
        getViewState().getImageFromStorageUpload();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getCurrentRoomLayoutSettings();
    }

    @VisibleForTesting
    protected void getCurrentRoomLayoutSettings() {
        addToDisposable(
                userRepository.getAllRooms()
                        .toObservable()
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .map(this::getRoomId)
                        .flatMap(this::getLayoutSettingsUser)
                        .subscribe(this::getRoomLayoutSettingsSuccess,
                                this::getRoomLayoutSettingsError));
    }

    private ObservableSource<RoomLayoutModel> getLayoutSettingsUser(Integer integer) {
        return userRepository.getRoomLayoutSettingsUser(integer);
    }

    private void getRoomLayoutSettingsError(Throwable throwable) {
        getViewState().onHideProgress();
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void getRoomLayoutSettingsSuccess(RoomLayoutModel roomLayoutModel) {
        getViewState().onHideProgress();
        this.roomLayoutModel = roomLayoutModel;
        getViewState().changeLayoutSettingsView(isOwnerCurrentUser());
        getViewState().setImageVisibility(roomLayoutModel.getBuildingRules().isImageEnabled());
    }

    public boolean isOwnerCurrentUser() {
        return roomLayoutModel.getRoomSettings().getOwnerId() == appPreferences.getCurrentUserId();
    }

    @Override
    public void detachView(LayoutSettingsView view) {
        super.detachView(view);
        deleteSelectedPhotoFromLocalStorage();
    }

    public void onBack() {
        router.exit();
    }

    public void showSelectImageDialog() {
        getViewState().showPickerPhotoDialog();
    }

    public void uploadImageFromSource(Sources sources) {
        addToDisposable(
                RxImagePicker.with(appContext)
                        .requestImage(sources)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::goToCropImage, this::onErrorUpload));
    }

    @VisibleForTesting
    protected void goToCropImage(@NonNull Uri uri) {
        nameImageFile = fileUtils.getUniqueFileName(FileUtils.IMAGE_PROFILE);
        goToCropImageInternally(getBundleImage(uri));
    }

    @VisibleForTesting
    protected void goToCropImageInternally(Bundle bundleImage) {
        router.navigateTo(RouteScreenName.CROP_FRAGMENT, bundleImage);
    }

    @NonNull
    @VisibleForTesting
    protected Bundle getBundleImage(@NonNull Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("uri", uri);
        bundle.putString("nameImage", nameImageFile);
        return bundle;
    }

    private void onErrorUpload(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public List<String> provideMenuItems() {
        return Arrays.asList(roomLayoutModel.getCurrentUser().getFName(), roomLayoutModel.getCurrentUser().getLName(), String.format("%s %s", roomLayoutModel.getCurrentUser().getFName(), roomLayoutModel.getCurrentUser().getLName()));
    }

    public boolean onNameItemClick(int itemId) {
        List<String> menuItemsList = provideMenuItems();
        String name = menuItemsList.get(itemId);
        getViewState().showName(name);
        return true;
    }

    public void submit(int layoutType, String headerUser, String description) {
        getViewState().onShowProgress();
        addToDisposable(
                userRepository.getAllRooms()
                        .toObservable()
                        .map(this::getRoomId)
                        .subscribe(roomId -> submitInternally(roomId, getAvatarImage(), layoutType, headerUser, description), this::onErrorGetRoomId));
    }

    private void onErrorGetRoomId(Throwable throwable) {
        getViewState().onHideProgress();
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void submitInternally(long roomId, File image, int layoutType, String headerUser, String description) {
        RequestBody requestFile = image != null
                ? RequestBody.create(MediaType.parse("image/jpeg"), image)
                : null;
        RequestBody header = headerUser != null
                ? RequestBody.create(MediaType.parse("multipart/form-data"), headerUser)
                : null;
        RequestBody text = description != null
                ? RequestBody.create(MediaType.parse("multipart/form-data"), description)
                : null;
        MultipartBody.Part multipartBodyImage = requestFile != null
                ? MultipartBody.Part.createFormData("image", image.getName(), requestFile)
                : null;

        switch (layoutType) {
            case LayoutSettingsFragment.LAYOUT_INDIVIDUAL:
                addToDisposable(
                        userService.updateInfoRoom(roomId,
                                multipartBodyImage,
                                header,
                                text)
                                .flatMap(this::updateRoomModelIndividually)
                                .flatMap(updateRoomModel ->
                                        updateLayoutSettings(
                                                roomId,
                                                null,
                                                null,
                                                null,
                                                false))
                                .subscribe(this::onSuccessUpdateOwnerIndividually,
                                        this::onErrorUpdateInfoRoom));
                break;
            case LayoutSettingsFragment.LAYOUT_FAMILY:
                addToDisposable(
                        updateLayoutSettings(roomId,
                                multipartBodyImage,
                                header,
                                text, true)
                                .subscribe(this::onSuccessUpdateInfoRoomFamily, this::onErrorUpdateInfoRoom));
                break;
        }
    }

    private ObservableSource<UpdateRoomModel> updateRoomModelIndividually(UpdateRoomModel updateRoomModel) {
        updateCurrentSettings(updateRoomModel, true);
        if (!isOwnerCurrentUser()) {
            getViewState().onHideProgress();
            getViewState().updateRoomSuccess(updateRoomModel);
        }
        return isOwnerCurrentUser() ? Observable.just(updateRoomModel) : Observable.empty();
    }

    private void onSuccessUpdateOwnerIndividually(UpdateRoomModel updateRoomModel) {
        getViewState().updateRoomSuccess(updateRoomModel);
        getViewState().onHideProgress();
    }

    private void onSuccessUpdateInfoRoomFamily(UpdateRoomModel updateRoomModel) {
        updateCurrentSettings(updateRoomModel, false);
        getViewState().updateRoomSuccess(updateRoomModel);
        getViewState().onHideProgress();
    }

    private void updateCurrentSettings(UpdateRoomModel updateRoomModel, boolean isIndividual) {
        if (isIndividual) {
            this.roomLayoutModel.getIndividualSettings().setUpdateRoom(updateRoomModel);
            return;
        }
        this.roomLayoutModel.getRoomSettings().setUpdateRoom(updateRoomModel);
    }

    private Observable<UpdateRoomModel> updateLayoutSettings(long roomId, MultipartBody.Part multipartBodyImage, RequestBody header, RequestBody text, boolean isSingleBtn) {
        RequestBody isSingleButton = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(isSingleBtn));
        return roomService.updateInfoRoom(roomId,
                multipartBodyImage,
                header,
                text,
                isSingleButton);
    }

    //TODO need change it
    private Integer getRoomId(List<DBRoom> dbRooms) {
        return dbRooms.get(0).getId();
    }

    public File getAvatarImage() {
        return fileUtils.getDiskCacheDir(DefigoApplication.getInstance(), nameImageFile);
    }

    private void onErrorUpdateInfoRoom(Throwable throwable) {
        getViewState().onHideProgress();
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void uploadStorageImageInView() {
        if (fileUtils.checkExistFile(appContext, nameImageFile)) {
            getViewState().uploadImageView(nameImageFile);
        }
    }

    public void deleteSelectedPhotoFromLocalStorage() {
        if (nameImageFile != null) {
            fileUtils.getDiskCacheDir(DefigoApplication.getInstance(), nameImageFile).delete();
        }
    }

    public void getLayoutSettings(int layoutType) {
        switch (layoutType) {
            case LayoutSettingsFragment.LAYOUT_INDIVIDUAL:
                showIndividualSettings();
                break;
            case LayoutSettingsFragment.LAYOUT_FAMILY:
                showFamilySettings();
                break;
        }
    }

    private void showFamilySettings() {
        if (roomLayoutModel != null && !isOwnerCurrentUser()) {
            getViewState().showOrHideLayoutSettingsView(false);
            return;
        }
        if (roomLayoutModel != null) {
            getViewState().showName(roomLayoutModel.getRoomSettings().getHeader());
            getViewState().showDescription(roomLayoutModel.getRoomSettings().getText());
            getViewState().uploadImageUrl(roomLayoutModel.getRoomSettings().getImgUrl());
        }
        getViewState().setLayoutTitle(appContext.getString(R.string.settings_layout_name_family));
        getViewState().setLayoutTypeText(appContext.getString(R.string.family_layout_settings));
        getViewState().setEmptyImageText(appContext.getString(R.string.upload_family_picture));
    }

    private void showIndividualSettings() {
        getViewState().showOrHideLayoutSettingsView(true);
        if (roomLayoutModel != null) {
            getViewState().showName(roomLayoutModel.getIndividualSettings().getHeader());
            getViewState().showDescription(roomLayoutModel.getIndividualSettings().getText());
            getViewState().uploadImageUrl(roomLayoutModel.getIndividualSettings().getImgUrl());
        }
        getViewState().setLayoutTitle(appContext.getString(R.string.settings_layout_name_individual));
        getViewState().setLayoutTypeText(appContext.getString(R.string.individual_layout_settings));
        getViewState().setEmptyImageText(appContext.getString(R.string.upload_individual_picture));
    }
}
