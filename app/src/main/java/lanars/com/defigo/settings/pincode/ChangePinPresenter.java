package lanars.com.defigo.settings.pincode;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.service.IRfidService;
import lanars.com.defigo.common.utils.IAppPreferences;
import ru.terrakok.cicerone.Router;

import static lanars.com.defigo.settings.pincode.ChangePinCodeFragment.TYPE_ACTION_PIN;
import static lanars.com.defigo.settings.pincode.ChangePinCodeFragment.TYPE_RFID_PIN;

@InjectViewState
public class ChangePinPresenter extends BasePresenter<ChangePinView> {

    @Inject
    Router router;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IRfidService rfidService;

    private final int rfidId;
    private static final int PIN_LENGTH = 6;

    public ChangePinPresenter(int rfidId) {
        DefigoApplication.getInstance().getAppComponent().inject(this);
        this.rfidId = rfidId;
    }

    public void onBack() {
        router.exit();
    }

    public void getCurrentPin(String typePin) {
        if (!TextUtils.isEmpty(appPreferences.getPin(typePin))) {
            getViewState().showCurrentPin(appPreferences.getPin(typePin));
        }
    }

    public void savePin(String pin, String typePin) {
        if (pin == null || pin.length() < PIN_LENGTH) {
            getViewState().onShowError(R.string.pin_short);
            return;
        }

        if (TYPE_RFID_PIN.equals(typePin)) {
            addToDisposable(
                    rfidService.setupRfidPin(this.rfidId, pin)
                            .doOnComplete(() -> appPreferences.savePin(pin, typePin))
                            .ignoreElements()
                            .subscribe(this::onSavePinSuccess, this::onError)
            );
            return;
        }

        if (TYPE_ACTION_PIN.equals(typePin)) {
            appPreferences.savePin(pin, typePin);
            getViewState().savePinSuccessfully();
        }
    }

    private void onError(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSavePinSuccess() {
        getViewState().savePinSuccessfully();
    }
}
