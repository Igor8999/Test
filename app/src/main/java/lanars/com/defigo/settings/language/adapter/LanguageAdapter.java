package lanars.com.defigo.settings.language.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.LanguageModel;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {

    private List<LanguageModel> languageModels;
    private OnItemClickRecyclerListener onItemClickRecyclerListener;
    private int seletedLanguagePosition;

    public LanguageAdapter(List<LanguageModel> languageModels, OnItemClickRecyclerListener onItemClickRecyclerListener) {
        this.languageModels = languageModels;
        this.onItemClickRecyclerListener = onItemClickRecyclerListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_languges_settings, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setOnClickListener(v -> {
            int position = viewHolder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onItemClickRecyclerListener.onItemClick(position);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LanguageModel language = languageModels.get(position);
        holder.tvName.setText(language.getName());
        holder.imgSelect.setVisibility(seletedLanguagePosition == position ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return languageModels.size();
    }

    public void setSelectedLanguage(int position) {
        seletedLanguagePosition = position;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imSelect)
        ImageView imgSelect;

        @BindView(R.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public List<LanguageModel> getLanguageModels() {
        return languageModels;
    }
}
