package lanars.com.defigo.settings.access;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import lanars.com.defigo.common.base.BaseMvpView;
import lanars.com.defigo.common.network.models.User;

public interface ProvideAccessListView extends BaseMvpView {

    void setProvideUsers(List<User> users);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void setAddFoundUsers(List<User> users);

    void onDeleteUser(User user);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void getProvideUsers();
}
