package lanars.com.defigo.settings.phonenumber;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.Utils;

public class PhoneNumberSettingsFragment extends BaseSettingsFragment implements PhoneNumberView {

    @InjectPresenter
    PhoneNumberPresenter phoneNumberPresenter;

    @BindView(R.id.tvResendCode)
    TextView tvResendCode;

    @BindView(R.id.edVerifyCode)
    EditText edVerifyCode;

    @BindView(R.id.tvPhoneNumberInfo)
    TextView tvPhoneNumberInfo;

    @BindView(R.id.etPhoneNumber)
    TextView etPhoneNumber;

    @BindView(R.id.btnVerify)
    Button btnVerify;

    @BindView(R.id.llResendCode)
    LinearLayout llResendCode;

    @BindView(R.id.btnChangePhoneNumber)
    Button btnChangePhoneNumber;

    @BindView(R.id.bottom_sheet_enter_number)
    LinearLayout llBottomSheet;

    private BottomSheetBehavior<LinearLayout> bottomSheetBehavior;

    @Override
    protected String getTitle() {
        return getString(R.string.phone_number);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.phone_number_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        tvResendCode.setPaintFlags(tvResendCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvResendCode.setText(getString(R.string.resend_code));
        initBottomSheetEnterPhoneNumber();
    }

    private void initBottomSheetEnterPhoneNumber() {
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:

                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    default:
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @OnTextChanged(value = R.id.etPhoneNumber, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void changePhoneEnable(Editable editable) {
        phoneNumberPresenter.changePhoneEnable(editable.toString());
    }

    @OnClick(R.id.btnChangePhoneNumber)
    void changePhoneNumber() {
        phoneNumberPresenter.changePhoneNumber(etPhoneNumber.getText().toString());
    }

    @OnClick(R.id.imgClose)
    void closeNumberVerificationsView() {
        imgClose.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        tvTitleSettings.setText(R.string.phone_number);
        Utils.hideKeyboard(getActivity());
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void handleBackClick() {
        phoneNumberPresenter.onBack();
    }

    @Override
    public void checkPermissionAndGetPhoneNumber() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.READ_PHONE_STATE)
                .subscribe(this::onSuccessPermission, this::onErrorPermission);
    }

    @Override
    public void setCurrentPhoneNumber(String phoneNumber) {
        etPhoneNumber.setText(phoneNumber);
    }

    @Override
    public void verifyCodeSuccess() {
        Utils.hideKeyboard(getActivity());
        imgClose.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        tvTitleSettings.setText(R.string.phone_number);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onShowSuccessUserUpdate() {
        showStatusVerified(true);
        Alert.create(getActivity())
                .setDuration(2000)
                .setText(R.string.changes_saved)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @Override
    public void onChangePhoneButtonEnable(boolean isVisible) {
        btnChangePhoneNumber.setVisibility(isVisible ? View.GONE : View.VISIBLE);
        showStatusVerified(isVisible);

    }

    @Override
    public void showStatusVerified(boolean isVerified) {
        btnChangePhoneNumber.setVisibility(isVerified ? View.GONE : View.VISIBLE);
    }

    private void onSuccessPermission(Boolean isGranted) {
        if (isGranted) {
            getLocalPhoneNumber();
        } else {
            showAlertPhonePermission();
        }
    }

    private void showAlertPhonePermission() {
        Alert.create(getActivity())
                .setText(R.string.add_permission_phone_alert)
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @SuppressLint("MissingPermission")
    private void getLocalPhoneNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            String currentPhoneNumber = telephonyManager.getLine1Number();
            phoneNumberPresenter.getPhoneNumberLocal(currentPhoneNumber);
        }
    }

    private void onErrorPermission(Throwable throwable) {
        showAlertPhonePermission();
    }
}
