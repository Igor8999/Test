package lanars.com.defigo.settings.profile;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.login.LoginActivity;

public class ProfileSettingsFragment extends BaseSettingsFragment implements ProfileSettingsView {

    @InjectPresenter
    ProfileFragmentPresenter profileFragmentPresenter;

    @Override
    protected String getTitle() {
        return getString(R.string.profile_info);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.profile_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void handleBackClick() {
        profileFragmentPresenter.onBack();
    }

    @OnClick(R.id.dsrPersonalInfo)
    void goToPersonalInfo() {
        profileFragmentPresenter.goToPersonalInfo();
    }

    @OnClick(R.id.dsrAddress)
    void goToAddressSettings() {
        profileFragmentPresenter.goToAddressSettings();
    }

    @OnClick(R.id.dsrChangePassword)
    void goToChangePassword() {
        profileFragmentPresenter.goToChangePassword();
    }

    @OnClick(R.id.dsrEmailManagment)
    void goToEmailSettings() {
        profileFragmentPresenter.goToEmailSettings();
    }

    @OnClick(R.id.dsrPhoneNumberManagment)
    void goToPhoneSettings() {
        profileFragmentPresenter.goToPhoneSettings();
    }

    @OnClick(R.id.dsrLanguage)
    void goToLanguageSettings() {
        profileFragmentPresenter.goToLanguageSettings();
    }

    @Override
    public void onShowLogoutButton(boolean isShow) {
        btnLogout.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void logoutView() {
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    @OnClick(R.id.btnLogout)
    void logout() {
        profileFragmentPresenter.clearData();
    }
}
