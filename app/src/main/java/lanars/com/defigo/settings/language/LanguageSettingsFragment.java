package lanars.com.defigo.settings.language;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.LanguageModel;
import lanars.com.defigo.common.ui.listener.OnItemClickRecyclerListener;
import lanars.com.defigo.settings.language.adapter.LanguageAdapter;

public class LanguageSettingsFragment extends BaseSettingsFragment implements LanguageSeetingsView, OnItemClickRecyclerListener {

    @InjectPresenter
    LanguageSettingsPresenter languageSettingsPresenter;

    @BindView(R.id.rvLanguages)
    RecyclerView rvLanguages;

    private LanguageAdapter languageAdapter;

    @Override
    protected String getTitle() {
        return getString(R.string.language);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.language_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        languageSettingsPresenter.getLanguages();
    }

    @Override
    public void handleBackClick() {
        languageSettingsPresenter.onBack();
    }

    @Override
    public void setLanguages(List<LanguageModel> languageModels) {
        languageAdapter = new LanguageAdapter(languageModels, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvLanguages.setLayoutManager(linearLayoutManager);
        rvLanguages.setAdapter(languageAdapter);
        rvLanguages.setHasFixedSize(true);
        rvLanguages.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onItemClick(int position) {
        languageAdapter.setSelectedLanguage(position);
    }
}
