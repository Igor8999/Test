package lanars.com.defigo.settings.actions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.settings.mutemode.MuteModeDaysFragment;

public class ActionsSettingsFragment extends BaseSettingsFragment implements ActionSettingsView {

    @InjectPresenter
    ActionsSettingsPresenter actionsSettingsFragment;

    @Override
    protected String getTitle() {
        return getString(R.string.actions);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.actions_settings_fragment, container, false);
    }

    @OnClick(R.id.llOpenDoorInstantly)
    protected void goToOpenDoorInstantly() {
        actionsSettingsFragment.goToOpenDoorScreen(MuteModeDaysFragment.MUTE_DAYS_OPEN_INSTANTLY);
    }

    @OnClick(R.id.llOpenDoorPin)
    protected void goToOpenDoorPin() {
        actionsSettingsFragment.goToOpenDoorScreen(MuteModeDaysFragment.MUTE_DAYS_OPEN_ON_PIN);
    }

    @Override
    public void initViews(View view) {

    }

    @Override
    public void handleBackClick() {
        actionsSettingsFragment.onBack();
    }

    @Override
    public void onShowError(String errorMessage){

    }
}
