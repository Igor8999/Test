package lanars.com.defigo.settings.access.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import lanars.com.defigo.R;
import lanars.com.defigo.common.network.models.TypeAccessUser;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.ui.listener.OnAddProvideAccessRecyclerListener;
import lanars.com.defigo.common.ui.listener.OnDeleteItemRecyclerListener;
import lanars.com.defigo.common.utils.AppConst;

public class ProvideAccessListAdapter extends RecyclerView.Adapter<ProvideAccessBaseViewHolder> {

    private List<User> users = new ArrayList<>();

    private OnAddProvideAccessRecyclerListener onAddProvideListener;

    private OnDeleteItemRecyclerListener onDeleteItemRecyclerListener;

    public ProvideAccessListAdapter(OnAddProvideAccessRecyclerListener onAddProvideListener,
                                    OnDeleteItemRecyclerListener onDeleteItemRecyclerListener,
                                    List<User> users) {
        this.users = users;
        this.onAddProvideListener = onAddProvideListener;
        this.onDeleteItemRecyclerListener = onDeleteItemRecyclerListener;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public ProvideAccessBaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TypeAccessUser.STATE_CLOSE_ACCESS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_provide_close_access, parent, false);
                ProvideAccessListCloseViewHolder accessCloseViewHolder = new ProvideAccessListCloseViewHolder(view);
                setOnClickListenersCloseProvideAccess(accessCloseViewHolder);
                return accessCloseViewHolder;
            case TypeAccessUser.STATE_OPEN_ACCESS_ALWAYS:
            case TypeAccessUser.STATE_OPEN_ACCESS_ON_TIME:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_provide_access, parent, false);
                ProvideAccessListOpenViewHolder accessOpenViewHolder = new ProvideAccessListOpenViewHolder(view);
                setOnClickListenersOpenProvideAccess(accessOpenViewHolder);
                return accessOpenViewHolder;
            default:
                return null;
        }
    }

    private void setOnClickListenersOpenProvideAccess(ProvideAccessListOpenViewHolder accessOpenViewHolder) {
        accessOpenViewHolder.btnDelete.setOnClickListener(v -> deleteAccessUser(accessOpenViewHolder));
        accessOpenViewHolder.imgProvideTimer.setOnClickListener(v -> noProvideAccessClick(accessOpenViewHolder));

    }

    private void setOnClickListenersCloseProvideAccess(ProvideAccessListCloseViewHolder accessCloseViewHolder) {
        accessCloseViewHolder.tvNoProvideAccess.setOnClickListener(v -> noProvideAccessClick(accessCloseViewHolder));
        accessCloseViewHolder.btnDelete.setOnClickListener(v -> deleteAccessUser(accessCloseViewHolder));
    }

    private void deleteAccessUser(RecyclerView.ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onDeleteItemRecyclerListener.onDeleteItem(position);
        }
    }

    private void noProvideAccessClick(RecyclerView.ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onAddProvideListener.onAddProvideAccess(position);
        }
    }

    @Override
    public void onBindViewHolder(ProvideAccessBaseViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        User user = users.get(position);
        switch (viewType) {
            case TypeAccessUser.STATE_CLOSE_ACCESS:
                ProvideAccessListCloseViewHolder accessCloseViewHolder = (ProvideAccessListCloseViewHolder) holder;
                accessCloseViewHolder.swLayout.setRightSwipeEnabled(false);
                accessCloseViewHolder.tvNameUser.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
                break;
            case TypeAccessUser.STATE_OPEN_ACCESS_ALWAYS:
                ProvideAccessListOpenViewHolder accessOpenViewHolder = (ProvideAccessListOpenViewHolder) holder;
                accessOpenViewHolder.tvNameUser.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
                break;
            case TypeAccessUser.STATE_OPEN_ACCESS_ON_TIME:
                ProvideAccessListOpenViewHolder accessOpenTimeViewHolder = (ProvideAccessListOpenViewHolder) holder;
                accessOpenTimeViewHolder.tvNameUser.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
                break;
        }

        uploadPhotoUser(holder, user);
    }

    private void uploadPhotoUser(ProvideAccessBaseViewHolder holder, User user) {
        Glide.with(holder.itemView.getContext())
                .load(user.getImgUrl())
                .apply(getRequestOptions())
                .into(holder.imgUser);
    }

    @NonNull
    private RequestOptions getRequestOptions() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.user_placeholder);
        requestOptions.error(R.drawable.user_placeholder);
        return requestOptions;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public int getItemViewType(int position) {
        User user = users.get(position);
        if (user.getSchedule() != null) {
            return TypeAccessUser.STATE_OPEN_ACCESS_ON_TIME;
        }
        return TypeAccessUser.STATE_CLOSE_ACCESS;
    }

    //TODO remove logic from adapter!

    public void addUsers(List<User> users) {
        AppConst.Logger("Addd users");
        int lastIndexInserted = this.users.size();
        this.users.addAll(users);
        notifyItemRangeInserted(lastIndexInserted, users.size());
    }

    public void clearData() {
        users.clear();
        notifyDataSetChanged();
    }

    public void deleteUser(User user) {
        int removeIndex = users.indexOf(user);
        users.remove(removeIndex);
        notifyItemRemoved(removeIndex);
    }
}
