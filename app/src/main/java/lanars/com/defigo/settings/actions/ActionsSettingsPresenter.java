package lanars.com.defigo.settings.actions;

import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.utils.AppConst;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ActionsSettingsPresenter extends BasePresenter<ActionSettingsView> {

    @Inject
    Router router;

    public ActionsSettingsPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        getViewState().onShowError();
        router.exit();
    }

    public void goToOpenDoorScreen(String openDoorType) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.OPEN_DOORS_TYPE, openDoorType);
        router.navigateTo(RouteScreenName.OPEN_DOORS_ACTION, bundle);
    }

}
