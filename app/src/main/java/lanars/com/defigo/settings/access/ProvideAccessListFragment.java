package lanars.com.defigo.settings.access;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.ui.listener.EndlessRecyclerViewScrollListener;
import lanars.com.defigo.common.ui.listener.OnAddProvideAccessRecyclerListener;
import lanars.com.defigo.common.ui.listener.OnDeleteItemRecyclerListener;
import lanars.com.defigo.common.utils.EditTextClearUtil;
import lanars.com.defigo.settings.access.adapter.ProvideAccessListAdapter;

public class ProvideAccessListFragment extends BaseSettingsFragment implements ProvideAccessListView, OnAddProvideAccessRecyclerListener, OnDeleteItemRecyclerListener {

    @BindView(R.id.rvProvideUsers)
    RecyclerView rvProvideUsers;

    @BindView(R.id.etSearch)
    EditText etSearch;

    @InjectPresenter
    ProvideAccessListPresenter provideAccessListPresenter;

    private ProvideAccessListAdapter accessAdapter;

    private EndlessRecyclerViewScrollListener scrollListener;

    private boolean isPagination;

    @Override
    protected String getTitle() {
        return getString(R.string.provide_access);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.provide_access_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        ButterKnife.bind(this, view);

        //todo need remove this
        new EditTextClearUtil.Builder(getContext())
                .setEditText(etSearch)
                .setClearDrawable(getResources().getDrawable(R.drawable.close_icon))
                .build();
        RxTextView.textChanges(etSearch).skipInitialValue()
                .subscribe(this::searchUsersByInput, this::onErrorInputSearch);
    }

    private void onErrorInputSearch(Throwable throwable) {
        //nothing
    }

    private void searchUsersByInput(CharSequence charSequence) {
        isPagination = true;
        accessAdapter.clearData();
        scrollListener.resetState();
        if (TextUtils.isEmpty(charSequence.toString())) {
            provideAccessListPresenter.getProvideUsers();
        } else {
            searchAllUsers(0, charSequence.toString(), ProvideAccessListPresenter.PROVIDE_LIMIT);
        }
    }

    @Override
    public void handleBackClick() {
        provideAccessListPresenter.onBack();
    }

    @Override
    public void setProvideUsers(List<User> users) {
        accessAdapter = new ProvideAccessListAdapter(this, this, users);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rvProvideUsers.setLayoutManager(linearLayoutManager);
        rvProvideUsers.setAdapter(accessAdapter);
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, 10) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                searchAllUsers(page, etSearch.getText().toString(), ProvideAccessListPresenter.PROVIDE_LIMIT);
            }
        };
        rvProvideUsers.addOnScrollListener(scrollListener);
    }

    private void searchAllUsers(int page, String search, int limit) {
        if (isPagination && !TextUtils.isEmpty(search)) {
            provideAccessListPresenter.searchUsers(search, page, limit);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isPagination = false;
        scrollListener.resetState();
    }

    @Override
    public void setAddFoundUsers(List<User> users) {
        accessAdapter.addUsers(users);
    }

    @Override
    public void onDeleteUser(User user) {
        accessAdapter.deleteUser(user);
    }

    @Override
    public void getProvideUsers() {
        if (TextUtils.isEmpty(etSearch.getText().toString())) {
            provideAccessListPresenter.getProvideUsers();
        }
    }

    @Override
    public void onAddProvideAccess(int position) {
        provideAccessListPresenter.goToProvideAccessScheduling(accessAdapter.getUsers().get(position));
    }

    @Override
    public void onDeleteItem(int position) {
        provideAccessListPresenter.deleteShareAccessUser(accessAdapter.getUsers().get(position));
    }
}
