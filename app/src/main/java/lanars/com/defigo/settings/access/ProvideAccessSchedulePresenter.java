package lanars.com.defigo.settings.access;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.network.models.StatusModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.repository.user.IDataRepository;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.settings.mutemode.MuteModePresenter;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ProvideAccessSchedulePresenter extends BasePresenter<ProvideAccessScheduleView> {

    @Inject
    Router router;

    @Inject
    IDataRepository dataRepository;

    @Inject
    IUserRepository userRepository;

    @Inject
    IAppPreferences appPreferences;

    private User selectProvideUser;

    private boolean isReapetedAttach;

    public ProvideAccessSchedulePresenter(User selectProvideUser) {
        this.selectProvideUser = selectProvideUser;
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void attachView(ProvideAccessScheduleView view) {
        super.attachView(view);
        if (isReapetedAttach) {
            getSelectedMuteDays();
        }
        isReapetedAttach = true;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        setProvideSettings();
    }

    private void setProvideSettings() {
        if (selectProvideUser.getSchedule() != null) {
            getViewState().setProvideSettings(selectProvideUser.getSchedule());
        }
    }

    public void onBack() {
        router.exit();
    }

    public void goToMuteDaysOfWeekScreen() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.KEY_MUTE_DAYS, MuteModePresenter.MUTE_DAY_PROVIDE_ACCESS);
        router.navigateTo(RouteScreenName.MUTE_MODE, bundle);
    }

    public void setTimeInView(boolean isSelectFrom, int hourOfDay, int minute) {
        if (isSelectFrom) {
            getViewState().setTimeFrom(hourOfDay, minute);
            return;
        }
        getViewState().setTimeTo(hourOfDay, minute);
    }

    public void getSelectedMuteDays() {
        addToDisposable(dataRepository.getAllDaysOfWeek(MuteModePresenter.MUTE_DAY_PROVIDE_ACCESS)
                .subscribe(this::setMuteDaysInView, this::onErrorGetDays));
    }

    private void onErrorGetDays(Throwable throwable) {

    }

    private void setMuteDaysInView(List<DayOfWeek> dayOfWeeks) {
        getViewState().setMuteDaysText(getStringDaysOfWeeks(dayOfWeeks));
    }

    @NonNull
    private String getStringDaysOfWeeks(List<DayOfWeek> dayOfWeeks) {
        StringBuilder stringBuilder = new StringBuilder();
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            if (dayOfWeek.isSelected()) {
                stringBuilder.append(dayOfWeek.getName().substring(0, 3))
                        .append("  ");
            }
        }
        return stringBuilder.toString();
    }

    public void submit(boolean isDeleteAccess, User selectProvideUser, String startTimeStr, String endTimeStr) {
        if (!isDeleteAccess) {
            removeSharedAccess(selectProvideUser);
        } else {
            Map<String, Object> params = createShareAccessParams(selectProvideUser, startTimeStr, endTimeStr);
            addToDisposable(userRepository.shareAccessToUser(params)
                    .subscribe(this::onSuccessAddProvideUser, this::onErrorProvideUser));
        }

    }

    private void removeSharedAccess(User selectProvideUser) {
        userRepository.removeSharedAccess(selectProvideUser.getId())
                .subscribe(this::onSuccessAddProvideUser, this::onErrorProvideUser);
    }

    @NonNull
    private Map<String, Object> createShareAccessParams(User selectProvideUser, String startTimeStr, String endTimeStr) {
        Map<String, Object> params = new HashMap<>();
        params.put("guestId", selectProvideUser.getId());
        params.put("pin", 0);
        params.put("endTime", endTimeStr);
        params.put("startTime", startTimeStr);
        params.put("daysOfWeek", getArrayMuteDays());
        return params;
    }

    private void onErrorProvideUser(Throwable throwable) {
        getViewState().onShowError(throwable.getMessage());
    }

    private void onSuccessAddProvideUser(StatusModel statusModel) {
        getViewState().onSuccessProvideUser();
    }

    private Object[] getArrayMuteDays() {
        List<Integer> dayIndexes = new ArrayList<>();
        for (DayOfWeek dayOfWeek : appPreferences.getMuteDays(MuteModePresenter.MUTE_DAY_PROVIDE_ACCESS)) {
            if (dayOfWeek.isSelected()) {
                dayIndexes.add(dayOfWeek.getIndex());
            }
        }
        return dayIndexes.toArray();
    }

    public void getMuteDaysFromIndexes(List<Integer> dayMuteIndexes) {
        getViewState().setMuteDaysText(getMuteDaysFromIndexesStr(dayMuteIndexes));
    }

    //todo need refactor this
    private String getMuteDaysFromIndexesStr(List<Integer> dayMuteIndexes) {
        StringBuilder stringBuilder = new StringBuilder();
        List<DayOfWeek> listDays = dataRepository.getAllDaysOfWeek(MuteModePresenter.MUTE_DAY_PROVIDE_ACCESS)
                .toList()
                .blockingGet()
                .get(0);
        for (int i = 0; i < listDays.size(); i++) {
            if (dayMuteIndexes.contains(listDays.get(i).getIndex())) {
                listDays.get(i).setSelected(true);
                stringBuilder.append(listDays.get(i).getName().substring(0, 3))
                        .append("  ");
            } else {
                listDays.get(i).setSelected(false);
            }
        }
        saveSelectedDays(listDays);
        return stringBuilder.toString();
    }

    private void saveSelectedDays(List<DayOfWeek> listDays) {
        dataRepository.saveDayOfWeeks(listDays, MuteModePresenter.MUTE_DAY_PROVIDE_ACCESS);
    }
}
