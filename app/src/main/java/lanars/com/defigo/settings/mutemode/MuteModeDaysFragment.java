package lanars.com.defigo.settings.mutemode;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.DayOfWeek;
import lanars.com.defigo.common.utils.AppConst;
import lanars.com.defigo.settings.visibility.adapter.MuteModeDaysAdapter;

public class MuteModeDaysFragment extends BaseSettingsFragment implements MuteModeView {

    @BindView(R.id.rvMuteModeDays)
    RecyclerView rvMuteModeDay;

    @InjectPresenter
    MuteModePresenter muteModePresenter;

    public static final String MUTE_DAYS_VISIBILITY = "mute_days_visibility";
    public static final String MUTE_DAYS_OPEN_INSTANTLY = "open";
    public static final String MUTE_DAYS_OPEN_ON_PIN = "pin";

    private MuteModeDaysAdapter adapter;

    private String keyMuteDays;

    public static MuteModeDaysFragment newInstance(Bundle bundle) {
        MuteModeDaysFragment fragment = new MuteModeDaysFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        keyMuteDays = getArguments().getString(AppConst.KEY_MUTE_DAYS);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getTitle() {
        return getMuteModeTitle();
    }

    private String getMuteModeTitle() {
        switch (keyMuteDays) {
            case MUTE_DAYS_OPEN_INSTANTLY:
                return getString(R.string.open_doors_instantly);
            case MUTE_DAYS_OPEN_ON_PIN:
                return getString(R.string.open_doors_with_pin);
            case MUTE_DAYS_VISIBILITY:
                return getString(R.string.visibility_options);
            default:
                return "";
        }
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.mute_mode_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        muteModePresenter.getDaysOfWeek(keyMuteDays);
    }

    @Override
    public void handleBackClick() {
        muteModePresenter.onBack();
    }

    @Override
    public void setDaysOfWeek(List<DayOfWeek> daysOfWeek) {
        adapter = new MuteModeDaysAdapter(daysOfWeek);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvMuteModeDay.setLayoutManager(linearLayoutManager);
        rvMuteModeDay.setAdapter(adapter);
        rvMuteModeDay.setHasFixedSize(true);
        rvMuteModeDay.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onStop() {
        super.onStop();
        muteModePresenter.saveMuteDays(adapter.getDayOfWeeks(), keyMuteDays);
    }
}
