package lanars.com.defigo.settings.callingsequence;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;

public class CallingSequenceMenuFragment extends BaseSettingsFragment implements CallingSequenceMenuView {

    @InjectPresenter
    CallingSequenceMenuPresenter callingSequenceMenuPresenter;

    @Override
    protected String getTitle() {
        return getString(R.string.calling_sequence);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.calling_sequence_menu_fragment, container, false);
    }

    @Override
    public void initViews(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void handleBackClick() {
        callingSequenceMenuPresenter.onBack();
    }

    @OnClick(R.id.llSimultaneousCall)
    void goToSimultaneousCall() {
        callingSequenceMenuPresenter.goToSimultaneousCall();
    }

    @OnClick(R.id.llSequentialCall)
    void goToCallSequence() {
        callingSequenceMenuPresenter.goToCallSequence();
    }
}
