package lanars.com.defigo.settings.profile.personal;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;

public interface PersonalInfoView extends BaseMvpView {

    @StateStrategyType(SkipStrategy.class)
    void showPickerPhotoDialog();

    @StateStrategyType(SkipStrategy.class)
    void submitSuccess();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void uploadImageView(String fileName);

    void getCurrentImageProfileUpload();

    @StateStrategyType(SkipStrategy.class)
    void showFirstName(String name);

    @StateStrategyType(SkipStrategy.class)
    void showLastName(String name);

    @StateStrategyType(SkipStrategy.class)
    void showUsername(String name);

    @StateStrategyType(SkipStrategy.class)
    void uploadPhoto(String imgUrl);
}
