package lanars.com.defigo.settings.password;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.common.utils.Utils;

public class ChangePasswordFragment extends BaseSettingsFragment implements ChangePasswordView {

    @InjectPresenter
    ChangePasswordPresenter presenter;

    @BindView(R.id.etCurrentPassword)
    EditText etCurrentPassword;

    @BindView(R.id.etNewPassword)
    EditText etNewPassword;

    @BindView(R.id.etConfirmNewPassword)
    EditText etConfirmNewPassword;

    @Override
    protected String getTitle() {
        return getString(R.string.change_password);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.change_password_fragment, container, false);
    }

    @Override
    public void initViews(View view) {

    }

    @Override
    public void handleBackClick() {
        presenter.onBack();
    }

    @Override
    public void onSuccessUpdate(int successTextResId) {
        Alert.create(getActivity())
                .setDuration(2500)
                .setText(successTextResId)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @OnClick(R.id.btnSubmit)
    public void submit(View view) {
        presenter.submit(etCurrentPassword.getText().toString(), etNewPassword.getText().toString(), etConfirmNewPassword.getText().toString());
    }

    @Override
    public void showValidationError(int textResId) {
        Alert.create(getActivity())
                .setText(getString(textResId))
                .setDuration(2500)
                .enableSwipeToDismiss()
                .show();
    }

    @Override
    public void hideKeyboard() {
        Utils.hideKeyboard(getActivity());
    }
}
