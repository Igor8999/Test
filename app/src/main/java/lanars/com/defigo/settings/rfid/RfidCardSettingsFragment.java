package lanars.com.defigo.settings.rfid;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseSettingsFragment;
import lanars.com.defigo.common.network.models.RfidModel;
import lanars.com.defigo.common.ui.alert.Alert;
import lanars.com.defigo.settings.rfid.adapter.RfidCardUsersAdapter;

public class RfidCardSettingsFragment extends BaseSettingsFragment implements RfidCardSettingsView {

    @BindView(R.id.tvChangePin)
    TextView tvChangePin;

    @BindView(R.id.rvRfidCardUsers)
    RecyclerView rvRfidCardUsers;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.rlDeletePin)
    ViewGroup rlDeletePin;

    @InjectPresenter
    RfidCardSettingsPresenter presenter;

    private RfidCardUsersAdapter adapter;

    @Override
    protected String getTitle() {
        return getString(R.string.rfid_card_management);
    }

    @Override
    public View createView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.rfid_card_settings_fragment, container, false);
    }

    @Override
    public void initViews(View view) {

    }

    @OnClick(R.id.tvChangePin)
    public void openChangePin(View view) {
        presenter.goToChangePinCode();
    }

    @Override
    public void handleBackClick() {
        presenter.onBack();
    }

    @Override
    public void setUsers(List<RfidModel> users) {
        adapter = new RfidCardUsersAdapter(users, presenter::handleItemClick, presenter::handleOnEditClick);
        rvRfidCardUsers.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvRfidCardUsers.setLayoutManager(linearLayoutManager);
        rvRfidCardUsers.setHasFixedSize(true);
    }

    @Override
    public void onSuccessUpdate(int successTextResId) {
        Alert.create(getActivity())
                .setDuration(2500)
                .setText(successTextResId)
                .setTextColor(getResources().getColor(R.color.colorTextHome))
                .setBackgroundColor(getResources().getColor(R.color.colorAlertSuccess))
                .show();
    }

    @Override
    public void setDeletePinVisibility(boolean isVisible) {
        rlDeletePin.setVisibility(isVisible
                ? View.VISIBLE
                : View.GONE
        );
    }

    @Override
    public void setChangePinLabelText(int labelTextResId) {
        tvChangePin.setText(labelTextResId);
    }

    @OnClick(R.id.rlDeletePin)
    public void onDeleteClick(View view) {
        presenter.deletePin();
    }

    @Override
    public void notifyDataChanged() {
        adapter.notifyDataSetChanged();
    }
}
