package lanars.com.defigo.settings.address;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.network.models.UserUpdateRequestBody;
import lanars.com.defigo.common.network.service.IUserService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBUser;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class AddressPresenter extends BasePresenter<AddressView> {

    @Inject
    Router router;

    @Inject
    IUserRepository userRepository;

    @Inject
    IUserService userService;

    @Inject
    IAppPreferences appPreferences;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getAddressUser();
    }

    public AddressPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    AddressPresenter(Router router, IUserRepository userRepository, IAppPreferences appPreferences, IUserService userService) {
        this.router = router;
        this.userRepository = userRepository;
        this.appPreferences = appPreferences;
        this.userService = userService;
    }

    protected void getAddressUser() {
        getViewState().onShowProgress();

        addToDisposable(userRepository
                .getUserById(appPreferences.getCurrentUserId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showUserAddress, this::onGetErrorUser));
    }

    private void onGetErrorUser(Throwable throwable) {
        getViewState().onHideProgress();
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void showUserAddress(DBUser dbUser) {
        getViewState().onHideProgress();
        getViewState().showStreet(dbUser.street);
        getViewState().showZip(dbUser.zip);
        getViewState().showCountry(dbUser.country);
        getViewState().showCity(dbUser.city);
    }

    public void onBack() {
        router.exit();
    }

    public void submitAddress(String street, String zip, String country, String city) {
        UserUpdateRequestBody userUpdateRequestBody = new UserUpdateRequestBody.Builder()
                .street(getRequestBodyParams(street))
                .zip(getRequestBodyParams(zip))
                .country(getRequestBodyParams(country))
                .city(getRequestBodyParams(city))
                .build();

        addToDisposable(
                userService.usersMyUpdate(userUpdateRequestBody)
                        .doOnSubscribe(disposable -> getViewState().onShowProgress())
                        .doAfterTerminate(() -> getViewState().onHideProgress())
                        .map(UserConverter::convertToDatabaseUser)
                        .doOnNext(updateUser())
                        .ignoreElements()
                        .subscribe(this::onSuccessUserUpdate, this::onErrorUserUpdate));
    }

    @Nullable
    private RequestBody getRequestBodyParams(String inputParams) {
        return inputParams != null
                ? RequestBody.create(MediaType.parse("multipart/form-data"), inputParams)
                : null;
    }

    private void onErrorUserUpdate(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void onSuccessUserUpdate() {
        getViewState().onShowSuccessUserUpdate();
    }

    @NonNull
    private Consumer<DBUser> updateUser() {
        return user -> userRepository.updateUsers(user);
    }
}
