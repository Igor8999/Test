package lanars.com.defigo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import lanars.com.defigo.common.utils.Utils;

public class NetworkStateObservable {

    private NetworkStateReceiver networkStateReceiver;
    private PublishSubject<Boolean> bus = PublishSubject.create();

    public NetworkStateObservable register(Context context) {
        networkStateReceiver = new NetworkStateReceiver();
        context.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        return this;
    }

    public void unRegister(Context context) {
        context.unregisterReceiver(networkStateReceiver);
    }

    public Observable<Boolean> toObservable() {
        return bus;
    }

    public class NetworkStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            bus.onNext(Utils.isConnected(context));
        }
    }
}
