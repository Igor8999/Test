package lanars.com.defigo.register;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import lanars.com.defigo.common.base.BaseMvpView;

public interface RegistrationView extends BaseMvpView {

    @StateStrategyType(SkipStrategy.class)
    void showPickerPhotoDialog();

    void uploadImageView(String nameImageFile);
}
