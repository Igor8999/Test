package lanars.com.defigo.register;

import android.Manifest;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mlsdev.rximagepicker.Sources;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.common.base.BaseFragment;
import lanars.com.defigo.common.ui.UploadPhotoView;
import lanars.com.defigo.common.utils.EditTextClearUtil;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import lanars.com.defigo.common.utils.Utils;

public class RegistrationFragment extends BaseFragment implements RegistrationView {

    @BindView(R.id.etUsername)
    EditText etUsername;

    @BindView(R.id.etFirstName)
    EditText etFirstName;

    @BindView(R.id.etLastName)
    EditText etLastName;

    @BindView(R.id.etPhone)
    EditText etPhone;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IFileUtils fileUtils;

    @InjectPresenter
    RegistrationPresenter registrationPresenter;

    @BindView(R.id.btnLogin)
    TextView btnLogin;

    @BindView(R.id.upload_view)
    UploadPhotoView uploadPhotoView;

    public static final int INPUT_PASS_NUMBER = 5;

    //todo tmp
    private Drawable clearDrawable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registration_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        clearDrawable = getActivity().getDrawable(R.drawable.close_icon);
        addClearUtil();
        validateInputPassword();
    }

    private void validateInputPassword() {
        registrationPresenter.addToDisposable(Observable.combineLatest(
                RxTextView.textChanges(etPassword).skipInitialValue()
                        .map(password -> password.toString().trim())
                        .doOnNext(this::checkPassInput),
                RxTextView.textChanges(etConfirmPassword).skipInitialValue()
                        .map(confirmPass -> confirmPass.toString().trim()),
                TextUtils::equals)
                .subscribe(this::checkConfirmPassEquals));
    }

    private void checkPassInput(CharSequence charSequence) {
        etPassword.setBackground(charSequence.length() > INPUT_PASS_NUMBER ? getContext().getDrawable(R.drawable.ed_selector_with_line) : getContext().getDrawable(R.drawable.error_edit_text));

    }

    private void checkConfirmPassEquals(Boolean isEqualsPassword) {
        etConfirmPassword.setBackground(isEqualsPassword ? getContext().getDrawable(R.drawable.ed_selector_with_line) : getContext().getDrawable(R.drawable.error_edit_text));
    }

    private void checkPermissionsAndGetImage() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .subscribe(this::OnSuccessPermission,
                        this::onErrorPermission);
    }

    private void OnSuccessPermission(Boolean isApprove) {
        if (isApprove) {
            registrationPresenter.showSelectImageDialog();
        } else {
            onShowError(getString(R.string.read_permission_alert));
        }
    }

    private void onErrorPermission(Throwable throwable) {
        onShowError(throwable.getLocalizedMessage());
    }

    private void addClearUtil() {
        getEditTextClearUtil(clearDrawable, etEmail);
        getEditTextClearUtil(clearDrawable, etPassword);
        getEditTextClearUtil(clearDrawable, etUsername);
        getEditTextClearUtil(clearDrawable, etLastName);
        getEditTextClearUtil(clearDrawable, etFirstName);
        getEditTextClearUtil(clearDrawable, etEmail);
        getEditTextClearUtil(clearDrawable, etPhone);
        getEditTextClearUtil(clearDrawable, etConfirmPassword);
    }

    @Override
    public void showPickerPhotoDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.select_photo))
                .setPositiveButton(getString(R.string.from_camera), (dialog, which) -> {
                    registrationPresenter.uploadImage(Sources.CAMERA);
                })
                .setNegativeButton(getString(R.string.from_gallery), (dialog, which) -> registrationPresenter.uploadImage(Sources.GALLERY))
                .create()
                .show();
    }

    @Override
    public void uploadImageView(String nameImageFile) {
        uploadPhotoView.uploadImage(nameImageFile);
    }

    @OnClick(R.id.upload_view)
    void showSelectDialog() {
        checkPermissionsAndGetImage();
    }

    @OnClick(R.id.btnCreate)
    void createAccount() {
        Utils.hideKeyboard(getActivity());
        if (!isValidateInput()) {
            return;
        }
        registrationPresenter.signUp(
                etUsername.getText().toString().trim(),
                etConfirmPassword.getText().toString().trim(),
                etFirstName.getText().toString().trim(),
                etLastName.getText().toString().trim(),
                etPhone.getText().toString().trim(),
                etEmail.getText().toString().trim()
        );
    }

    private boolean isValidateInput() {
        if (etPassword.getText().toString().trim().length() < 5) {
            onShowError(getString(R.string.password_short));
            return false;
        }
        if (!TextUtils.equals(
                etPassword.getText().toString().trim(),
                etConfirmPassword.getText().toString().trim())
                ) {
            onShowError(getString(R.string.confirm_pass_invalid));
            return false;
        }
        return true;
    }

    @OnClick(R.id.btnLogin)
    void goToLogin() {
        registrationPresenter.goToLogin();
    }

    private EditTextClearUtil getEditTextClearUtil(Drawable clearDrawable, EditText editText) {
        return new EditTextClearUtil.Builder(getContext())
                .setEditText(editText)
                .setClearDrawable(clearDrawable)
                .build();
    }

}