package lanars.com.defigo.register;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.arellomobile.mvp.InjectViewState;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.navigation.RouteScreenName;
import lanars.com.defigo.common.network.models.SignModel;
import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.common.network.models.UsersRoom;
import lanars.com.defigo.common.network.service.IAuthService;
import lanars.com.defigo.common.network.service.IDeviceService;
import lanars.com.defigo.common.repository.user.IUserRepository;
import lanars.com.defigo.common.utils.DeviceIdUtil;
import lanars.com.defigo.common.utils.FileUtils;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.common.utils.IFileUtils;
import lanars.com.defigo.cropimage.CropPresenter;
import lanars.com.defigo.database.converter.RoomConverter;
import lanars.com.defigo.database.converter.UserConverter;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.database.model.DBUser;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class RegistrationPresenter extends BasePresenter<RegistrationView> {

    @Inject
    Router router;

    @Inject
    IAuthService authService;

    @Inject
    IUserRepository userRepository;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    IDeviceService deviceService;

    @Inject
    SocketManager socketManager;

    @Inject
    IFileUtils fileUtils;

    @Inject
    Context appContext;

    private String nameImageFile;

    public RegistrationPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @VisibleForTesting
    RegistrationPresenter(Router router, IAuthService authService, IUserRepository userRepository, IAppPreferences appPreferences, IDeviceService deviceService, SocketManager socketManager, IFileUtils fileUtils) {
        this.router = router;
        this.authService = authService;
        this.userRepository = userRepository;
        this.appPreferences = appPreferences;
        this.deviceService = deviceService;
        this.socketManager = socketManager;
        this.fileUtils = fileUtils;
    }

    @Override
    public void attachView(RegistrationView view) {
        super.attachView(view);
        uploadStorageImageInView();
    }

    public void uploadStorageImageInView() {
        if (fileUtils.checkExistFile(appContext, nameImageFile)) {
            getViewState().uploadImageView(nameImageFile);
        }
    }

    public void signUp(String userName, String password, String firstName, String lastName, String phone, String email) {
        RequestBody requestImageFile = getAvatarImage() != null
                ? RequestBody.create(MediaType.parse("image/jpeg"), getAvatarImage())
                : null;

        MultipartBody.Part multipartBodyImage = requestImageFile != null
                ? MultipartBody.Part.createFormData("image", getAvatarImage().getName(), requestImageFile)
                : null;

        authService.signUp(
                multipartBodyImage,
                getRequestBodyParams(userName),
                getRequestBodyParams(password),
                getRequestBodyParams(firstName),
                getRequestBodyParams(lastName),
                getRequestBodyParams(phone),
                getRequestBodyParams(email))
                .doOnSubscribe(disposable -> getViewState().onShowProgress())
                .doAfterTerminate(() -> getViewState().onHideProgress())
                .flatMap(saveAccessToken())
                .flatMap(getCurrentUserProfile())
                .doOnNext(saveCurrentUserId())
                .doOnNext(this::convertAndSaveRoomsInDb)
                .map(UserConverter::convertToDatabaseUser)
                .doOnNext(saveUserInDatabase())
                .doOnNext(this::updateDeviceId)
                .ignoreElements()
                .subscribe(this::onSuccessRegister, this::onErrorRegister);
    }

    @Nullable
    private RequestBody getRequestBodyParams(String inputParams) {
        return inputParams != null
                ? RequestBody.create(MediaType.parse("multipart/form-data"), inputParams)
                : null;
    }

    @NonNull
    private Consumer<DBUser> saveUserInDatabase() {
        return user -> userRepository.insertUsers(user);
    }

    private void convertAndSaveRoomsInDb(User user) {
        List<DBRoom> dbRooms = new ArrayList<>();

        if (user.getUsersRooms() == null) {
            return;
        }

        DBRoom[] dbRoomArray = new DBRoom[user.getUsersRooms().size()];
        for (UsersRoom userRoom : user.getUsersRooms()) {
            dbRooms.add(RoomConverter.convertToDatabaseRoom(userRoom));
        }
        dbRooms.toArray(dbRoomArray);
        saveUserRoom(dbRoomArray);
    }

    private void saveUserRoom(DBRoom... dbRoom) {
        userRepository.insertRooms(dbRoom);
    }

    @NonNull
    private Function<SignModel, ObservableSource<SignModel>> saveAccessToken() {
        return signModel -> {
            appPreferences.saveAccessToken(signModel.getAccessToken());
            return Observable.just(signModel);
        };
    }

    @NonNull
    private Function<Object, ObservableSource<User>> getCurrentUserProfile() {
        return signModelResponse -> userRepository.usersMy();
    }

    @NonNull
    private Consumer<User> saveCurrentUserId() {
        return user -> appPreferences.saveCurrentUserId(user.getId());
    }

    @VisibleForTesting
    void updateDeviceId(DBUser dbUser) {
        DeviceIdUtil.updateDeviceId(deviceService);
    }

    private void onSuccessRegister() {
        socketManager.connectSocket(appPreferences.getAccessToken(), appPreferences.getCurrentUserId(), null);
        router.newRootScreen(RouteScreenName.MAIN);
    }

    private void onErrorRegister(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    public void uploadImage(Sources sources) {
        addToDisposable(
                RxImagePicker.with(DefigoApplication.getInstance())
                        .requestImage(sources)
                        .doOnComplete(() -> getViewState().onShowProgress())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::goToCropImage, this::onErrorUpload));
    }

    private void onErrorUpload(Throwable throwable) {
        getViewState().onShowError(throwable.getLocalizedMessage());
    }

    private void goToCropImage(@NonNull Uri uri) {
        getViewState().onHideProgress();
        nameImageFile = fileUtils.getUniqueFileName(FileUtils.IMAGE_PROFILE);
        Bundle bundle = new Bundle();
        bundle.putParcelable(CropPresenter.IMAGE_URI, uri);
        bundle.putString(CropPresenter.IMAGE_NAME, nameImageFile);
        router.navigateTo(RouteScreenName.CROP_FRAGMENT, bundle);
    }

    public File getAvatarImage() {
        return fileUtils.getDiskCacheDir(DefigoApplication.getInstance(), nameImageFile);
    }

    public void showSelectImageDialog() {
        getViewState().showPickerPhotoDialog();
    }

    public void deleteSelectedPhotoFromLocalStorage(String nameImageFile) {
        if (nameImageFile != null) {
            File diskCacheDir = fileUtils.getDiskCacheDir(DefigoApplication.getInstance(), nameImageFile);
            fileUtils.deleteFile(diskCacheDir);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deleteSelectedPhotoFromLocalStorage(nameImageFile);
    }

    public void goToLogin() {
        router.exit();
    }
}
