package lanars.com.defigo;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.iid.FirebaseInstanceId;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;
import org.webrtc.SessionDescription;

import java.net.URISyntaxException;
import java.util.Arrays;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;

public class SocketManager {
    public static final String CALL_REQUEST_EVENT = "call:request";
    public static final String CALL_OFFER = "call:offer";
    public static final String CALL_CANDIDATE = "call:candidate";
    public static final String CALL_STOPPED = "call:stopped";
    public static final String CALL_PING = "call:ping";
    public static final String CALL_PONG = "call:pong";
    public static final String CALL_STATUS = "call:status";
    private static final String AUTHORIZED_EVENT = "authorized";
    private static final String EXCEPTION_EVENT = "call:exception";
    private static final String CALL_ANSWER = "call:answer";
    private static final String CALL_RESPONSE = "call:response";
    private static final String CALL_BRAKE = "call:brake";

    private Socket socket;
    private String callUuid;
    private long userId;

    public void connectSocket(String accessToken, long userId, @Nullable Ack ack) {
        this.userId = userId;

        try {
            connectToSocket(accessToken, ack);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void connectToSocket(String accessToken, @Nullable Ack onAuthorizedCallback) throws URISyntaxException {
        String deviceId = FirebaseInstanceId.getInstance().getId();

        String uri = BuildConfig.SERVER_URL + "users?token=" + accessToken + "&deviceId=" + deviceId;
        Logger.d("Socket URL " + uri);
        socket = IO.socket(uri);

        socket.disconnect();
        socket.connect()
                .on(CALL_REQUEST_EVENT, this::handleCallRequestEvent)
                .on(AUTHORIZED_EVENT, args -> this.handleAuthorizedEvent(onAuthorizedCallback, args))
                .on(Socket.EVENT_CONNECT, args -> Logger.d("EVENT_CONNECT args.length= " + Arrays.toString(args)))
                .on(Socket.EVENT_CONNECT_ERROR, args -> Logger.e("CONNECT_ERROR= " + Arrays.toString(args)))
                .on(Socket.EVENT_ERROR, args -> Logger.e("EVENT_ERROR= " + Arrays.toString(args)))
                .on(EXCEPTION_EVENT, args -> Logger.e("EXCEPTION_EVENT= " + Arrays.toString(args)));
    }

    private void handleCallRequestEvent(Object... args) {
        Logger.d("SOCKET MANAGER CALL_REQUEST_EVENT " + Arrays.toString(args));
        try {
            JSONObject jsonObject = new JSONObject(args[0].toString());

            this.callUuid = jsonObject.getString("callUuid");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleAuthorizedEvent(@Nullable Ack ack, Object... args) {
        Logger.i("authorized " + Arrays.toString(args) + " id " + socket.id());
        if (ack != null) {
            ack.call(socket.id());
        }
    }

    public void sendCallResponse() {
        try {
            JSONObject jsonCallUuid = new JSONObject();
            jsonCallUuid.put("callUuid", this.callUuid);

            socket.emit(CALL_RESPONSE, jsonCallUuid);
            Logger.d("CALL_RESPONSE JSON " + jsonCallUuid.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendAnswer(SessionDescription sessionDescription) {
        JSONObject body = new JSONObject();
        JSONObject answer = new JSONObject();
        try {
            answer.put("type", sessionDescription.type.toString().toLowerCase());
            answer.put("sdp", sessionDescription.description);

            body.put("answer", answer.toString());
            body.put("callUuid", this.callUuid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.d("socket send Answer " + body.toString());
        socket.emit(CALL_ANSWER, body);
    }

    public void sendIceCandidate(IceCandidate iceCandidate) {
        Logger.d("onIceCandidate");
        JSONObject body = new JSONObject();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("sdpMid", iceCandidate.sdpMid);
            jsonObject.put("sdpMLineIndex", iceCandidate.sdpMLineIndex);
            jsonObject.put("candidate", iceCandidate.sdp);

            body.put("candidate", jsonObject.toString());
            body.put("callUuid", this.callUuid);

            socket.emit(CALL_CANDIDATE, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void brakeCall(long userId, String callUuid, @NonNull Ack ack) {
        JSONObject body = new JSONObject();
        try {
            body.put("userId", userId);
            body.put("callUuid", callUuid);

            socket.emit(CALL_BRAKE, body);
            Logger.i("brake call callUuid " + callUuid + " userId " + userId);
            ack.call();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void disconnect() {
        if (socket != null) {
            socket.disconnect();
            socket = null;
            this.callUuid = null;
            Logger.w("socket manager disconnect");
        }
    }

    public void checkCallStatus(String callUuid) {

        try {
            JSONObject jsonCallUuid = new JSONObject();
            jsonCallUuid.put("callUuid", callUuid);

            getSocket()
                    .emit(SocketManager.CALL_STATUS, jsonCallUuid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public long getUserId() {
        return userId;
    }

    public String getCallUuid() {
        return callUuid;
    }

    public void setCallUuid(String callUuid) {
        this.callUuid = callUuid;
    }
}
