package lanars.com.defigo.cropimage;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;

public class CropFragment extends MvpAppCompatFragment implements CropFragmentView {

    @BindView(R.id.cropImageView)
    public CropImageView cropImageView;

    @BindView(R.id.btnApply)
    public Button btnApply;

    @InjectPresenter
    public CropPresenter cropPresenter;

    private String nameImage;

    public static CropFragment newInstance(Bundle bundle) {
        CropFragment fragment = new CropFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.crop_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        uploadImage();
    }

    private void uploadImage() {
        Uri uri = getArguments().getParcelable("uri");
        nameImage = getArguments().getString("nameImage");
        Glide.with(this)
                .load(uri)
                .into(cropImageView);
    }

    //TODO Move logic to presenter
    @OnClick(R.id.btnApply)
    public void applyCropImage() {
        cropPresenter.saveImageInFileStorage(nameImage, cropImageView.getCroppedImage());
        cropPresenter.onBack();
    }

    @Override
    public void onShowError(String errorMessage) {

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }
}
