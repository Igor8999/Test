package lanars.com.defigo.cropimage;

import android.content.Context;
import android.graphics.Bitmap;

import com.arellomobile.mvp.InjectViewState;

import java.io.File;

import javax.inject.Inject;

import lanars.com.defigo.BuildConfig;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.common.base.BasePresenter;
import lanars.com.defigo.common.utils.FileUtils;
import lanars.com.defigo.common.utils.IAppPreferences;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class CropPresenter extends BasePresenter<CropFragmentView> {

    @Inject
    Router router;

    @Inject
    Context context;

    private FileUtils fileUtils = new FileUtils();

    public static final String IMAGE_URI = "uri";
    public static final String IMAGE_NAME = "nameImage";

    @Inject
    IAppPreferences appPreferences;

    public CropPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    public void onBack() {
        router.exit();
    }

    public void saveImageInFileStorage(String nameImage, Bitmap croppedBitmap) {
        File file = fileUtils.getDiskCacheDir(context, nameImage);
        fileUtils.saveBitmapIntoFile(file, Bitmap.createScaledBitmap(croppedBitmap, BuildConfig.AVATAR_SIZE_IN_PX, BuildConfig.AVATAR_SIZE_IN_PX, false));
    }
}
