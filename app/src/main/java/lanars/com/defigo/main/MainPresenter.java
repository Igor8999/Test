package lanars.com.defigo.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.SocketManager;
import lanars.com.defigo.call.CallPreviewActivity;
import lanars.com.defigo.common.utils.IAppPreferences;
import lanars.com.defigo.push.TrackerMessagingService;
import ru.terrakok.cicerone.Router;

import static lanars.com.defigo.SocketManager.CALL_REQUEST_EVENT;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @Inject
    Router router;

    @Inject
    SocketManager socketManager;

    @Inject
    IAppPreferences appPreferences;

    @Inject
    Context context;

    public MainPresenter() {
        DefigoApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        connectToCallRequestEvent();
    }

    private void connectToCallRequestEvent() {
        Logger.d("MAIN PRESENTER connectToCallRequestEvent");
        if (socketManager.getSocket() == null) {
            socketManager.connectSocket(appPreferences.getAccessToken(), (int) appPreferences.getCurrentUserId(), args -> socketManager.getSocket()
                    .on(CALL_REQUEST_EVENT, MainPresenter.this::handleCallRequestEvent));
            return;
        }
        socketManager.getSocket()
                .on(CALL_REQUEST_EVENT, this::handleCallRequestEvent);
    }

    private void handleCallRequestEvent(Object... args) {
        Logger.d("MAIN PRESENTER CALL_REQUEST_EVENT " + Arrays.toString(args));
        try {
            JSONObject jsonObject = new JSONObject(args[0].toString());

            String callUuid = jsonObject.getString(TrackerMessagingService.KEY_CALL_UUID);
            String address = jsonObject.getString(TrackerMessagingService.KEY_ADDRESS);
            String doorbell = jsonObject.getString(TrackerMessagingService.KEY_DOORBELL);
            String doorbellId = jsonObject.getString(TrackerMessagingService.KEY_DOORBELL_ID);
            boolean isVideoEnabled = jsonObject.getBoolean(TrackerMessagingService.KEY_VIDEO_ENABLED);

            Bundle bundle = new Bundle();
            bundle.putString(TrackerMessagingService.KEY_CALL_UUID, callUuid);
            bundle.putString(TrackerMessagingService.KEY_ADDRESS, address);
            bundle.putString(TrackerMessagingService.KEY_DOORBELL, doorbell);
            bundle.putString(TrackerMessagingService.KEY_DOORBELL_ID, doorbellId);
            bundle.putBoolean(TrackerMessagingService.KEY_VIDEO_ENABLED, isVideoEnabled);

            if (CallPreviewActivity.isRunning) {
                return;
            }

            Intent callIntent = CallPreviewActivity.constructIntent(context, bundle);
            context.startActivity(callIntent);

            // TODO Fix issue when mViews == null
//            getViewState().openCallScreen(bundle);
        } catch (JSONException throwable) {
            Crashlytics.logException(throwable);
            throwable.printStackTrace();
        }
    }
}
