package lanars.com.defigo.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.support.v4.view.RxViewPager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import lanars.com.defigo.DefigoApplication;
import lanars.com.defigo.R;
import lanars.com.defigo.call.CallPreviewActivity;
import lanars.com.defigo.common.base.BaseActivity;
import lanars.com.defigo.home.HomeFragment;
import lanars.com.defigo.home.adapter.HomePagerAdapter;
import lanars.com.defigo.settings.SettingsContainerFragment;
import ru.terrakok.cicerone.NavigatorHolder;

import static lanars.com.defigo.push.TrackerMessagingService.KEY_CALL_UUID;

public class MainActivity extends BaseActivity implements MainView {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.appBar)
    AppBarLayout appBarLayout;

    @BindView(R.id.noConnection)
    View noConnection;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @InjectPresenter
    MainPresenter mainPresenter;

    @Inject
    NavigatorHolder navigatorHolder;
    private static Set<View> views = new HashSet<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DefigoApplication.getInstance().getAppComponent().inject(this);
        initViews();
        initContainers();
    }

    @Override
    public void changeNetworkConnection(boolean isConnection) {
        super.changeNetworkConnection(isConnection);
        noConnection.setVisibility(isConnection ? View.GONE : View.VISIBLE);
    }

    private void initContainers() {
        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager(), getFragmentList());
        viewPager.setAdapter(adapter);
        RxViewPager.pageSelections(viewPager).skipInitialValue().subscribe(this::selectTabWithPager);
    }

    private void selectTabWithPager(Integer integer) {
        bottomNavigationView.setSelectedItemId(bottomNavigationView.getMenu().getItem(integer).getItemId());
    }

    private List<Fragment> getFragmentList() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new HomeFragment());
        fragmentList.add(new SettingsContainerFragment());
        return fragmentList;
    }

    private void initViews() {
        ButterKnife.bind(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this::bottomMenuNavigation);
    }

    private boolean bottomMenuNavigation(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                viewPager.setCurrentItem(0);
                break;
            case R.id.settings:
                viewPager.setCurrentItem(1);
                break;
        }
        return true;
    }

    @Override
    public void onShowError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

    @Override
    public void openCallScreen(Bundle bundle) {
        // TODO This method is not called. Look at presenter (Note: Fix issue when mViews == null)
        if (bundle != null && !TextUtils.isEmpty(bundle.getString(KEY_CALL_UUID))) {
            if (DefigoApplication.getInstance().isRunning()) {
                return;
            }
            Intent callIntent = CallPreviewActivity.constructIntent(MainActivity.this, bundle);
            startActivity(callIntent);
        }
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }
}
