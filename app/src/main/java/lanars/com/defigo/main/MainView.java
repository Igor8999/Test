package lanars.com.defigo.main;

import android.os.Bundle;

import lanars.com.defigo.common.base.BaseMvpView;

public interface MainView extends BaseMvpView {
    void openCallScreen(Bundle bundle);
}
