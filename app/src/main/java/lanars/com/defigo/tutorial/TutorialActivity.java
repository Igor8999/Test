package lanars.com.defigo.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lanars.com.defigo.R;
import lanars.com.defigo.admin.base.AdminBaseActivity;

public class TutorialActivity extends MvpAppCompatActivity implements TutorialView {

    @BindView(R.id.btnStart)
    TextView btnStart;

    @InjectPresenter
    TutorialPresenter tutorialPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnStart)
    void start() {
        tutorialPresenter.start();
    }

    @Override
    public void startMain() {
        //todo tmp (need add check admin)
//        MainActivity.start(this);
        startActivity(new Intent(this, AdminBaseActivity.class));
        finish();
    }
}
