package lanars.com.defigo.tutorial;

import com.arellomobile.mvp.InjectViewState;

import lanars.com.defigo.common.base.BasePresenter;

@InjectViewState
public class TutorialPresenter extends BasePresenter<TutorialView> {

    public void start() {
        getViewState().startMain();
    }
}
