package lanars.com.defigo.tutorial;

import com.arellomobile.mvp.MvpView;

public interface TutorialView extends MvpView {

    void startMain();
}
