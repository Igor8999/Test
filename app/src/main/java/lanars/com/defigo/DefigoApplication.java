package lanars.com.defigo;

import android.app.Activity;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import lanars.com.defigo.common.di.AppComponent;
import lanars.com.defigo.common.di.DaggerAppComponent;
import lanars.com.defigo.common.di.modules.DataModule;
import lanars.com.defigo.common.di.modules.LocalNavigationModule;
import lanars.com.defigo.common.di.modules.NavigationModule;
import lanars.com.defigo.common.di.modules.database.DatabaseModule;
import lanars.com.defigo.common.di.modules.network.DefigoApiModule;

public class DefigoApplication extends MultiDexApplication {

    public static DefigoApplication instance;
    private AppComponent appComponent;
    private boolean isRunning;
    private int numberActivityStarted;

    @Inject
    SocketManager socketManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appComponent = buildComponent();
        appComponent.inject(this);
        instance = this;
        registerActivityLifecycleCallbacks(activityLifecycleCallbacks);

        PrettyFormatStrategy.Builder formatStrategyBuilder = PrettyFormatStrategy.newBuilder();
        formatStrategyBuilder.methodCount(4);

        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategyBuilder.build()) {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .databaseModule(new DatabaseModule())
                .dataModule(new DataModule(this))
                .navigationModule(new NavigationModule())
                .localNavigationModule(new LocalNavigationModule())
                .defigoApiModule(new DefigoApiModule())
                .build();
    }

    public static DefigoApplication getInstance() {
        return instance;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public boolean isRunning() {
        return isRunning;
    }

    private ActivityLifecycleCallbacks activityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {
            if (numberActivityStarted == 0) {
                isRunning = true;
            }
            numberActivityStarted++;
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {
            numberActivityStarted--;

            if (numberActivityStarted == 0) {
                isRunning = false;
                socketManager.disconnect();
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    };
}
