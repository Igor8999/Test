package lanars.com.defigo.database.converter;

import lanars.com.defigo.common.network.models.UsersRoom;
import lanars.com.defigo.database.model.DBRoom;

public class RoomConverter {

    public static DBRoom convertToDatabaseRoom(UsersRoom usersRoom) {
        DBRoom.Builder builder = new DBRoom.Builder();
        builder
                .id(usersRoom.getId())
                .header(usersRoom.getHeader())
                .address(usersRoom.getAddress());

        return builder.build();
    }

}
