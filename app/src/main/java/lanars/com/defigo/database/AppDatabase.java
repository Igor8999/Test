package lanars.com.defigo.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import lanars.com.defigo.database.dao.RoomsDao;
import lanars.com.defigo.database.dao.UsersDao;
import lanars.com.defigo.database.model.DBRoom;
import lanars.com.defigo.database.model.DBUser;

@Database(entities = {DBUser.class, DBRoom.class}, version = 4)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UsersDao usersDao();

    public abstract RoomsDao roomsDao();
}