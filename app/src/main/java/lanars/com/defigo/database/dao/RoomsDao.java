package lanars.com.defigo.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import lanars.com.defigo.database.model.DBRoom;

@Dao
public interface RoomsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRooms(DBRoom... dbRooms);

    @Update
    void updateRooms(DBRoom... dbRooms);

    @Query("SELECT * FROM DBRoom")
    Flowable<List<DBRoom>> getAllRooms();

    @Query("DELETE FROM DBRoom")
    void clearRoomsDb();
}
