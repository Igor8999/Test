package lanars.com.defigo.database.converter;

import lanars.com.defigo.common.network.models.User;
import lanars.com.defigo.database.model.DBUser;

public final class UserConverter {

    public static DBUser convertToDatabaseUser(User user) {
        DBUser.Builder builder = new DBUser.Builder();
        builder
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .userName(user.getUserName())
                .imgUrl(user.getImgUrl())
                .email(user.getEmail())
                .phone(user.getPhone())
                .city(user.getCity())
                .street(user.getStreet())
                .country(user.getCountry())
                .zip(user.getZip())
                .createdAt(user.getCreatedAt())
                .updatedAt(user.getUpdatedAt())
                .hasVideo(user.hasVideo())
                .role(user.getRole())
                .currentSound(user.getCurrentSound());

        return builder.build();
    }
}