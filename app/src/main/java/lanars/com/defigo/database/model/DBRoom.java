package lanars.com.defigo.database.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class DBRoom {

    @PrimaryKey
    public int id;

    private String header;
    private String address;

    public DBRoom(int id, String header, String address) {
        this.id = id;
        this.header = header;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private DBRoom(Builder builder) {
        id = builder.id;
        header = builder.header;
        address = builder.address;
    }

    public static final class Builder {
        private int id;
        private String header;
        private String address;

        public Builder() {
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder header(String val) {
            header = val;
            return this;
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public DBRoom build() {
            return new DBRoom(this);
        }
    }
}
