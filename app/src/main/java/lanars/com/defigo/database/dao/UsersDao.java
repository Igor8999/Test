package lanars.com.defigo.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import lanars.com.defigo.database.model.DBUser;

@Dao
public interface UsersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsers(DBUser... dbUsers);

    @Update
    void updateUsers(DBUser... dbUsers);

    @Query("SELECT * FROM DBUser WHERE id = :id")
    Flowable<List<DBUser>> getUserById(long id);

    @Query("DELETE FROM DBUser")
    void clearUserDb();
}