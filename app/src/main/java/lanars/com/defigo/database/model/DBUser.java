package lanars.com.defigo.database.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class DBUser {
    @PrimaryKey
    public long id;

    public String firstName;
    public String lastName;
    public String userName;
    public String email;
    public String phone;
    public String defaultAddress;
    public String imgUrl;
    public String createdAt;
    public String updatedAt;
    public String street;
    public String zip;
    public String country;
    public String city;
    public boolean hasVideo;
    public int role;
    public String currentSound;

    @Override
    public String toString() {
        return "DBUser{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", defaultAddress='" + defaultAddress + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", street='" + street + '\'' +
                ", zip='" + zip + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", hasVideo=" + hasVideo +
                ", role=" + role +
                ", currentSound='" + currentSound + '\'' +
                '}';
    }

    public static DBUser provideStubUser() {
        return new DBUser(-1, null, null);
    }

    public DBUser(long id, String firstName, String lastName) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private DBUser(Builder builder) {
        id = builder.id;
        firstName = builder.firstName;
        lastName = builder.lastName;
        userName = builder.userName;
        email = builder.email;
        phone = builder.phone;
        defaultAddress = builder.defaultAddress;
        imgUrl = builder.imgUrl;
        createdAt = builder.createdAt;
        updatedAt = builder.updatedAt;
        street = builder.street;
        zip = builder.zip;
        country = builder.country;
        city = builder.city;
        hasVideo = builder.hasVideo;
        role = builder.role;
        currentSound = builder.currentSound;
    }

    public static final class Builder {
        private String firstName;
        private String lastName;
        private String userName;
        private String email;
        private String phone;
        private String defaultAddress;
        private String imgUrl;
        private String createdAt;
        private String updatedAt;
        private String street;
        private String zip;
        private String country;
        private String city;
        private boolean hasVideo;
        private int role;
        private String currentSound;
        private long id;

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder firstName(String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(String val) {
            lastName = val;
            return this;
        }

        public Builder userName(String val) {
            userName = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public Builder defaultAddress(String val) {
            defaultAddress = val;
            return this;
        }

        public Builder imgUrl(String val) {
            imgUrl = val;
            return this;
        }

        public Builder createdAt(String val) {
            createdAt = val;
            return this;
        }

        public Builder updatedAt(String val) {
            updatedAt = val;
            return this;
        }

        public Builder street(String val) {
            street = val;
            return this;
        }

        public Builder zip(String val) {
            zip = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public Builder city(String val) {
            city = val;
            return this;
        }

        public Builder hasVideo(boolean val) {
            hasVideo = val;
            return this;
        }

        public Builder role(int val) {
            role = val;
            return this;
        }

        public Builder currentSound(String val) {
            currentSound = val;
            return this;
        }

        public DBUser build() {
            return new DBUser(this);
        }
    }
}